package com.quess.thing.util.objects

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.text.TextUtils.replace
import androidx.fragment.app.Fragment
import com.quess.thing.model.UserData
import com.quess.thing.util.MainNavigationFragment
import com.quess.thing.util.inTransaction
import java.util.*


object Utils {

    fun randomWordFromList(list: List<String>, random: Random): String {
        return list[random.nextInt(list.size)]
    }

    fun randomWordFromArrayList(list: ArrayList<UserData?>, random: Random): UserData? {
        return list[random.nextInt(list.size)]
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
    }

    fun shuffleLettersFromWord(wordToDraw: String): String {
        val chars: ArrayList<Char> = ArrayList(wordToDraw.length)
        for (c in wordToDraw.toCharArray()) {
            chars.add(c)
        }
        chars.shuffle()
        val shuffled = CharArray(chars.size)
        for (i in shuffled.indices) {
            shuffled[i] = chars[i]
        }
        return String(shuffled)
    }

    fun splitString(string: String): List<String> {
        val pattern = """""".toRegex()
        return pattern.split(string).filter { it.isNotBlank() }
    }
}