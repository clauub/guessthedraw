package com.quess.thing.util.admob

import android.content.Context
import android.view.View
import com.google.android.ads.nativetemplates.TemplateView
import com.google.android.gms.ads.*
import com.quess.thing.R
import kotlinx.android.synthetic.main.fragment_one_try_dialog.*
import timber.log.Timber

object AdUtil {

    fun loadAd(root: View, adViewBanner: Int) {
        val adView = root.findViewById<AdView>(adViewBanner)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    fun loadAdNative(root: View, context: Context, adViewNative: Int) {
        val adView = root.findViewById<TemplateView>(adViewNative)
        val adLoader = AdLoader.Builder(context, context.getString(R.string.ad_native)).forUnifiedNativeAd {
            adView.setNativeAd(it)
        }
            .build()
        adLoader.loadAd(AdRequest.Builder().build())
    }
}