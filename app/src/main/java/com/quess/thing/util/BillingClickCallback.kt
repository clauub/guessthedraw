package com.quess.thing.util

import com.quess.thing.repository.billing.localdb.AugmentedSkuDetails

interface BillingClickCallback {
    fun onClick(skuDetails: AugmentedSkuDetails)
}
