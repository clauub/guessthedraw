package com.quess.thing.util

import android.content.Context
import com.quess.thing.R

class ListString(context: Context) {

    val ANIMALS_LIST: List<String> = listOf(
        context.getString(R.string.cat),
        context.getString(R.string.dog),
        context.getString(R.string.tiger),
        context.getString(R.string.elephant),
        context.getString(R.string.lion),
        context.getString(R.string.fish),
        context.getString(R.string.giraffe)
    )
    val FOODS_LIST: List<String> = listOf(
        context.getString(R.string.burger),
        context.getString(R.string.hot_dog),
        context.getString(R.string.pizza),
        context.getString(R.string.egg_eye),
        context.getString(R.string.bread),
        context.getString(R.string.salami)
    )
    val DESSERTS_LIST: List<String> = listOf(
        context.getString(R.string.pancakes),
        context.getString(R.string.ice_cream),
        context.getString(R.string.cake),
        context.getString(R.string.chocolate),
        context.getString(R.string.muffin),
        context.getString(R.string.donut)
    )
    val PRODUCE_LIST: List<String> = listOf(
        context.getString(R.string.apple),
        context.getString(R.string.carrot),
        context.getString(R.string.pear),
        context.getString(R.string.grapes),
        context.getString(R.string.banana),
        context.getString(R.string.cherries),
        context.getString(R.string.pineapple),
        context.getString(R.string.tomatoes),
        context.getString(R.string.cucumber),
        context.getString(R.string.mushroom),
        context.getString(R.string.potatoes)
    )
    val SPORTS_LIST: List<String> = listOf(
        context.getString(R.string.medal),
        context.getString(R.string.tennis_racket),
        context.getString(R.string.soccer_gate),
        context.getString(R.string.basketball_hoop),
        context.getString(R.string.golf_club),
        context.getString(R.string.baseball_bat)
    )
    val CLOTHES_LIST: List<String> = listOf(
        context.getString(R.string.skirt),
        context.getString(R.string.trousers),
        context.getString(R.string.t_shirt),
        context.getString(R.string.shirt),
        context.getString(R.string.shorts),
        context.getString(R.string.jumper),
        context.getString(R.string.cap),
        context.getString(R.string.flip_flops),
        context.getString(R.string.sock),
        context.getString(R.string.bow_tie)
    )
    val HOME_LIST: List<String> = listOf(
        context.getString(R.string.sofa),
        context.getString(R.string.vase),
        context.getString(R.string.window),
        context.getString(R.string.painting),
        context.getString(R.string.table),
        context.getString(R.string.lamp),
        context.getString(R.string.fan),
        context.getString(R.string.bed),
        context.getString(R.string.nightstand)
    )

    val TECH_LIST: List<String> = listOf(
        context.getString(R.string.laptop),
        context.getString(R.string.computer),
        context.getString(R.string.keyboard),
        context.getString(R.string.smartphone),
        context.getString(R.string.headphones),
        context.getString(R.string.speakers),
        context.getString(R.string.photo_camera),
        context.getString(R.string.printer)
    )

    val PLAYER_LIST: List<String> =
        listOf(context.getString(R.string.player1), context.getString(R.string.player2))


    val EASY_WORD_LIST: List<String> = listOf(
        context.getString(R.string.laptop),
        context.getString(R.string.ice_cream),
        context.getString(R.string.bed),
        context.getString(R.string.medal),
        context.getString(R.string.shorts),
        context.getString(R.string.pear),
        context.getString(R.string.apple),
        context.getString(R.string.carrot),
        context.getString(R.string.egg_eye),
        context.getString(R.string.hot_dog),
        context.getString(R.string.fish)
    )

    val MEDIUM_WORD_LIST: List<String> = listOf(
        context.getString(R.string.smartphone),
        context.getString(R.string.photo_camera),
        context.getString(R.string.printer),
        context.getString(R.string.nightstand),
        context.getString(R.string.sofa),
        context.getString(R.string.burger),
        context.getString(R.string.giraffe),
        context.getString(R.string.mushroom),
        context.getString(R.string.potatoes),
        context.getString(R.string.grapes)
    )

    val HARD_WORD_LIST: List<String> = listOf(
        context.getString(R.string.flip_flops),
        context.getString(R.string.baseball_bat),
        context.getString(R.string.muffin),
        context.getString(R.string.lion),
        context.getString(R.string.elephant),
        context.getString(R.string.tiger),
        context.getString(R.string.pancakes),
        context.getString(R.string.keyboard),
        context.getString(R.string.speakers)
    )
}