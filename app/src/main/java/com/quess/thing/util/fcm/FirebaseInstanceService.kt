package com.quess.thing.util.fcm

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.quess.thing.R
import timber.log.Timber

class FirebaseInstanceService : FirebaseMessagingService() {

    private lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        sendNotification(p0)
    }

    private fun sendNotification(p0: RemoteMessage) {
        val title = p0.notification?.title
        val content = p0.notification?.body

        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelID = "GuessTheDraw"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            notificationChannel =
                NotificationChannel(
                    channelID,
                    "GuessTheDraw Notification",
                    NotificationManager.IMPORTANCE_HIGH
                )
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.BLUE
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(this, channelID)
                .setContentTitle(title)
                .setContentText(content)
                .setColor(resources.getColor(R.color.splashColor))
                .setSmallIcon(R.drawable.ic_stat_name)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_stat_name))
        } else {

            Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(content)
                .setColor(resources.getColor(R.color.splashColor))
                .setSmallIcon(R.drawable.ic_stat_name)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_stat_name))
        }


        notificationManager.notify(1234, builder.build())

    }

    @SuppressLint("TimberArgCount")
    override fun onNewToken(p0: String) {
        super.onNewToken(p0)

        Timber.d("TokenFirebase", p0)
    }

}
