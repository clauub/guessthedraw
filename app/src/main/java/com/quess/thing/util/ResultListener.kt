package com.quess.thing.util

interface ResultListener {
    fun onSuccess()
    fun onFailure(message: String)
}