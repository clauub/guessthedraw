package com.quess.thing.util.objects

import android.content.Context
import com.quess.thing.R

object Constants {

    const val NAME: String = "name"
    const val PHOTO_URL: String = "photo_url"
    const val COINS_KEY: String = "coins"
    const val USERS: String = "users"
    const val INVITATIONS: String = "invitations"
    const val INVITATION_NUMBER: String = "invitation_number"
    const val UID: String = "uid"
    const val WINS: String = "wins"
    const val WIN: String = "win"
    const val IS_PLAYING: String = "isPlaying"
    const val IMAGES: String = "images"
    const val DRAW_IMAGE: String = "draw_image"
    const val GAME: String = "game"
    const val LOSES: String = "loses"
    const val WORD_TO_DRAW: String = "word_to_draw"
    const val TIMESTAMP: String = "timestamp"
    const val CARD_INT_KEY: String = "card_int"
    const val COLOR_INT_KEY: String = "color_int"
    const val UNLOCK_KEY_1: String = "unlock_key_1"
    const val UNLOCK_KEY_2: String = "unlock_key_2"
    const val UNLOCK_KEY_3: String = "unlock_key_3"
    const val UNLOCK_BROWN: String = "unlock_brown"
    const val UNLOCK_PINK: String = "unlock_pink"
    const val UNLOCK_ACHIEVEMENT: String = "unlock_achievement"
}