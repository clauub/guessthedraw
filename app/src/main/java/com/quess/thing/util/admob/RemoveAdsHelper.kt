package com.quess.thing.util.admob

import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.ads.nativetemplates.TemplateView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import com.quess.thing.R
import com.quess.thing.ui.main.shop.BillingViewModel
import com.quess.thing.util.objects.Navigation

class RemoveAdsHelper(root: View, adViewBanner: Int, fragment: Fragment) {
    private var billingAdsViewModel: BillingViewModel =
        ViewModelProviders.of(fragment).get(BillingViewModel::class.java)


    val adView: AdView = root.findViewById(adViewBanner)

    private fun hideAds(entitled: Boolean) {
        if (entitled) {
            adView.visibility = View.GONE
        }
    }

    fun removeAds(lifecycleOwner: LifecycleOwner) {
        billingAdsViewModel.premiumCarLiveData.observe(lifecycleOwner, Observer {
            it?.apply { hideAds(entitled) }
        })
    }
}


class RemoveNativeAdsHelper(root: View, adViewBanner: Int, fragment: Fragment) {
    private var billingAdsViewModel: BillingViewModel =
        ViewModelProviders.of(fragment).get(BillingViewModel::class.java)

    private val adNativeView: TemplateView = root.findViewById(adViewBanner)

    private fun hideNativeAds(entitled: Boolean) {
        if (entitled) {
            adNativeView.visibility = View.GONE
        }
    }

    fun removeNativeAds(lifecycleOwner: LifecycleOwner) {
        billingAdsViewModel.premiumCarLiveData.observe(lifecycleOwner, Observer {
            it?.apply { hideNativeAds(entitled) }
        })
    }
}

class RemoveInterstitialAdsHelper(private val root: View, fragment: Fragment) {
    private var billingAdsViewModel: BillingViewModel =
        ViewModelProviders.of(fragment).get(BillingViewModel::class.java)

    private fun hideInterstitialAds(entitled: Boolean, loadInterstitialAd: Unit) = if (entitled) {
        Navigation.goToMainActivity(root)
    } else {
        loadInterstitialAd
    }

    fun removeInterstitialAds(lifecycleOwner: LifecycleOwner, loadInterstitialAd: Unit) {
        billingAdsViewModel.premiumCarLiveData.observe(lifecycleOwner, Observer {
            it?.apply { hideInterstitialAds(entitled, loadInterstitialAd) }
        })
    }
}
