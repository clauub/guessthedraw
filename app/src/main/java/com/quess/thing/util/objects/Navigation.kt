package com.quess.thing.util.objects

import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import com.quess.thing.R


object Navigation {

    fun changeFragment(view: View, layout: Int) {
        Navigation.findNavController(view).navigate(layout)
    }

    fun goToGameFragment(view: View, bundle: Bundle) {
        Navigation.findNavController(view).navigate(R.id.gameFragment, bundle)
    }

    fun goToAnswerFragment(view: View, bundle: Bundle) {
        Navigation.findNavController(view).navigate(R.id.answerFragment, bundle)
    }

    fun goToLostFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.lostFragment)
    }

    fun goToWinnerFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.winnerFragment)
    }

    fun goToWordFragment(view: View, bundle: Bundle) {
        Navigation.findNavController(view).navigate(R.id.wordFragment, bundle)
    }

    fun goToMainFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.mainFragment)
    }

    fun goToMainActivity(view: View) {
        Navigation.findNavController(view).navigate(R.id.mainActivity)
    }

    fun goToWinnerFragment2(view: View) {
        Navigation.findNavController(view).navigate(R.id.winnerFragment)
    }

    fun goToCategoryFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.categoryFragment)
    }

    fun goToShopFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.aboutFragment)
    }

    fun goToSettingsFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.settingsFragment)
    }

    fun goToFreeModeFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.freeModeGameFragment)
    }

}