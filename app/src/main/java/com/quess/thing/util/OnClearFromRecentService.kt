package com.quess.thing.util

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.quess.thing.model.UserData
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.objects.Constants.GAME
import com.quess.thing.util.objects.Constants.IS_PLAYING
import dagger.android.AndroidInjection
import timber.log.Timber
import javax.inject.Inject


@SuppressLint("Registered")
open class OnClearFromRecentService : Service() {

    @Inject
    lateinit var databaseUserRepository: DatabaseGameRepository

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand")
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")

    }

    override fun onTaskRemoved(rootIntent: Intent) {
        super.onTaskRemoved(rootIntent)
        Timber.e("onTaskRemoved")

        //Code here
        deleteGameChildForBothUsers()

        stopSelf()
    }

    private fun deleteGameChildForBothUsers() {
        databaseUserRepository.setOpponentUserIDWinner()
        databaseUserRepository.deleteGameChild()
    }
}