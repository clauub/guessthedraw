package com.quess.thing.util.drawing

import android.content.res.Resources
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SeekBar
import androidx.constraintlayout.widget.ConstraintLayout
import com.divyanshu.draw.widget.DrawView
import com.quess.thing.ui.widget.CircleView
import kotlinx.android.synthetic.main.fragment_free_mode_game.*

@Suppress("NAME_SHADOWING")
class DrawingTools {

    fun setUpDrawingTools(
        root: View,
        imageDrawColor: Int,
        imageDrawWidth: Int,
        drawTools: Int,
        drawColorPalette: Int,
        circleViewWidth: Int,
        seekBarWidth: Int
    ) {
        val imageDrawColor: ImageView = root.findViewById(imageDrawColor)
        val imageDrawWidth: ImageView = root.findViewById(imageDrawWidth)
        val drawTools: ConstraintLayout = root.findViewById(drawTools)
        val drawColorPalette: LinearLayout = root.findViewById(drawColorPalette)
        val circleViewWidth: CircleView = root.findViewById(circleViewWidth)
        val seekBarWidth: SeekBar = root.findViewById(seekBarWidth)

        imageDrawColor.setOnClickListener {
            if (drawTools.translationY == (56).toPx) {
                toggleDrawTools(drawTools, true)
            } else if (drawTools.translationY == (0).toPx && drawColorPalette.visibility == View.VISIBLE) {
                toggleDrawTools(drawTools, false)
            }
            circleViewWidth.visibility = View.GONE
            drawColorPalette.visibility = View.VISIBLE
            seekBarWidth.visibility = View.GONE
        }

        imageDrawWidth.setOnClickListener {
            if (drawTools.translationY == (56).toPx) {
                toggleDrawTools(drawTools, true)
            } else if (drawTools.translationY == (0).toPx && seekBarWidth.visibility == View.VISIBLE) {
                toggleDrawTools(drawTools, false)
            }
            circleViewWidth.visibility = View.VISIBLE
            drawColorPalette.visibility = View.GONE
            seekBarWidth.visibility = View.VISIBLE
        }

    }

    fun setPaintWidth(root: View, seekBarWidth: Int, drawView: Int, circleViewWidth: Int) {
        val drawView: DrawView = root.findViewById(drawView)
        val circleViewWidth: CircleView = root.findViewById(circleViewWidth)
        val seekBarWidth: SeekBar = root.findViewById(seekBarWidth)

        seekBarWidth.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                drawView.setStrokeWidth(progress.toFloat())
                circleViewWidth.setCircleRadius(progress.toFloat())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

    }

    private fun toggleDrawTools(view: View, showView: Boolean = true) {
        if (showView) {
            view.animate().translationY((0).toPx)
        } else {
            view.animate().translationY((56).toPx)
        }
    }

    private val Int.toPx: Float
        get() = (this * Resources.getSystem().displayMetrics.density)
}