package com.quess.thing.util

import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import com.mikhaellopez.circularimageview.CircularImageView
import com.quess.thing.R

object Binding {

    @JvmStatic
    @BindingAdapter("app:circleImageView")
    fun loadImage(circleView: CircularImageView, imageUrl: String?) {
        Glide.with(circleView.context).load(imageUrl).into(circleView)
    }

    @JvmStatic
    @BindingAdapter("app:imgUrl")
    fun setDrawingImage(imageView: ImageView, imgUrl: String?) {
        Glide.with(imageView.context).load(imgUrl)
            .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("animation")
    fun setButtonAnimation(materialButton: MaterialButton, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(materialButton.context, R.anim.anim_from_left)
            animation.repeatMode = Animation.INFINITE
            materialButton.startAnimation(animation)
        }
    }

    @JvmStatic
    @BindingAdapter("animationButtonRight")
    fun setButtonAnimationRight(materialButton: MaterialButton, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(materialButton.context, R.anim.anim_from_right)
            animation.repeatMode = Animation.INFINITE
            materialButton.startAnimation(animation)
        }
    }

    @JvmStatic
    @BindingAdapter("animationButtonPopIn")
    fun setButtonAnimationPopIn(materialButton: MaterialButton, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(materialButton.context, R.anim.pop_up)
            animation.repeatMode = Animation.INFINITE
            materialButton.startAnimation(animation)
        }
    }


    @JvmStatic
    @BindingAdapter("animationTextViewFadeIn")
    fun setTextViewFadeIn(textView: TextView, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(textView.context, R.anim.fade_in)
            animation.repeatMode = Animation.INFINITE
            textView.startAnimation(animation)
        }
    }

    @JvmStatic
    @BindingAdapter("animationTextViewLeftIn")
    fun setTextViewFromLeftIn(textView: TextView, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(textView.context, R.anim.anim_from_left)
            animation.repeatMode = Animation.INFINITE
            textView.startAnimation(animation)
        }
    }


    @JvmStatic
    @BindingAdapter("animationTextViewPopIn")
    fun setTextViewPopIn(textView: TextView, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(textView.context, R.anim.pop_up)
            animation.repeatMode = Animation.INFINITE
            textView.startAnimation(animation)
        }
    }


    @JvmStatic
    @BindingAdapter("animationCard")
    fun setCardLeftAnimation(cardView: MaterialCardView, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(cardView.context, R.anim.anim_from_left)
            animation.repeatMode = Animation.INFINITE
            cardView.startAnimation(animation)
        }
    }

    @JvmStatic
    @BindingAdapter("animationCardRight")
    fun setCardRightAnimation(cardView: MaterialCardView, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(cardView.context, R.anim.anim_from_right)
            animation.repeatMode = Animation.INFINITE
            cardView.startAnimation(animation)
        }
    }


    @JvmStatic
    @BindingAdapter("animationCardPopIn")
    fun setCardPopInAnimation(cardView: MaterialCardView, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(cardView.context, R.anim.pop_up)
            animation.repeatMode = Animation.INFINITE
            cardView.startAnimation(animation)
        }
    }


    @JvmStatic
    @BindingAdapter("animationText")
    fun setTextViewAnimation(textView: TextView, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(textView.context, R.anim.anim_from_left)
            animation.repeatMode = Animation.INFINITE
            textView.startAnimation(animation)
        }
    }

    @JvmStatic
    @BindingAdapter("animationImageViewPopIn")
    fun setImageViewPopIn(imageView: ImageView, isAnimation: Boolean) {
        if (isAnimation) {
            val animation =
                AnimationUtils.loadAnimation(imageView.context, R.anim.pop_up)
            animation.repeatMode = Animation.INFINITE
            imageView.startAnimation(animation)
        }
    }


}