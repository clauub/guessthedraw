package com.quess.thing.util

interface IOnBackPressed {

    fun onBackPressed()

}