package com.quess.thing.util.drawing

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import com.divyanshu.draw.R
import com.divyanshu.draw.widget.DrawView
import com.google.android.gms.ads.AdView
import com.mikhaellopez.circleview.CircleView

class ColorSelector(
    root: View,
    drawView: Int,
    black: Int,
    red: Int,
    yellow: Int,
    green: Int,
    blue: Int,
    pink: Int,
    brown: Int,
    circleImageWidth: Int
) {

    private val black: ImageView = root.findViewById(black)
    private val red: ImageView = root.findViewById(red)
    private val yellow: ImageView = root.findViewById(yellow)
    private val green: ImageView = root.findViewById(green)
    private val blue: ImageView = root.findViewById(blue)
    private val pink: ImageView = root.findViewById(pink)
    private val brown: ImageView = root.findViewById(brown)
    private val circleImageViewWidth: com.quess.thing.ui.widget.CircleView =
        root.findViewById(circleImageWidth)
    private val drawView: DrawView = root.findViewById(drawView)

    fun colorSelector(context: Context) {
        black.setOnClickListener {
            val color = context.getColor(R.color.color_black)
            drawView.setColor(color)
            circleImageViewWidth.setColor(color)
            scaleColorView(black)
        }
        red.setOnClickListener {
            val color = context.getColor(R.color.color_red)
            drawView.setColor(color)
            circleImageViewWidth.setColor(color)
            scaleColorView(red)
        }
        yellow.setOnClickListener {
            val color = context.getColor(R.color.color_yellow)
            drawView.setColor(color)
            circleImageViewWidth.setColor(color)
            scaleColorView(yellow)
        }
        green.setOnClickListener {
            val color = context.getColor(R.color.color_green)
            drawView.setColor(color)
            circleImageViewWidth.setColor(color)
            scaleColorView(green)
        }
        blue.setOnClickListener {
            val color = context.getColor(R.color.color_blue)
            drawView.setColor(color)
            circleImageViewWidth.setColor(color)
            scaleColorView(blue)
        }
        pink.setOnClickListener {
            val color = context.getColor(R.color.color_pink)
            drawView.setColor(color)
            circleImageViewWidth.setColor(color)
            scaleColorView(pink)
        }
        brown.setOnClickListener {
            val color = context.getColor(R.color.color_brown)
            drawView.setColor(color)
            circleImageViewWidth.setColor(color)
            scaleColorView(brown)
        }
    }

    private fun scaleColorView(view: View) {
        //reset scale of all views
        black.scaleX = 1f
        black.scaleY = 1f

        red.scaleX = 1f
        red.scaleY = 1f

        yellow.scaleX = 1f
        yellow.scaleY = 1f

        green.scaleX = 1f
        green.scaleY = 1f

        blue.scaleX = 1f
        blue.scaleY = 1f

        pink.scaleX = 1f
        pink.scaleY = 1f

        brown.scaleX = 1f
        brown.scaleY = 1f

        //set scale of selected view
        view.scaleX = 1.5f
        view.scaleY = 1.5f
    }

    fun setUpDrawingTools() {

    }

}