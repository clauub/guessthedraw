package com.quess.thing.util

interface FirebaseSuccessListener {

    fun onDataFound(isDataFetched: Boolean)
}
