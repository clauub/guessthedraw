package com.quess.thing.util.analytics

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.quess.thing.ui.signin.SignInViewModelDelegate
import com.quess.thing.util.SharedPref
import timber.log.Timber

/**
 * Firebase Analytics implementation of AnalyticsHelper
 */
class FirebaseAnalyticsHelper(
    signInViewModelDelegate: SignInViewModelDelegate,
    context: Context
) : AnalyticsHelper {


    private val UPROP_USER_SIGNED_IN = "user_signed_in"
    private val UPROP_USER_REGISTERED = "user_registered"

    private var firebaseAnalytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(context)

    /**
     * stores a strong reference to preference change][PreferenceManager]
     */
    private var prefListener: SharedPreferences.OnSharedPreferenceChangeListener? = null

    /**
     * Log a specific screen view under the `screenName` string.
     */
    private val FA_CONTENT_TYPE_SCREENVIEW = "screen"
    private val FA_KEY_UI_ACTION = "ui_action"
    private val FA_CONTENT_TYPE_UI_EVENT = "ui event"

    private var analyticsEnabled: Boolean = false
        set(enabled) {
            field = enabled
            Timber.d("Setting Analytics enabled: $enabled")
            firebaseAnalytics.setAnalyticsCollectionEnabled(enabled)
        }

    /**
     * Initialize Analytics tracker.  If the user has permitted tracking and has already signed TOS,
     * (possible except on first run), initialize analytics Immediately.
     */
    init {
        Timber.d("Analytics initialized")

        // The listener will initialize Analytics when the TOS is signed, or enable/disable
        // Analytics based on the "anonymous data collection" setting.
        setupPreferenceChangeListener(context)

        signInViewModelDelegate.observeSignedInUser().observeForever { signedIn ->
            setUserSignedIn(signedIn == true)
            Timber.d("Updated user signed in to $signedIn")
        }

    }

    override fun sendScreenView(screenName: String, activity: Activity) {
        val params = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, screenName)
            putString(FirebaseAnalytics.Param.CONTENT_TYPE, FA_CONTENT_TYPE_SCREENVIEW)
        }
        firebaseAnalytics.run {
            setCurrentScreen(activity, screenName, null)
            logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params)
            Timber.d("Screen View recorded: $screenName")
        }
    }

    override fun logUiEvent(itemId: String, action: String) {
        val params = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, itemId)
            putString(FirebaseAnalytics.Param.CONTENT_TYPE, FA_CONTENT_TYPE_UI_EVENT)
            putString(FA_KEY_UI_ACTION, action)
        }
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params)
        Timber.d("Event recorded for $itemId, $action")
    }


    override fun setUserSignedIn(isSignedIn: Boolean) {
        firebaseAnalytics.setUserProperty(UPROP_USER_SIGNED_IN, isSignedIn.toString())
    }

    override fun setUserRegistered(isRegistered: Boolean) {
        firebaseAnalytics.setUserProperty(UPROP_USER_REGISTERED, isRegistered.toString())
    }

    /**
     * Set up a listener for preference changes.
     */
    private fun setupPreferenceChangeListener(context: Context) {
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { pref, key ->
            val action = try {
                getBooleanPreferenceAction(pref, key)
            } catch (e: ClassCastException) {
                return@OnSharedPreferenceChangeListener
            }

            if (key == SharedPref.PREF_SEND_USAGE_STATISTICS) {
                val sendStats = pref.getBoolean(key, false)
                analyticsEnabled = sendStats
            } else {
                logUiEvent("Preference: $key", action)
            }
        }

        SharedPref(context).registerOnPreferenceChangeListener(listener)
        prefListener = listener
        Timber.d("Preference Change Listener has been set up.")
    }

    private fun getBooleanPreferenceAction(prefs: SharedPreferences, key: String): String {
        return if (prefs.getBoolean(key, true)) AnalyticsActions.ENABLE
        else AnalyticsActions.DISABLE
    }
}
