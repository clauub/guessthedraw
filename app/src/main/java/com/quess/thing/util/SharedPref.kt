package com.quess.thing.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.objects.Constants.NAME


class SharedPref(context: Context) {

    private val appContext = context.applicationContext
    private val prefs =
        context.applicationContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)

    fun saveCoins(key: String, value: Int) {
        preference.edit().putInt(key, value).apply()
    }

    fun saveStringShared(key: String, value: String) {
        preference.edit().putString(key, value).apply()
    }

    fun getStringShared(key: String): String? {
        return preference.getString(key, "null")
    }

    fun getCoins(key: String): Int {
        return preference.getInt(key, 0)
    }

    fun getLocked1(key: String): Boolean {
        return preference.getBoolean(key, true)
    }

    fun preferenceBoolean(key: String, value: Boolean) {
        preference.edit().putBoolean(key, value).apply()
    }

    fun registerOnPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        prefs.registerOnSharedPreferenceChangeListener(listener)
    }

    companion object {
        const val PREFS_NAME = "guessthing"
        const val PREF_SEND_USAGE_STATISTICS = "pref_send_usage_statistics"
    }


}