@file:Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.quess.thing.util

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.quess.thing.R
import com.quess.thing.databinding.NoInternetConnectionDialogBinding
import com.quess.thing.util.objects.Navigation.goToAnswerFragment

fun View.showSnackbarWithColor(snackbarText: String, timeLength: Int, @ColorInt colorInt: Int) {
    Snackbar.make(this, snackbarText, timeLength).run {
        addCallback(object : Snackbar.Callback() {
            override fun onShown(sb: Snackbar?) {
//                EspressoIdlingResource.increment()
            }

            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
//                EspressoIdlingResource.decrement()
            }
        })
            .withColor(colorInt)
        show()
    }
}

fun View.showSnackbarWithColorAndAction(
    snackbarText: String,
    timeLength: Int, @ColorInt colorInt: Int,
    textAction: String,
    dialogFragment: DialogFragment,
    fragmentActivity: FragmentActivity
) {
    Snackbar.make(this, snackbarText, timeLength).run {
        addCallback(object : Snackbar.Callback() {
            override fun onShown(sb: Snackbar?) {
//                EspressoIdlingResource.increment()
            }

            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
//                EspressoIdlingResource.decrement()
            }

        })
            .withColor(colorInt)
            .setAction(textAction) {
                it.setOnClickListener {
                    dialogFragment.show(fragmentActivity.supportFragmentManager, "DIALOG")
                }
            }

        show()
    }
}

fun View.showSnackbar(snackbarText: String, timeLength: Int) {
    Snackbar.make(this, snackbarText, timeLength).run {
        addCallback(object : Snackbar.Callback() {
            override fun onShown(sb: Snackbar?) {
//                EspressoIdlingResource.increment()
            }

            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
//                EspressoIdlingResource.decrement()
            }
        })
        show()
    }
}

fun View.setupSnackbar(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<Int>>,
    timeLength: Int
) {

    snackbarEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let {
            showSnackbar(context.getString(it), timeLength)
        }
    })
}


fun View.setupSnackbarWithCustomColor(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<Int>>,
    timeLength: Int,
    colorInt: Int
) {

    snackbarEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let {
            showSnackbarWithColor(context.getString(it), timeLength, colorInt)
        }
    })
}

fun View.setupSnackbarWithCustomColorAndAction(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<Int>>,
    timeLength: Int,
    colorInt: Int,
    textAction: String,
    dialogFragment: DialogFragment,
    fragmentActivity: FragmentActivity
) {

    snackbarEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let {
            showSnackbarWithColorAndAction(
                context.getString(it),
                timeLength,
                colorInt,
                textAction,
                dialogFragment,
                fragmentActivity
            )
        }
    })
}

fun View.showToast(toastText: String, timeLength: Int) {
    Toast.makeText(context, toastText, timeLength).run {
        show()
    }
}

fun View.setupToast(
    lifecycleOwner: LifecycleOwner,
    toastEvent: LiveData<Event<Int>>,
    timeLength: Int
) {
    toastEvent.observe(lifecycleOwner, Observer { it ->
        it.getContentIfNotHandled()?.let {
            showToast(context.getString(it), timeLength)
        }

    })
}

@SuppressLint("InflateParams")
fun alertDialogMoreTry(activity: Activity, view: View, bundle: Bundle) {
    val dialogBuilder = MaterialAlertDialogBuilder(activity)

    val inflater = activity.layoutInflater
    val dialogView = inflater.inflate(R.layout.alert_dialog_singleplayer, null)
    dialogBuilder.setView(dialogView)

        .setPositiveButton(activity.getString(R.string.ok)) { _, _ ->
            goToAnswerFragment(view, bundle)
        }
        .setNegativeButton(activity.getString(R.string.no)) { i, _ ->
            i.dismiss()
        }

    dialogBuilder.create()
    dialogBuilder.show()
}

fun alertDialogNoInternetConnection(context: Context) {
    val binding: NoInternetConnectionDialogBinding = DataBindingUtil
        .inflate(
            LayoutInflater.from(context),
            R.layout.no_internet_connection_dialog,
            null,
            false
        )

    val dialog = Dialog(context)
    dialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
    dialog.setContentView(binding.root)

    binding.okBtn.setOnClickListener {
        dialog.dismiss()
    }

    dialog.show()
}

fun toastLong(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}

fun snackBarLong(view: View, message: Int) {
    Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
}

fun snackBarCustomColor(view: View, message: Int, colorInt: Int) {
    Snackbar.make(view, message, Snackbar.LENGTH_LONG).withColor(colorInt).show()
}

fun snackBarLongWithAction(
    view: View,
    message: Int,
    textAction: String,
    dialogFragment: DialogFragment,
    fragmentActivity: FragmentActivity
) {
    Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        .setAction(textAction) {
            dialogFragment.show(fragmentActivity.supportFragmentManager, "DIALOG")
        }
        .show()
}


private fun Snackbar.withColor(@ColorInt colorInt: Int): Snackbar {
    this.view.setBackgroundColor(colorInt)
    return this
}
