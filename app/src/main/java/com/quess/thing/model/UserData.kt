package com.quess.thing.model

import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.database.PropertyName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@IgnoreExtraProperties
data class UserData(
    var uid: String? = null,
    var name: String? = null,
    var email: String? = null,
    var photoURL: String? = null,
    var isSearching: Boolean? = null,
    var wins: Int? = 0,
    var loses: Int? = 0,
    var isPlaying: Boolean? = null,
    var challenge: Challenge? = null,
    var game_request: GameRequest? = null,
    var game: Game? = null,
    @Exclude
    var isNew: Boolean? = null
) : Serializable

@IgnoreExtraProperties
data class GameRequest(
    @PropertyName("uid")
    var uid: String? = null,
    @PropertyName("name")
    var name: String? = null,
    @PropertyName("photoURL")
    var photoURL: String? = null,
    @PropertyName("timestamp")
    var timestamp: Long? = null
) : Serializable

@IgnoreExtraProperties
data class Challenge(
    @PropertyName("uid")
    var uid: String? = null,
    @PropertyName("name")
    var name: String? = null,
    @PropertyName("photoURL")
    var photoURL: String? = null,
    @PropertyName("timestamp")
    var timestamp: Long? = null
) : Serializable

@IgnoreExtraProperties
@Parcelize
data class Game(
    @PropertyName("uid")
    var uid: String? = null,
    @PropertyName("name")
    var name: String? = null,
    @PropertyName("timestamp")
    var timestamp: Long? = null,
    @PropertyName("word_to_draw")
    var word_to_draw: String? = null,
    @PropertyName("win")
    var win: String? = null,
    @PropertyName("draw_image")
    var draw_image: String? = null
) : Parcelable

@IgnoreExtraProperties
@Parcelize
data class Invitation(
    @PropertyName("uid")
    var uid: String? = null,
    @PropertyName("name")
    var name: String? = null,
    @PropertyName("invitation_number")
    var invitation_number: String? = null
) : Parcelable