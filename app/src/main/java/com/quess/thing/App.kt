package com.quess.thing

import androidx.multidex.MultiDex
import com.google.android.gms.ads.MobileAds
import com.quess.thing.di.components.DaggerAppComponent
import com.quess.thing.util.CrashlyticsTree
import com.quess.thing.util.analytics.AnalyticsHelper
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber
import javax.inject.Inject

class App : DaggerApplication() {
    // Even if the var isn't used, needs to be initialized at application startup.
    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    override fun onCreate() {
        super.onCreate()

        MultiDex.install(this)
        MobileAds.initialize(this, "ca-app-pub-2854051253644042~5610758091")

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashlyticsTree())
        }
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }
}