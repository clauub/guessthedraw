package com.quess.thing.di.modules

import android.app.Application
import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.quess.thing.App
import com.quess.thing.ui.signin.SignInViewModelDelegate
import com.quess.thing.ui.signin.datasource.ObserveUserAuthStateUseCase
import com.quess.thing.util.SharedPref
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.analytics.FirebaseAnalyticsHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    fun provideContext(context: App): Context {
        return context.applicationContext
    }

    @Singleton
    @Provides
    fun provideApplication(application: Application): Application {
        return application
    }

    @Singleton
    @Provides
    fun providesPreferenceStorage(context: Context): SharedPref =
        SharedPref(context)

    @Singleton
    @Provides
    fun providesAnalyticsHelper(
        signInDelegate: SignInViewModelDelegate,
        context: Context
    ): AnalyticsHelper = FirebaseAnalyticsHelper(signInDelegate, context)

    @Singleton
    @Provides
    fun provideSignInViewModelDelegate(
        dataSource: ObserveUserAuthStateUseCase,
        firebaseAuth: FirebaseAuth
    ): SignInViewModelDelegate {
        return SignInViewModelDelegate.FirebaseSignInViewModelDelegate(dataSource, firebaseAuth)
    }
}
