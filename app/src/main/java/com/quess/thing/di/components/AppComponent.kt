package com.quess.thing.di.components

import android.app.Application
import com.quess.thing.App
import com.quess.thing.di.modules.*
import com.quess.thing.di.modules.SignInModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


/**
 *
 * This is the main injection class that injects [learn.app.tech.di.components] Activities, and android modules.
 */
@Singleton
@Component(
    modules = [ActivityBindingModule::class,
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        SignInModule::class,
        NetworkModule::class, AppModule::class, ServiceBuilderModule::class]
)
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}