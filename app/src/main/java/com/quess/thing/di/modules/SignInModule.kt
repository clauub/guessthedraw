package com.quess.thing.di.modules

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.quess.thing.ui.signin.datasource.AuthStateUserDataSource
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.ui.signin.datasource.FirebaseAuthStateUserDataSource
import com.quess.thing.util.SharedPref
import com.quess.thing.util.signin.SignInHandler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class SignInModule {

    @Provides
    fun provideSignInHandler(): SignInHandler = SignInHandler.DefaultSignInHandler()

    @Singleton
    @Provides
    fun provideFirebaseFirestore(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    @Singleton
    @Provides
    fun provideFirebaseDatabase(): FirebaseDatabase {
        return FirebaseDatabase.getInstance()
    }

    @Singleton
    @Provides
    fun provideAuthStateUserDataSource(
        firebaseAuth: FirebaseAuth,
        firestore: FirebaseDatabase,
        sharedPref: SharedPref
    ): AuthStateUserDataSource {
        return FirebaseAuthStateUserDataSource(
            firebaseAuth,
            DatabaseUserRepository(
                firestore,
                firebaseAuth,
                sharedPref
            )
        )
    }

    @Singleton
    @Provides
    fun provideFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Singleton
    @Provides
    fun provideFirebaseStorage(): FirebaseStorage {
        return FirebaseStorage.getInstance()
    }
}
