package com.quess.thing.di.modules

import com.quess.thing.util.OnClearFromRecentService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeMyService(): OnClearFromRecentService


}
