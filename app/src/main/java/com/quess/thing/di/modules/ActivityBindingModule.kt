package com.quess.thing.di.modules

import com.quess.thing.di.scopes.ActivityScoped
import com.quess.thing.ui.gameplay.game_dialogs.check_invitation_number.CheckInvitationNumberModule
import com.quess.thing.ui.gameplay.game_dialogs.no_coins.NoCoinsModule
import com.quess.thing.ui.gameplay.game_dialogs.show_first_letter.ShowFirstLetterModule
import com.quess.thing.ui.gameplay.game_dialogs.show_word.ShowWordDialogModule
import com.quess.thing.ui.gameplay.multiplayer_game.create_invitation.CreateInvitationModule
import com.quess.thing.ui.gameplay.multiplayer_game.lost.LostActivity
import com.quess.thing.ui.gameplay.multiplayer_game.lost.LostModule
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer.MultiplayerActivity
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer.MultiplayerModule
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.MultiplayerAnswerActivity
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.MultiplayerAnswerModule
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.MultiplayerAnswerWaitingModule
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game.MultiplayerGameActivity
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game.MultiplayerGameModule
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game.MultiplayerGameWaitingModule
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game.SelectWordToDrawModule
import com.quess.thing.ui.gameplay.multiplayer_game.search_random_player.SearchRandomPlayerModule
import com.quess.thing.ui.gameplay.multiplayer_game.winner.WinnerActivity
import com.quess.thing.ui.gameplay.multiplayer_game.winner.WinnerModule
import com.quess.thing.ui.gameplay.offline_gameplay.GameModule
import com.quess.thing.ui.gameplay.offline_gameplay.answer.AnswerModule
import com.quess.thing.ui.gameplay.offline_gameplay.answer.OneMoreTryModule
import com.quess.thing.ui.main.MainActivity
import com.quess.thing.ui.main.MainActivityModule
import com.quess.thing.ui.main.MainModule
import com.quess.thing.ui.gameplay.offline_gameplay.category.CategoryModule
import com.quess.thing.ui.gameplay.offline_gameplay.category.UnlockCardModule
import com.quess.thing.ui.main.settings.SettingsModule
import com.quess.thing.ui.main.shop.ShopModule
import com.quess.thing.ui.main.splash.SplashActivity
import com.quess.thing.ui.main.splash.SplashModule
import com.quess.thing.ui.gameplay.offline_gameplay.word.WordModule
import com.quess.thing.ui.signin.SignInDialogModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            MainActivityModule::class,
            MainModule::class,
            CategoryModule::class,
            WordModule::class,
            GameModule::class,
            AnswerModule::class,
            SignInDialogModule::class,
            OneMoreTryModule::class,
            UnlockCardModule::class,
            LostModule::class,
            WinnerModule::class,
            ShopModule::class,
            ShowFirstLetterModule::class,
            NoCoinsModule::class,
            ShowWordDialogModule::class,
            SettingsModule::class]
    )
    internal abstract fun mainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            SignInDialogModule::class,
            MultiplayerModule::class,
            SearchRandomPlayerModule::class,
            CheckInvitationNumberModule::class,
            CreateInvitationModule::class
        ]
    )
    internal abstract fun multiplayerActivity(): MultiplayerActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            MultiplayerGameModule::class,
            SelectWordToDrawModule::class,
            MultiplayerGameWaitingModule::class
        ]
    )
    internal abstract fun multiplayerGameActivity(): MultiplayerGameActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            MultiplayerAnswerModule::class,
            MultiplayerAnswerWaitingModule::class,
            OneMoreTryModule::class,
            ShowFirstLetterModule::class,
            ShowWordDialogModule::class,
            NoCoinsModule::class
        ]
    )
    internal abstract fun multiplayerAnswerActivity(): MultiplayerAnswerActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [LostModule::class, MainModule::class])
    internal abstract fun lostActivity(): LostActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [WinnerModule::class, MainModule::class])
    internal abstract fun winnerActivity(): WinnerActivity


    @ActivityScoped
    @ContributesAndroidInjector(modules = [SplashModule::class])
    internal abstract fun splashActivity(): SplashActivity

}
