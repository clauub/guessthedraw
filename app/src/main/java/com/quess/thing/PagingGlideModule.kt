/*
 * Copyright (c) 2019 Bășcărău Claudiu. All rights reserved.
 *
 * This file is part of PapeWall project.
 *
 *  PapeWall project can not be copied and/or distributed without the express permission of the author.
 *
 */

package com.quess.thing

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Ahmed Abd-Elmeged on 2/20/2018.
 */
@GlideModule
class PagingGlideModule : AppGlideModule()
