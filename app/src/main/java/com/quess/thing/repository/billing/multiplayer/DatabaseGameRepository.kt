package com.quess.thing.repository.billing.multiplayer

import android.annotation.SuppressLint
import android.content.Context
import android.widget.GridView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.quess.thing.databinding.FragmentMultiplayerAnswerBinding
import com.quess.thing.databinding.FragmentMultiplayerBinding
import com.quess.thing.model.Game
import com.quess.thing.model.Invitation
import com.quess.thing.model.UserData
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.MultiplayerAnswerViewModel
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter.GridViewLetterAdapterMultiplayer
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter.GridViewLetterClickListener
import com.quess.thing.util.FirebaseSuccessListener
import com.quess.thing.util.objects.Constants.DRAW_IMAGE
import com.quess.thing.util.objects.Constants.GAME
import com.quess.thing.util.objects.Constants.IMAGES
import com.quess.thing.util.objects.Constants.INVITATIONS
import com.quess.thing.util.objects.Constants.INVITATION_NUMBER
import com.quess.thing.util.objects.Constants.LOSES
import com.quess.thing.util.objects.Constants.UID
import com.quess.thing.util.objects.Constants.USERS
import com.quess.thing.util.objects.Constants.WIN
import com.quess.thing.util.objects.Constants.WINS
import com.quess.thing.util.objects.Constants.WORD_TO_DRAW
import com.quess.thing.util.objects.Utils.shuffleLettersFromWord
import com.quess.thing.util.objects.Utils.splitString
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class DatabaseGameRepository @Inject constructor(
    firebaseDatabase: FirebaseDatabase,
    firebaseAuth: FirebaseAuth,
    firebaseStorage: FirebaseStorage
) {

    @SuppressLint("SimpleDateFormat")
    val fileNewName: String = SimpleDateFormat("yyyy-MM-dd-HHmm").format(Date())

    val usersRef = firebaseDatabase.reference.child(USERS)
    private val invitationsRef = firebaseDatabase.reference.child(INVITATIONS)
    private val storageRef = firebaseStorage.reference.child(IMAGES).child("img-$fileNewName.jpg")
    private val firebaseUser = firebaseAuth.currentUser

    // UID specific to the provider
    val currentUID = firebaseUser?.uid

    // Name, email address, and profile photo Url
    val name = firebaseUser?.displayName
    val photoUrl = firebaseUser?.photoUrl.toString()
    var start = System.currentTimeMillis()
    var receiverID: String? = null
    var wins: Int? = null
    var end = start + 25 * 1000 // 25 seconds

    fun createGameInvitationRoom(invitationNumber: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val invitation = Invitation(currentUID, name, invitationNumber)
            invitationsRef.child(currentUID!!).setValue(invitation)
        }
    }

    fun checkInvitationNumber(invitationNumber: String) {
        CoroutineScope(Dispatchers.IO).launch {
            invitationsRef.orderByChild("invitation_number").equalTo(invitationNumber)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponent = data.getValue(UserData::class.java)
                            val userID = p0.child(opponent?.uid!!).child("uid").value.toString()

                            val gamePlayer1 =
                                Game(
                                    userID,
                                    opponent.name,
                                    System.currentTimeMillis()
                                )
                            val gamePlayer2 = Game(currentUID, name, System.currentTimeMillis())

                            accessCurrentUserGameChild(userID).setValue(gamePlayer1)
                            accessReceiverUserGameChild(userID).setValue(gamePlayer2)
                            invitationsRef.child(userID).removeValue()
                        }
                    }

                })
        }
    }

    fun checkIfGameExist(dataFetched: FirebaseSuccessListener) {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            if (data.exists()) {
                                dataFetched.onDataFound(true)
                            }
                        }
                    }
                })
        }
    }

    fun checkIfDrawImageExists(dataFetched: FirebaseSuccessListener) {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo!!.uid!!
                            val wordToDraw =
                                p0.child(receiverID!!).child(DRAW_IMAGE)

                            if (wordToDraw.exists()) {
                                dataFetched.onDataFound(true)
                            }
                        }
                    }
                })
        }
    }

    fun ifCurrentUserWon(dataFetched: FirebaseSuccessListener) {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo!!.uid!!

                            val winnerID =
                                p0.child(receiverID!!).child(WIN).value

                            if (p0.child(receiverID!!).child(WIN).exists()) {
                                if (winnerID == currentUID) {
                                    dataFetched.onDataFound(true)
                                }
                            }
                        }
                    }

                })
        }
    }

    fun ifCurrentUserLost(dataFetched: FirebaseSuccessListener) {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo!!.uid!!

                            val winnerID =
                                p0.child(receiverID!!).child(WIN).value

                            if (p0.child(receiverID!!).child(WIN).exists()) {
                                if (winnerID != currentUID) {
                                    dataFetched.onDataFound(true)
                                }
                            }
                        }
                    }

                })
        }
    }

    fun getGameRequest(dataFetched: FirebaseSuccessListener) {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child("game_request")
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)

                            if (data.exists()) {
                                Timber.d("Show dialog with game request")
                                dataFetched.onDataFound(true)
                            }
                            Timber.d(opponentInfo!!.name)
                        }
                    }
                })
        }
    }

    fun deleteGameRequestAndChallenge() {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child("game_request")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)

                            usersRef.child(currentUID).child("game_request").removeValue()
                            usersRef.child(opponentInfo!!.uid!!).child("challenge").removeValue()

                        }
                    }
                })
        }
    }

    fun saveWordToDrawGame(word: String) {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo!!.uid!!

                            accessCurrentUserGameChild(receiverID!!).child(WORD_TO_DRAW)
                                .setValue(word)
                            accessReceiverUserGameChild(receiverID!!).child(WORD_TO_DRAW)
                                .setValue(word)
                        }
                    }

                })
        }
    }

    fun createGame() {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child("game_request")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            val opponentID = opponentInfo!!.uid

                            val gamePlayer1 =
                                Game(
                                    opponentInfo.uid,
                                    opponentInfo.name,
                                    System.currentTimeMillis()
                                )
                            val gamePlayer2 = Game(currentUID, name, System.currentTimeMillis())

                            usersRef.child(currentUID).child("game_request").removeValue()
                            accessCurrentUserGameChild(opponentID!!).setValue(gamePlayer1)
                            usersRef.child(currentUID).child("isPlaying").setValue(true)

                            usersRef.child(opponentID).child("challenge").removeValue()
                            accessReceiverUserGameChild(opponentID).setValue(gamePlayer2)
                            usersRef.child(opponentID).child("isPlaying").setValue(true)

                        }
                    }
                })
        }
    }

    fun getWordFromDatabase(wordTextView: TextView) {
        CoroutineScope(Dispatchers.IO).apply {
            usersRef.child(currentUID!!).child(GAME)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo!!.uid!!

                            val wordToDraw =
                                p0.child(receiverID!!).child(WORD_TO_DRAW).value.toString()

                            wordTextView.text = wordToDraw

                        }
                    }
                })
        }
    }

    fun getDataForMultiplayerAnswer(
        gridView: GridView,
        listener: GridViewLetterClickListener,
        context: Context,
        binding: FragmentMultiplayerAnswerBinding,
        vm: MultiplayerAnswerViewModel
    ) {
        CoroutineScope(Dispatchers.IO).apply {
            usersRef.child(currentUID!!).child(GAME)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo!!.uid!!

                            val wordToDraw =
                                p0.child(receiverID!!).child(WORD_TO_DRAW).value.toString()
                            val imageUrl =
                                p0.child(receiverID!!).child(DRAW_IMAGE).value.toString()

                            val shuffledWord = shuffleLettersFromWord(wordToDraw)

                            binding.wordText = wordToDraw
                            binding.imageUrl = imageUrl

                            val adapter =
                                GridViewLetterAdapterMultiplayer(
                                    context,
                                    splitString(shuffledWord) as ArrayList<String>, listener, vm
                                )
                            gridView.adapter = adapter
                        }
                    }
                })
        }
    }

    fun getCurrentUserInfo(binding: FragmentMultiplayerBinding) {
        usersRef.child(currentUID!!).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                val wins = p0.child("wins").value.toString()
                val loses = p0.child("loses").value.toString()

                binding.wins = wins
                binding.loses = loses
                binding.imageUrl = photoUrl
                binding.username = name

            }

        })

    }

    fun setCurrentUserIDWinner() {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo!!.uid!!

                            accessCurrentUserGameChild(receiverID!!).child(WIN).setValue(currentUID)
                            accessReceiverUserGameChild(receiverID!!).child(WIN)
                                .setValue(currentUID)

                            calculateCurrentUserWins()
                            calculateOpponentUserLoses(receiverID!!)
                        }
                    }
                })
        }
    }

    fun setOpponentUserIDWinner() {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        if (p0.exists()) {
                            for (data in p0.children) {
                                val opponentInfo = data.getValue(UserData::class.java)
                                receiverID = opponentInfo!!.uid!!

                                accessCurrentUserGameChild(receiverID!!).child(WIN)
                                    .setValue(receiverID)
                                accessReceiverUserGameChild(receiverID!!).child(WIN)
                                    .setValue(receiverID)

                                calculateCurrentUserLoses()
                                calculateOpponentUserWins(receiverID!!)
                            }
                        }
                    }
                })
        }
    }

    fun deleteChallengeAndGameRequest() {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child("challenge")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)

                            usersRef.child(opponentInfo!!.uid!!).child("game_request")
                                .removeValue()
                            usersRef.child(currentUID).child("challenge").removeValue()

                        }
                    }
                })
        }
    }

    fun deleteGameChild() {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        if (p0.exists()) {
                            usersRef.child(currentUID).child(GAME).removeValue()
                        }
                    }

                })
        }
    }

    fun deleteInvitationChild() {
        CoroutineScope(Dispatchers.IO).launch {
            invitationsRef.child(currentUID!!).removeValue()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun saveImageDrawingToStorage(imageUri: ByteArray) {
        storageRef.putBytes(imageUri).addOnSuccessListener {
            storageRef.downloadUrl.addOnSuccessListener { p0 -> saveImageToFirebase(p0.toString()) }
        }
    }

    private fun saveImageToFirebase(downlandLink: String) {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(currentUID!!).child(GAME)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (data in p0.children) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo!!.uid!!

                            accessReceiverUserGameChild(receiverID!!).child(DRAW_IMAGE)
                                .setValue(downlandLink)
                            accessCurrentUserGameChild(receiverID!!).child(DRAW_IMAGE)
                                .setValue(downlandLink)
                        }
                    }

                })
        }
    }

    fun accessReceiverUserGameChild(receiverID: String): DatabaseReference {
        return usersRef.child(receiverID).child(GAME).child(currentUID!!)
    }

    fun accessCurrentUserGameChild(receiverID: String): DatabaseReference {
        return usersRef.child(currentUID!!).child(GAME).child(receiverID)
    }

    fun calculateCurrentUserWins() {
        usersRef.child(currentUID!!).child(WINS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(p0: DataSnapshot) {
                    val wins = p0.value.toString()
                    val winsCalculate = wins.toInt() + 1

                    usersRef.child(currentUID).child(WINS).setValue(winsCalculate)
                }

            })
    }

    fun calculateOpponentUserWins(receiverID: String) {
        usersRef.child(receiverID).child(WINS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(p0: DataSnapshot) {
                    val wins = p0.value.toString()
                    val winsCalculate = wins.toInt() + 1

                    usersRef.child(receiverID).child(WINS).setValue(winsCalculate)
                }

            })
    }

    fun calculateCurrentUserLoses() {
        usersRef.child(currentUID!!).child(LOSES)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(p0: DataSnapshot) {
                    val loses = p0.value.toString()
                    val winsCalculate = loses.toInt() + 1

                    usersRef.child(currentUID).child(LOSES).setValue(winsCalculate)
                }

            })
    }

    fun calculateOpponentUserLoses(receiverID: String) {
        usersRef.child(receiverID).child(LOSES)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(p0: DataSnapshot) {
                    val loses = p0.value.toString()
                    val winsCalculate = loses.toInt() + 1

                    usersRef.child(receiverID).child(LOSES).setValue(winsCalculate)
                }

            })
    }
}
