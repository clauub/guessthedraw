package com.quess.thing.repository.billing.multiplayer

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.quess.thing.model.Challenge
import com.quess.thing.model.GameRequest
import com.quess.thing.model.UserData
import com.quess.thing.util.FirebaseSuccessListener
import com.quess.thing.util.SharedPref
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.objects.Constants.LOSES
import com.quess.thing.util.objects.Constants.NAME
import com.quess.thing.util.objects.Constants.PHOTO_URL
import com.quess.thing.util.objects.Constants.USERS
import com.quess.thing.util.objects.Constants.WIN
import com.quess.thing.util.objects.Constants.WINS
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import javax.inject.Inject


class DatabaseUserRepository @Inject constructor(
    private val firebaseDatabase: FirebaseDatabase,
    private val firebaseAuth: FirebaseAuth,
    private val sharedPref: SharedPref
) {

    val usersRef = firebaseDatabase.reference.child(USERS)
    private val firebaseUser = firebaseAuth.currentUser
    // UID specific to the provider
    val uid = firebaseUser?.uid
    // Name, email address, and profile photo Url
    val name = firebaseUser?.displayName
    val photoUrl = firebaseUser?.photoUrl.toString()
    private val random = Random()
    var start = System.currentTimeMillis()
    var receiverID: String? = null
    var end = start + 25 * 1000 // 25 seconds

    fun addUser(displayUser: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val usersRef = firebaseDatabase.reference.child(USERS)
            val firebaseUser = firebaseAuth.currentUser
            // UID specific to the provider
            val uid = firebaseUser?.uid

            // Name, email address, and profile photo Url
            val email = firebaseUser?.email
            val photoUrl = firebaseUser?.photoUrl.toString()

            val user = UserData(uid!!, displayUser, email!!, photoUrl, true)

            usersRef.orderByChild("uid").equalTo(uid)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        if (p0.value != null) { // if user exist in firebase save it just to SharedPref
                            saveUserToShared(displayUser, photoUrl)
                        } else { // if user doesn't exist in database save it in database and local(SharedPref)
                            usersRef.child(uid).setValue(user)
                            saveUserToShared(displayUser, photoUrl)
                        }
                    }

                })
        }
    }

    fun checkIfUserFound(dataFetched: FirebaseSuccessListener) {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.child(uid!!).child("isPlaying")
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        val isPlaying = p0.value

                        if (isPlaying == true) {
                            dataFetched.onDataFound(true)
                        }
                    }
                })
        }
    }

    fun getOnlineUser() {
        CoroutineScope(Dispatchers.IO).launch {
            usersRef.orderByChild("searching").equalTo(true)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        val index = random.nextInt(p0.childrenCount.toInt())


                        for ((count, data) in p0.children.withIndex()) {
                            val opponentInfo = data.getValue(UserData::class.java)
                            receiverID = opponentInfo?.uid!!
                            val currentUsersChallenges =
                                p0.child(uid!!).child("challenge").childrenCount
                            val opponentGameRequests =
                                p0.child(receiverID!!).child("game_request").childrenCount
                            val opponentGameChild =
                                p0.child(receiverID!!).child("game").childrenCount
                            val currentUserGameChild =
                                p0.child(uid).child("game").childrenCount

                            if (System.currentTimeMillis() < end) {
                                if (count == index) {
                                    when {
                                        uid == receiverID -> {
                                            getOnlineUser()
                                            Timber.d("Random user is the current user, try again")
                                        }
                                        currentUsersChallenges > 0 -> {
                                            getOnlineUser()
                                            Timber.d("You already have a challenge")
                                        }
                                        opponentGameRequests > 0 -> {
                                            getOnlineUser()
                                            Timber.d("You already have a challenge")
                                        }
                                        opponentGameChild > 0 -> {
                                            getOnlineUser()
                                            Timber.d("User has a game opened")
                                        }
                                        currentUserGameChild > 0 -> {
                                            getOnlineUser()
                                            Timber.d("User a game opened")
                                        }
                                        else -> {
                                            Timber.d("Available user found")
                                            val detailChallenge = Challenge(
                                                receiverID,
                                                opponentInfo.name,
                                                opponentInfo.photoURL,
                                                System.currentTimeMillis()
                                            )
                                            val detailGameRequest = GameRequest(
                                                uid,
                                                name,
                                                photoUrl,
                                                System.currentTimeMillis()
                                            )

                                            usersRef.child(uid).child("challenge")
                                                .child(receiverID!!)
                                                .setValue(detailChallenge)

                                            usersRef.child(receiverID!!).child("game_request")
                                                .child(uid)
                                                .setValue(detailGameRequest)
                                        }
                                    }
                                    Timber.d(receiverID)
                                }
                            }
                        }
                    }
                })
        }
    }

    fun setUserIsSearching() {
        usersRef.child(uid!!).child("searching").setValue(true)
    }

    fun setUsersIsNotSearching() {
        usersRef.child(uid!!).child("searching").setValue(false)
    }

    fun setUserIsNotPlaying() {
        usersRef.child(uid!!).child("isPlaying").setValue(false)
    }

    fun setUserIsPlaying() {
        usersRef.child(uid!!).child("isPlaying").setValue(true)
    }

    fun saveUserToShared(displayUser: String, photoUrl: String) {
        sharedPref.saveStringShared(NAME, displayUser)
        sharedPref.saveStringShared(PHOTO_URL, photoUrl)
        sharedPref.saveStringShared(WINS, photoUrl)
        sharedPref.saveStringShared(LOSES, photoUrl)
    }
}

