package com.quess.thing.ui.gameplay.offline_gameplay.answer

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.GridView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.quess.thing.R
import com.quess.thing.databinding.FragmentAnswerBinding
import com.quess.thing.ui.gameplay.game_dialogs.no_coins.NoCoinsFragmentDialog
import com.quess.thing.ui.gameplay.game_dialogs.show_first_letter.ShowFirstLetterDialogFragment
import com.quess.thing.ui.gameplay.game_dialogs.show_word.ShowWordDialogFragment
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter.GridViewLetterClickListener
import com.quess.thing.ui.gameplay.offline_gameplay.answer.adapter.GridViewLetterAdapter
import com.quess.thing.util.EventObserver
import com.quess.thing.util.admob.AdUtil.loadAd
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Utils
import com.quess.thing.util.setupSnackbarWithCustomColor
import com.quess.thing.util.toastLong
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_multiplayer_answer.*
import timber.log.Timber
import javax.inject.Inject

class AnswerFragment : DaggerFragment(), GridViewLetterClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var answerViewModel: AnswerViewModel
    private var inputWord: String? = null

    @SuppressLint("DefaultLocale")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        answerViewModel = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentAnswerBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@AnswerFragment
            vm = this@AnswerFragment.answerViewModel
        }

        displayAds(dataBinding.root)
        removeAds(dataBinding.root)

        setAdapter(dataBinding.gridView)

        val givenWord = arguments?.getString("word")
        Timber.d("Your word : $givenWord + $inputWord")
        analyticsHelper.sendScreenView("Answer", requireActivity())

        answerViewModel.openOneMoreTryDialog.observe(viewLifecycleOwner, EventObserver {
            openOneMoreTryDialog()
        })

        answerViewModel.openFirstLetterDialog.observe(this, EventObserver {
            val firstLetter = givenWord?.substring(0, 1)
            openFirstLetterDialog(requireActivity(), firstLetter!!)
        })

        answerViewModel.openShowWordDialog.observe(viewLifecycleOwner, EventObserver {
            openShowWordDialog(requireActivity(), givenWord!!)
        })

        answerViewModel.deleteLetter.observe(viewLifecycleOwner, EventObserver {
            deleteLastLetter(dataBinding.editText)
        })

        answerViewModel.checkWord.observe(viewLifecycleOwner, EventObserver {
            inputWord = dataBinding.editText.text.toString()
            answerViewModel.checkWord(inputWord!!, givenWord!!, dataBinding.editText, view!!)
        })

        answerViewModel.lastCheckWord.observe(viewLifecycleOwner, EventObserver {
            inputWord = dataBinding.editText.text.toString()
            answerViewModel.lastCheckWord(inputWord!!, givenWord!!, view!!)
        })

        answerViewModel.openNoCoinsDialog.observe(viewLifecycleOwner, EventObserver {
            openNoCoinsDialog()
        })

        return dataBinding.root
    }

    private fun setAdapter(gridView: GridView) {
        val givenWord = arguments?.getString("word")
        val shuffledWord = Utils.shuffleLettersFromWord(givenWord!!)
        val adapter =
            GridViewLetterAdapter(
                context!!,
                Utils.splitString(shuffledWord) as ArrayList<String>, this, answerViewModel
            )

        gridView.adapter = adapter
    }

    private fun openNoCoinsDialog() {
        val dialog = NoCoinsFragmentDialog()
        dialog.show(requireActivity().supportFragmentManager, "NO_COINS_DIALOG")
    }

    private fun openFirstLetterDialog(
        activity: FragmentActivity,
        firstLetter: String
    ) {
        val dialog = ShowFirstLetterDialogFragment.newInstance(firstLetter)
        dialog.show(
            activity.supportFragmentManager,
            ShowFirstLetterDialogFragment.DIALOG_FIRST_LETTER
        )
    }

    private fun openShowWordDialog(
        activity: FragmentActivity,
        word: String
    ) {
        val dialog = ShowWordDialogFragment.newInstance(word)
        dialog.show(
            activity.supportFragmentManager,
            ShowWordDialogFragment.DIALOG_SHOW_WORD
        )
    }

    private fun deleteLastLetter(editText: EditText) {
        var str: String = editText.text.toString()
        if (editText.text.trim().isNotEmpty()) {
            str = str.substring(0, str.length - 1)
            editText.setText(str)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpSnackbar()
    }

    private fun openOneMoreTryDialog() {
        val dialog =
            OneTryDialogFragment()
        dialog.show(requireActivity().supportFragmentManager, "DIALOG")
    }

    private fun setUpSnackbar() {
        view?.setupSnackbarWithCustomColor(
            this,
            answerViewModel.snackbarText,
            Snackbar.LENGTH_SHORT,
            context!!.getColor(R.color.colorAccent)
        )
    }

    private fun displayAds(root: View) {
        loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }

    @SuppressLint("SetTextI18n")
    override fun onLetterItemClick(
        view: View,
        letter: String,
        lettersList: ArrayList<String>,
        position: Int
    ) {
        editText.setText(editText.text.toString() + letter)

        val adapter =
            GridViewLetterAdapter(
                context!!,
                lettersList,
                this,
                answerViewModel
            )
        gridView.adapter = adapter
        adapter.notifyDataSetChanged()
    }


    override fun onStart() {
        super.onStart()
        answerViewModel.isAnimation.set(true)
    }
}
