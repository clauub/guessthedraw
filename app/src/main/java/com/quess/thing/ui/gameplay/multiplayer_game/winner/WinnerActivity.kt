package com.quess.thing.ui.gameplay.multiplayer_game.winner

import android.os.Bundle
import androidx.navigation.findNavController
import com.quess.thing.R
import dagger.android.support.DaggerAppCompatActivity

class WinnerActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_winner)
    }

    // back button support
    override fun onSupportNavigateUp() = this.findNavController(R.id.nav_host_main_fragment).navigateUp()

    //    Disable BackButton
    override fun onBackPressed() {

    }
}
