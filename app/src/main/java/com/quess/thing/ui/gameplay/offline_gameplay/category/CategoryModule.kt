package com.quess.thing.ui.gameplay.offline_gameplay.category

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class CategoryModule {

    /**
     * Generates an [AndroidInjector] for the [CategoryFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeCategoryFragment(): CategoryFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [CategoryViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel::class)
    abstract fun bindCategoryViewModel(viewModel: CategoryViewModel): ViewModel
}