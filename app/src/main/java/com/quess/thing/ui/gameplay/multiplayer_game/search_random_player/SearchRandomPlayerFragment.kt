package com.quess.thing.ui.gameplay.multiplayer_game.search_random_player


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar

import com.quess.thing.databinding.FragmentSearchRandomPlayerBinding
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.MultiplayerAnswerActivity
import com.quess.thing.util.MainNavigationFragment
import com.quess.thing.util.setupToast
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class SearchRandomPlayerFragment : DaggerFragment(), MainNavigationFragment {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var vm: SearchRandomPlayerViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)
        val dataBinding =
            FragmentSearchRandomPlayerBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@SearchRandomPlayerFragment
                vm = vm
            }

        vm.getOnlineUser()

        vm.openGameActivity.observe(viewLifecycleOwner, Observer {
            startActivity(Intent(context, MultiplayerAnswerActivity::class.java))
        })

        vm.closeFragment.observe(viewLifecycleOwner, Observer {
            fragmentManager?.popBackStack()
        })

        vm.checkIfUserFound()

        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpToast()
    }

    override fun onStop() {
        super.onStop()
        vm.setUserIsNotSearching()

        vm.deleteChallenge()
    }

    override fun onStart() {
        super.onStart()
        vm.setUserIsSearching()
    }

    private fun setUpToast() {
        view?.setupToast(
            this,
            vm.toastText,
            Snackbar.LENGTH_SHORT
        )
    }

}
