package com.quess.thing.ui.gameplay.multiplayer_game.search_random_player

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.quess.thing.R
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.util.Event
import com.quess.thing.util.FirebaseSuccessListener
import kotlinx.coroutines.*
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

class SearchRandomPlayerViewModel @Inject constructor(
    private val userRepository: DatabaseUserRepository,
    private val gameRepository: DatabaseGameRepository
) :
    ViewModel() {

    private val _openGameActivity = MutableLiveData<Event<Unit>>()
    val openGameActivity: LiveData<Event<Unit>>
        get() = _openGameActivity

    private val _closeFragment = MutableLiveData<Event<Unit>>()
    val closeFragment: LiveData<Event<Unit>>
        get() = _closeFragment

    private val _toastText = MutableLiveData<Event<Int>>()
    val toastText: LiveData<Event<Int>> = _toastText

    private val coroutineContext = viewModelScope.coroutineContext + Dispatchers.IO

    fun getOnlineUser() {
        CoroutineScope(coroutineContext).launch {
            try {

                userRepository.getOnlineUser()

            } catch (e: Exception) {
                Timber.d(e)
            }
        }
    }

    fun deleteChallenge() {
        gameRepository.deleteChallengeAndGameRequest()
    }

    fun checkIfUserFound() {
        userRepository.checkIfUserFound(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                if (isDataFetched) {
                    Timber.d("Here open game activity")
                    _openGameActivity.value = Event(Unit)
                }
            }

        })
    }

    fun setUserIsSearching() {
        userRepository.setUserIsSearching()
    }

    fun setUserIsNotSearching() {
        userRepository.setUsersIsNotSearching()
    }

    init {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                delay(25000) // 25 seconds
                _closeFragment.value = Event(Unit)
                _toastText.value = Event(R.string.no_users_found)
                deleteChallenge()
            }
        }

    }
}