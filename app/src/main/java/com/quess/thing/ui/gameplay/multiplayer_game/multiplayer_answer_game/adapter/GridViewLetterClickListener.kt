package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter

import android.view.View

interface GridViewLetterClickListener {
    fun onLetterItemClick(view: View, letter: String, lettersList: ArrayList<String>, position: Int)
}