package com.quess.thing.ui.gameplay.offline_gameplay.word


import android.annotation.SuppressLint
import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider

import com.quess.thing.R
import com.quess.thing.databinding.FragmentWordBinding
import com.quess.thing.util.EventObserver
import com.quess.thing.util.admob.AdUtil
import com.quess.thing.util.admob.AdUtil.loadAd
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.objects.Navigation.goToGameFragment
import com.quess.thing.util.objects.Navigation.goToMainFragment
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Navigation.goToCategoryFragment
import com.quess.thing.util.snackBarLong
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_word.*
import javax.inject.Inject

class WordFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var wordViewModel: WordViewModel

    private var visible: Boolean = false
    private var btnPressed: Boolean = false

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        context ?: return null
        wordViewModel = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentWordBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@WordFragment
            vm = this@WordFragment.wordViewModel
        }

        displayAds(dataBinding.root)
        removeAds(dataBinding.root)

        analyticsHelper.sendScreenView("Word", requireActivity())

        val word = arguments?.getString("word")

        dataBinding.wordView.text = "$word"

        dataBinding.seeWordBtn.setOnClickListener {
            TransitionManager.beginDelayedTransition(transitionsContainer)
            btnPressed = true

            visible = !visible
            wordView.visibility = if (visible) View.VISIBLE else View.GONE

        }

        wordViewModel.backHome.observe(viewLifecycleOwner, EventObserver {
            goToMainFragment(view!!)
        })

        wordViewModel.backToCategory.observe(viewLifecycleOwner, EventObserver {
            goToCategoryFragment(view!!)
        })

        wordViewModel.startGame.observe(viewLifecycleOwner, EventObserver {
            if (btnPressed) {
                val bundle = bundleOf("word" to word)
                goToGameFragment(view!!, bundle)
            } else {
                snackBarLong(view!!, R.string.take_a_look_to_word)
            }
        })

        return dataBinding.root
    }

    private fun displayAds(root: View) {
        loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }

    override fun onStart() {
        super.onStart()
        wordViewModel.isAnimation.set(true)
    }
}
