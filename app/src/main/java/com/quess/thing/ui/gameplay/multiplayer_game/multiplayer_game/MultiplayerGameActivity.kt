package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game

import android.os.Bundle
import com.quess.thing.R
import com.quess.thing.util.IOnBackPressed
import com.quess.thing.util.inTransaction
import dagger.android.support.DaggerAppCompatActivity

class MultiplayerGameActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiplayer_game)

        if (savedInstanceState == null) {
            supportFragmentManager.inTransaction {
                add(R.id.container,
                    SelectWordToDrawDialog()
                )
            }
        }
    }

    override fun onBackPressed() {
        val fragment =
            this.supportFragmentManager.findFragmentById(R.id.container)
        (fragment as? IOnBackPressed)?.onBackPressed()?.let {

        }
    }


}
