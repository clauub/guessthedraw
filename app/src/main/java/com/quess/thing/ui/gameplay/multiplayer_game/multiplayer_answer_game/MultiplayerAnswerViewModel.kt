package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game

import android.content.Context
import android.widget.GridView
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.R
import com.quess.thing.databinding.FragmentMultiplayerAnswerBinding
import com.quess.thing.databinding.FragmentMultiplayerBinding
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter.GridViewLetterClickListener
import com.quess.thing.util.Event
import com.quess.thing.util.FirebaseSuccessListener
import com.quess.thing.util.SharedPref
import com.quess.thing.util.objects.Constants
import kotlinx.coroutines.*
import javax.inject.Inject


class MultiplayerAnswerViewModel @Inject constructor(
    private val userRepository: DatabaseUserRepository,
    private val gameRepository: DatabaseGameRepository,
    private val sharedPref: SharedPref
) :
    ViewModel() {

    private val _goToWinnerActivity = MutableLiveData<Event<Unit>>()
    val goToWinnerActivity: LiveData<Event<Unit>>
        get() = _goToWinnerActivity

    private val _goToLostActivity = MutableLiveData<Event<Unit>>()
    val goToLostActivity: LiveData<Event<Unit>>
        get() = _goToLostActivity

    private val _goToAnswerActivity = MutableLiveData<Event<Unit>>()
    val goToAnswerActivity: LiveData<Event<Unit>>
        get() = _goToAnswerActivity

    private val _deleteEditTextWord = MutableLiveData<Event<Unit>>()
    val deleteEditTextWord: LiveData<Event<Unit>>
        get() = _deleteEditTextWord

    private val _snackbarText = MutableLiveData<Event<Int>>()
    val snackbarText: LiveData<Event<Int>> = _snackbarText

    private val _checkWord = MutableLiveData<Event<Unit>>()
    val checkWord: LiveData<Event<Unit>>
        get() = _checkWord

    private val _openOneMoreTryDialog = MutableLiveData<Event<Unit>>()
    val openOneMoreTryDialog: LiveData<Event<Unit>>
        get() = _openOneMoreTryDialog

    private val _openShowWordDialog = MutableLiveData<Event<Unit>>()
    val openShowWordDialog: LiveData<Event<Unit>>
        get() = _openShowWordDialog

    private val _openFirstLetterDialog = MutableLiveData<Event<Unit>>()
    val openFirstLetterDialog: LiveData<Event<Unit>>
        get() = _openFirstLetterDialog

    private val _openNoCoinsDialog = MutableLiveData<Event<Unit>>()
    val openNoCoinsDialog: LiveData<Event<Unit>>
        get() = _openNoCoinsDialog

    private val _toastText = MutableLiveData<Event<Int>>()
    val toastText: LiveData<Event<Int>> = _toastText
    var isAnimation = ObservableBoolean(false)
    val coins = sharedPref.getCoins(Constants.COINS_KEY)

    fun setCurrentUserIDWinner() {
        gameRepository.setCurrentUserIDWinner()
    }

    fun setOpponentIDWinner() {
        gameRepository.setOpponentUserIDWinner()
    }

    fun goToLostActivity() {
        _goToLostActivity.value = Event(Unit)
    }

    fun onFirstLetterIconClicked() {
        if (coins >= 5) {
            _openFirstLetterDialog.value = Event(Unit)
        } else {
            _openNoCoinsDialog.value = Event(Unit)
        }
    }

    fun onShowWordIconClicked() {
        if (coins >= 15) {
            _openShowWordDialog.value = Event(Unit)
        } else {
            _openNoCoinsDialog.value = Event(Unit)
        }
    }

    fun setWordFromDatabase(
        gridView: GridView,
        context: Context,
        binding: FragmentMultiplayerAnswerBinding,
        listener: GridViewLetterClickListener
    ) {
        gameRepository.getDataForMultiplayerAnswer(
            gridView,
            listener,
            context,
            binding,
            this
        )
    }

    fun deleteWordTextFromEditText() {
        _deleteEditTextWord.value = Event(Unit)
    }

    fun checkWord() {
        _checkWord.value = Event(Unit)
    }

    fun goToWinnerActivity() {
        _goToWinnerActivity.value = Event(Unit)
    }

    fun openOneMoreTryDialog() {
        _openOneMoreTryDialog.value = Event(Unit)
    }

    fun currentUserWon() {
        gameRepository.ifCurrentUserWon(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _goToWinnerActivity.value = Event(Unit)
            }

        })
    }

    fun currentUserLost() {
        gameRepository.ifCurrentUserLost(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _goToLostActivity.value = Event(Unit)
            }
        })
    }
}