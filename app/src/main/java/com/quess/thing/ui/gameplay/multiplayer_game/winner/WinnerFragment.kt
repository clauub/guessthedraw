package com.quess.thing.ui.gameplay.multiplayer_game.winner


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R

import com.quess.thing.databinding.FragmentWinnerBinding
import com.quess.thing.util.EventObserver
import com.quess.thing.util.admob.AdUtil.loadAd
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.admob.RemoveInterstitialAdsHelper
import com.quess.thing.util.objects.Navigation.goToMainActivity
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class WinnerFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var vm: WinnerViewModel

    private val coinsNumberWin: Int = 4

    @SuppressLint("BinaryOperationInTimber", "SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentWinnerBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@WinnerFragment
            vm = this@WinnerFragment.vm
        }

        displayAds(dataBinding.root)

        removeAds(dataBinding.root)

        analyticsHelper.sendScreenView("Winner", requireActivity())

        vm.backHome.observe(viewLifecycleOwner, Observer {
            goToMainActivity(view!!)
        })

        vm.showInterstitial.observe(viewLifecycleOwner, EventObserver {
            removeInterstitialAds(dataBinding.root)
        })

        vm.saveCoins(coinsNumberWin)

        return dataBinding.root
    }

    private fun displayAds(root: View) {
        loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }

    private fun removeInterstitialAds(root: View) {
        RemoveInterstitialAdsHelper(root, this).removeInterstitialAds(
            viewLifecycleOwner,
            vm.loadInterstitialAd()
        )
    }

    override fun onStop() {
        super.onStop()
        vm.deleteGameFromFirebase()
        vm.setUserIsNotPlaying()
    }

    override fun onStart() {
        super.onStart()
        vm.isAnimation.set(true)
    }
}
