package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import cn.iwgang.countdownview.CountdownView
import com.quess.thing.R
import com.quess.thing.databinding.FragmentMultiplayerAnswerWaitingBinding
import com.quess.thing.ui.gameplay.multiplayer_game.lost.LostActivity
import com.quess.thing.ui.gameplay.multiplayer_game.winner.WinnerActivity
import com.quess.thing.util.*
import com.quess.thing.util.objects.Utils.isNetworkAvailable
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class MultiplayerAnswerWaitingFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var vm: MultiplayerAnswerWaitingViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)
        val dataBinding =
            FragmentMultiplayerAnswerWaitingBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@MultiplayerAnswerWaitingFragment
                vm = vm
            }

        vm.goToAnswerFragment.observe(viewLifecycleOwner, EventObserver {
            replaceFragment(context!!, R.id.container,
                MultiplayerAnswerFragment()
            )
        })

        vm.goToWinnerActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, WinnerActivity::class.java))
        })

        vm.goToLostActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, LostActivity::class.java))
        })

        vm.currentUserWon.observe(viewLifecycleOwner, EventObserver {
            ifCurrentUserWonGame()
        })

        vm.currentUserWon()

        vm.currentUserLost()

        dataBinding.countDownView!!.start(61000)

        onTickListenerDone(dataBinding.countDownView)

        vm.ifWordImageExistsGoToAnswer()

        return dataBinding.root
    }

    private fun onTickListenerDone(countDownTime: CountdownView?) {
        if (isNetworkAvailable(context!!)) {
            countDownTime!!.setOnCountdownEndListener {
                ifCurrentUserWonGame()
            }
        } else {
            alertDialogNoInternetConnection(context!!)
        }
    }

    private fun ifCurrentUserWonGame() {
        vm.setCurrentUserIDWinner()
        vm.goToWinnerActivity()
    }
}