package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cn.iwgang.countdownview.CountdownView
import com.quess.thing.databinding.FragmentMultiplayerGameWaitingBinding.inflate
import com.quess.thing.ui.gameplay.multiplayer_game.lost.LostActivity
import com.quess.thing.ui.gameplay.multiplayer_game.winner.WinnerActivity
import com.quess.thing.util.EventObserver
import com.quess.thing.util.MainNavigationFragment
import com.quess.thing.util.alertDialogNoInternetConnection
import com.quess.thing.util.objects.Utils.isNetworkAvailable
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class MultiplayerGameWaitingFragment : DaggerFragment(), MainNavigationFragment {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var vm: MultiplayerGameWaitingViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)
        val dataBinding =
            inflate(inflater, container, false).apply {
                lifecycleOwner = this@MultiplayerGameWaitingFragment
                vm = this@MultiplayerGameWaitingFragment.vm
            }

        dataBinding.countDownView.start(60000)

        onTickListenerDone(dataBinding.countDownView)


        vm.goToLostActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, LostActivity::class.java))
        })

        vm.goToWinnerActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, WinnerActivity::class.java))
        })

        vm.currentUserLost()

        vm.currentUserWon()

        return dataBinding.root
    }

    private fun onTickListenerDone(countDownTime: CountdownView) {
        if (isNetworkAvailable(context!!)) {
            countDownTime.setOnCountdownEndListener {
                currentUserWonGame()
            }
        } else {
            alertDialogNoInternetConnection(context!!)
        }
    }

    private fun currentUserWonGame() {
        vm.setCurrentUserIDWinner()
        vm.goToWinnerActivity()
    }
}
