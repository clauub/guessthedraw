package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.ChildFragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class SelectWordToDrawModule {

    /**
     * Generates an [AndroidInjector] for the [SelectWordToDrawDialog].
     */
    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeSelectWordToDrawDialog(): SelectWordToDrawDialog

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [SelectWordToDrawViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(SelectWordToDrawViewModel::class)
    abstract fun bindSelectWordToDrawViewModel(viewModel: SelectWordToDrawViewModel): ViewModel
}