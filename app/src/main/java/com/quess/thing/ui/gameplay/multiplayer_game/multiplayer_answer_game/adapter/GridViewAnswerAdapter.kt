package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button

class GridViewAnswerAdapter(
    private val answerCharacter: CharArray,
    private val context: Context
) : BaseAdapter() {


    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val button: Button

        if (p1 == null) {
            //Create new button
            button = Button(context)
            button.layoutParams = ViewGroup.LayoutParams(85, 85)
            button.setPadding(8, 8, 8, 8)
            button.setBackgroundColor(Color.DKGRAY)
            button.setTextColor(Color.RED)
            button.text = answerCharacter[p0].toString()
        } else
            button = p1 as Button
        return button
    }

    override fun getItem(p0: Int): Any? {
        return answerCharacter[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }


    override fun getCount(): Int {
        return answerCharacter.size
    }

}
