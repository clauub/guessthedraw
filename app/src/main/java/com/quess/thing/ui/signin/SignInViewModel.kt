package com.quess.thing.ui.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.util.Event
import com.quess.thing.util.ResultListener
import timber.log.Timber
import javax.inject.Inject


class SignInViewModel @Inject constructor(
    signInViewModelDelegate: SignInViewModelDelegate,
    private val repository: DatabaseUserRepository
) : ViewModel(), SignInViewModelDelegate by signInViewModelDelegate {

    private val _dismissDialogAction = MutableLiveData<Event<Unit>>()
    val dismissDialogAction: LiveData<Event<Unit>>
        get() = _dismissDialogAction

    fun onSignIn() {
        Timber.d("Sign in requested")
        emitSignInRequest()
        _dismissDialogAction.value = Event(Unit)
    }

    fun onCancel() {
        _dismissDialogAction.value = Event(Unit)
    }
}