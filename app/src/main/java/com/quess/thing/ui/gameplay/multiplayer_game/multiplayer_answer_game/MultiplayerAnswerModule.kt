package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class MultiplayerAnswerModule {


    /**
     * Generates an [AndroidInjector] for the [MultiplayerAnswerFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeMultiplayerAnswerFragment(): MultiplayerAnswerFragment


    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [MultiplayerAnswerViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(MultiplayerAnswerViewModel::class)
    abstract fun bindGameViewModel(viewModel: MultiplayerAnswerViewModel): ViewModel
}