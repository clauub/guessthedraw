package com.quess.thing.ui.main.shop

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import com.quess.thing.ui.main.MainFragment
import com.quess.thing.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class ShopModule {

    /**
     * Generates an [AndroidInjector] for the [ShopFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeShopFragment(): ShopFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [ShopViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(ShopViewModel::class)
    abstract fun bindShopViewModel(viewModel: ShopViewModel): ViewModel


}