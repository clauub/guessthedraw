package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R
import com.quess.thing.databinding.FragmentMultiplayerBinding
import com.quess.thing.databinding.RequestGameDialogBinding
import com.quess.thing.ui.gameplay.game_dialogs.check_invitation_number.CheckInvitationNumberFragmentDialog
import com.quess.thing.ui.gameplay.multiplayer_game.create_invitation.CreateInvitationFragment
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game.MultiplayerGameActivity
import com.quess.thing.ui.gameplay.multiplayer_game.search_random_player.SearchRandomPlayerFragment
import com.quess.thing.ui.main.MainFragment
import com.quess.thing.ui.signin.SignInDialogFragment
import com.quess.thing.util.*
import com.quess.thing.util.analytics.AnalyticsHelper
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.random.Random


/**
 * A simple [Fragment] subclass.
 */
@Suppress(
    "NAME_SHADOWING", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class MultiplayerFragment : DaggerFragment() {

    companion object {
        const val FRAGMENT_ID = R.id.container
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var multiplayerViewModel: MultiplayerViewModel

    private lateinit var currentFragment: MainNavigationFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        multiplayerViewModel = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentMultiplayerBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@MultiplayerFragment
            vm = this@MultiplayerFragment.multiplayerViewModel
        }

        analyticsHelper.sendScreenView("MultiplayerFragment", requireActivity())

        multiplayerViewModel.setUserIsNotPlaying()

        multiplayerViewModel.deleteGameFromFirebase()

        multiplayerViewModel.openMultiPlayer.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, MultiplayerActivity::class.java))
        })

        multiplayerViewModel.onQuickMatchClicked.observe(viewLifecycleOwner, EventObserver {
            consume {
                replaceFragment(SearchRandomPlayerFragment())
            }
        })

        multiplayerViewModel.openSignInDialog.observe(viewLifecycleOwner, EventObserver {
            openSignInDialog()
        })

        multiplayerViewModel.openRequestGameDialog.observe(viewLifecycleOwner, EventObserver {
            showRequestDialog()
        })

        multiplayerViewModel.openGameActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, MultiplayerGameActivity::class.java))
        })

        multiplayerViewModel.navigateToCheckInvitationNumber.observe(
            viewLifecycleOwner,
            EventObserver {
                openCheckInvitationNumberDialog()
            })

        multiplayerViewModel.navigateToCreateInvitation.observe(viewLifecycleOwner, EventObserver {
            replaceFragment(context!!, R.id.container, CreateInvitationFragment())
        })

        multiplayerViewModel.onGetGameRequest()

        multiplayerViewModel.checkIfUserFound()

        multiplayerViewModel.setUserDetails(dataBinding)

        return dataBinding.root
    }

    private fun openCheckInvitationNumberDialog() {
        val dialog = CheckInvitationNumberFragmentDialog()
        dialog.show(requireActivity().supportFragmentManager, "CHECK_INVITATION_DIALOG")
    }

    private fun openSignInDialog() {
        val dialog = SignInDialogFragment()
        dialog.show(requireActivity().supportFragmentManager, MainFragment.DIALOG_NEED_TO_SIGN_IN)
    }

    private fun <F> replaceFragment(fragment: F) where F : Fragment, F : MainNavigationFragment {
        fragmentManager!!.inTransaction {
            currentFragment = fragment
            replace(FRAGMENT_ID, fragment).addToBackStack(null)
        }
    }

    @SuppressLint("InflateParams")
    private fun showRequestDialog() {

        val binding: RequestGameDialogBinding = DataBindingUtil
            .inflate(LayoutInflater.from(context), R.layout.request_game_dialog, null, false)

        val dialog = Dialog(context)
        dialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)

        binding.declineBtn.setOnClickListener {
            dialog.dismiss()
            multiplayerViewModel.onDeclineButtonPressed()
        }

        binding.acceptBtn.setOnClickListener {
            dialog.dismiss()
            multiplayerViewModel.onAcceptButtonPressed()
        }

        dialog.show()

        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                delay(25000) // 25 second
                multiplayerViewModel.onDeclineButtonPressed()
                dialog.dismiss()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        multiplayerViewModel.setUserIsNotSearching()
    }

    override fun onStart() {
        super.onStart()
        multiplayerViewModel.setUserIsSearching()
        multiplayerViewModel.isAnimation.set(true)
    }
}
