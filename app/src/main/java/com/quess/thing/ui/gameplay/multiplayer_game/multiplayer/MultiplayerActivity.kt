package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer

import android.os.Bundle
import com.quess.thing.R
import com.quess.thing.util.inTransaction
import dagger.android.support.DaggerAppCompatActivity

class MultiplayerActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiplayer)

        if (savedInstanceState == null) {
            supportFragmentManager.inTransaction {
                add(R.id.container,
                    MultiplayerFragment()
                )
            }
        }
    }
}
