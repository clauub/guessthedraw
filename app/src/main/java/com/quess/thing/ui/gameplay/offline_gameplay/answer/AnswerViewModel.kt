package com.quess.thing.ui.gameplay.offline_gameplay.answer

import android.content.Context
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.R
import com.quess.thing.util.*
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.objects.Constants.COINS_KEY
import com.quess.thing.util.objects.Navigation.goToLostFragment
import com.quess.thing.util.objects.Navigation.goToWinnerFragment2
import javax.inject.Inject


class AnswerViewModel @Inject constructor(
    private val context: Context,
    private val sharedPref: SharedPref
) : ViewModel() {

    private val _openOneMoreTryDialog = MutableLiveData<Event<Unit>>()
    val openOneMoreTryDialog: LiveData<Event<Unit>>
        get() = _openOneMoreTryDialog

    private val _openFirstLetterDialog = MutableLiveData<Event<Unit>>()
    val openFirstLetterDialog: LiveData<Event<Unit>>
        get() = _openFirstLetterDialog

    private val _openShowWordDialog = MutableLiveData<Event<Unit>>()
    val openShowWordDialog: LiveData<Event<Unit>>
        get() = _openShowWordDialog

    private val _deleteLetter = MutableLiveData<Event<Unit>>()
    val deleteLetter: LiveData<Event<Unit>>
        get() = _deleteLetter

    private val _checkWord = MutableLiveData<Event<Unit>>()
    val checkWord: LiveData<Event<Unit>>
        get() = _checkWord

    private val _lastCheckWord = MutableLiveData<Event<Unit>>()
    val lastCheckWord: LiveData<Event<Unit>>
        get() = _lastCheckWord

    private val _openNoCoinsDialog = MutableLiveData<Event<Unit>>()
    val openNoCoinsDialog: LiveData<Event<Unit>>
        get() = _openNoCoinsDialog

    private val _snackbarText = MutableLiveData<Event<Int>>()
    val snackbarText: LiveData<Event<Int>> = _snackbarText

    private var attempts: Int = 0
    private var attemptsRemained: Int = 4

    var isAnimation = ObservableBoolean(false)
    val bundle = bundleOf()
    val coins = sharedPref.getCoins(COINS_KEY)

    fun checkWordButtonPressed() {
        _checkWord.value = Event(Unit)

        checkAttempts()

        checkIfAttemptsDone()
    }

    fun onFirstLetterIconClicked() {
        if (coins >= 5) {
            _openFirstLetterDialog.value = Event(Unit)
        } else {
            _openNoCoinsDialog.value = Event(Unit)
        }
    }

    fun onShowWordIconClicked() {
        if (coins >= 15) {
            _openShowWordDialog.value = Event(Unit)
        } else {
            _openNoCoinsDialog.value = Event(Unit)
        }
    }

    fun onDeleteLetterIconClicked() {
        _deleteLetter.value = Event(Unit)
    }

    private fun checkAttempts() {
        if (++attempts == 4) {
            _openOneMoreTryDialog.value = Event(Unit)
        }
    }

    private fun checkIfAttemptsDone() {
        if (attempts == 5) {
            _lastCheckWord.value = Event(Unit)
        }
    }

    fun checkWord(
        inputString: String,
        wordString: String,
        editText: EditText,
        view: View
    ) {
        if (wordString == inputString) {
            goToWinnerFragment2(view)
        } else {
            attemptsRemained--
            if (attemptsRemained > 0) {
                _snackbarText.value = Event(R.string.you_remained_attempts)
            }
            editText.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake))
        }
    }

    fun lastCheckWord(inputString: String, wordString: String, view: View) {
        if (wordString == inputString) {
            goToWinnerFragment2(view)
        } else {
            goToLostFragment(view)
        }
    }
}