package com.quess.thing.ui.signin

import android.net.Uri

interface UserInfo {
    val uid: String

    val providerId: String

    val displayName: String?

    val photoUrl: Uri?

    val email: String?

    val phoneNumber: String?

    val isEmailVerified: Boolean
}
