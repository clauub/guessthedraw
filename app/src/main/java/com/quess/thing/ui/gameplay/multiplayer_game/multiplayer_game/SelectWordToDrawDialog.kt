package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R
import com.quess.thing.databinding.FragmentSelectWordToDrawDialogBinding
import com.quess.thing.ui.gameplay.multiplayer_game.lost.LostActivity
import com.quess.thing.ui.gameplay.multiplayer_game.winner.WinnerActivity
import com.quess.thing.ui.widget.CustomDimDialogFragment
import com.quess.thing.util.EventObserver
import com.quess.thing.util.alertDialogNoInternetConnection
import com.quess.thing.util.viewModelProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class SelectWordToDrawDialog : CustomDimDialogFragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    private lateinit var vm: SelectWordToDrawViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)

        val databinding =
            FragmentSelectWordToDrawDialogBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@SelectWordToDrawDialog
                vm = this@SelectWordToDrawDialog.vm
            }


        val easyWord = vm.randomEasyWordList()
        val mediumWord = vm.randomMediumWordList()
        val hardWord = vm.randomHardWordList()
        databinding.easyWord.text = easyWord
        databinding.mediumWord.text = mediumWord
        databinding.hardWord.text = hardWord

        vm.easyWord.observe(viewLifecycleOwner, EventObserver {
            vm.saveWordToDatabase(easyWord)
        })
        vm.mediumWord.observe(viewLifecycleOwner, EventObserver {
            vm.saveWordToDatabase(mediumWord)
        })
        vm.hardWord.observe(viewLifecycleOwner, EventObserver {
            vm.saveWordToDatabase(hardWord)
        })

        vm.openNoConnectionDialog.observe(viewLifecycleOwner, EventObserver {
            alertDialogNoInternetConnection(context!!)
        })

        vm.openMultiplayerGameFragment.observe(viewLifecycleOwner, EventObserver {
            changeFragmentFromFragment(MultiplayerGameFragment())
        })

        vm.goToLostActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, LostActivity::class.java))
        })

        vm.goToWinnerActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, WinnerActivity::class.java))
        })

        vm.currentUserWon.observe(viewLifecycleOwner, EventObserver {
            ifCurrentUserWonGame()
        })

        vm.currentUserWon()

        vm.currentUserLost()

        return databinding.root
    }

    private fun changeFragmentFromFragment(fragment: Fragment) {
        // Create new fragment and transaction
        // Create new fragment and transaction
        val newFragment: Fragment = fragment
        val transaction: FragmentTransaction = fragmentManager!!.beginTransaction()

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
        // Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
        transaction.replace(R.id.container, newFragment)
        transaction.addToBackStack(null)

// Commit the transaction
        // Commit the transaction
        transaction.commit()
    }

    private fun ifCurrentUserWonGame() {
        vm.setCurrentUserIDWinner()
        vm.goToWinnerActivity()
    }
}
