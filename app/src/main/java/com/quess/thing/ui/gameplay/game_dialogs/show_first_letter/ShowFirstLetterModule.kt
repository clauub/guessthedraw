package com.quess.thing.ui.gameplay.game_dialogs.show_first_letter

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.ChildFragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import com.quess.thing.ui.gameplay.offline_gameplay.answer.OneMoreTryViewModel
import com.quess.thing.ui.gameplay.offline_gameplay.answer.OneTryDialogFragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class ShowFirstLetterModule {

    /**
     * Generates an [AndroidInjector] for the [ShowFirstLetterDialogFragment].
     */
    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeShowFirstLetterModule(): ShowFirstLetterDialogFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [ShowFirstLetterViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(ShowFirstLetterViewModel::class)
    abstract fun bindShowFirstLetterViewModel(viewModel: ShowFirstLetterViewModel): ViewModel
}