package com.quess.thing.ui.gameplay.game_dialogs.no_coins

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.quess.thing.R
import com.quess.thing.repository.billing.localdb.COINS_PURCHASE
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.objects.Constants.COINS_KEY
import kotlinx.coroutines.cancel
import timber.log.Timber
import javax.inject.Inject

class NoCoinsDialogViewModel @Inject constructor(
    private val sharedPref: SharedPref,
    private val analyticsHelper: AnalyticsHelper,
    private val context: Context
) : ViewModel(), RewardedVideoAdListener {

    val coins = sharedPref.getCoins(COINS_KEY)
    private var rewardedVideoAd: RewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context)
    private val reward5Coins = 5

    init {
        rewardedVideoAd.rewardedVideoAdListener = this
    }

    private val _dismissDialogAction = MutableLiveData<Event<Unit>>()
    val dismissDialogAction: LiveData<Event<Unit>>
        get() = _dismissDialogAction

    private val _openShop = MutableLiveData<Event<Unit>>()
    val openShop: LiveData<Event<Unit>>
        get() = _openShop

    fun onCloseIconClicked() {
        _dismissDialogAction.value = Event(Unit)
    }

    fun shopClicked() {
        _openShop.value = Event(Unit)
    }

    fun onWatchAdClicked() {
        analyticsHelper.logUiEvent("WatchADFromNoCoinsDialog", AnalyticsActions.CLICK)
        if (rewardedVideoAd.isLoaded) {
            rewardedVideoAd.show()
        }
    }

    fun loadAd() {
        rewardedVideoAd.loadAd(
            context.getString(R.string.ad_rewarded),
            AdRequest.Builder().build()
        )
    }

    fun onResumeVideoAd() {
        rewardedVideoAd.resume(context)
    }

    fun onPauseVideoAd() {
        rewardedVideoAd.pause(context)
    }

    fun onDestroyVideoAd() {
        rewardedVideoAd.destroy(context)
    }

    override fun onRewardedVideoAdClosed() {
        Timber.d("Video ad closed")
        loadAd()
    }

    override fun onRewardedVideoAdLeftApplication() {
        Timber.d("Video ad left application")
    }

    override fun onRewardedVideoAdLoaded() {
        Timber.d("Video ad loaded")
    }

    override fun onRewardedVideoAdOpened() {
        Timber.d("Video ad opened")
    }

    override fun onRewardedVideoCompleted() {
        Timber.d("Video complete")
    }

    override fun onRewarded(p0: RewardItem?) {
        Timber.d("You got 5 coins reward")
        get5RewardedCoins()
    }

    override fun onRewardedVideoStarted() {
        Timber.d("Video ad started")
    }

    override fun onRewardedVideoAdFailedToLoad(p0: Int) {
        Timber.d("Video ad failed to load")
    }

    override fun onCleared() {
        super.onCleared()
        Timber.d("onCleared")
        viewModelScope.coroutineContext.cancel()
    }

    private fun get5RewardedCoins() {
        sharedPref.saveCoins(COINS_KEY, coins + reward5Coins)
    }

}
