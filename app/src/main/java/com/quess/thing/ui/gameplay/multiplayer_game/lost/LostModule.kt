package com.quess.thing.ui.gameplay.multiplayer_game.lost

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class LostModule {

    /**
     * Generates an [AndroidInjector] for the [LostFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeLostFragment(): LostFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [LostViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(LostViewModel::class)
    abstract fun bindLostViewModel(viewModel: LostViewModel): ViewModel


}