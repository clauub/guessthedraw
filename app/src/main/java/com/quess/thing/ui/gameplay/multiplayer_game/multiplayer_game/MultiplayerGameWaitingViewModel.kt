package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.util.Event
import com.quess.thing.util.FirebaseSuccessListener
import kotlinx.coroutines.*
import javax.inject.Inject

class MultiplayerGameWaitingViewModel @Inject constructor(private val gameRepository: DatabaseGameRepository) :
    ViewModel() {

    private val _goToWinnerActivity = MutableLiveData<Event<Unit>>()
    val goToWinnerActivity: LiveData<Event<Unit>>
        get() = _goToWinnerActivity

    private val _goToLostActivity = MutableLiveData<Event<Unit>>()
    val goToLostActivity: LiveData<Event<Unit>>
        get() = _goToLostActivity

    fun goToWinnerActivity() {
        _goToWinnerActivity.value = Event(Unit)
    }

    fun goToLostActivity() {
        _goToLostActivity.value = Event(Unit)
    }

    fun setCurrentUserIDWinner() {
        gameRepository.setCurrentUserIDWinner()
    }

    fun currentUserWon() {
        gameRepository.ifCurrentUserWon(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _goToWinnerActivity.value = Event(Unit)
            }

        })
    }

    private fun setOpponentIDWinner() {
        gameRepository.setOpponentUserIDWinner()
    }

    fun currentUserLost() {
        gameRepository.ifCurrentUserLost(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _goToLostActivity.value = Event(Unit)
            }

        })
    }

    fun setWinnerAfter10SecondsInactivity() {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                delay(10000) // 10 seconds

                setOpponentIDWinner()

            }
        }
    }

}