package com.quess.thing.ui.main.shop

import android.app.Activity
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.quess.thing.repository.billing.BillingRepository
import com.quess.thing.repository.billing.localdb.AugmentedSkuDetails
import com.quess.thing.repository.billing.localdb.PremiumCar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import timber.log.Timber

class BillingViewModel(application: Application) : AndroidViewModel(application) {

    private val inappSkuDetailsListLiveData: LiveData<List<AugmentedSkuDetails>>
    val premiumCarLiveData: LiveData<PremiumCar>

    private val viewModelScope = CoroutineScope(Job() + Dispatchers.Main)
    private val repository: BillingRepository = BillingRepository.getInstance(application)

    init {
        repository.startDataSourceConnections()
        premiumCarLiveData = repository.premiumCarLiveData
        inappSkuDetailsListLiveData = repository.inappSkuDetailsListLiveData
    }

    override fun onCleared() {
        super.onCleared()
        Timber.d("onCleared")
        repository.endDataSourceConnections()
        viewModelScope.coroutineContext.cancel()
    }
}