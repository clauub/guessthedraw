package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game

import android.content.Context
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.quess.thing.R
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.util.Event
import com.quess.thing.util.FirebaseSuccessListener
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Utils
import kotlinx.coroutines.*
import javax.inject.Inject

class MultiplayerGameViewModel @Inject constructor(
    private val context: Context,
    private val userRepository: DatabaseUserRepository,
    private val gameRepository: DatabaseGameRepository,
    private val analyticsHelper: AnalyticsHelper
) :
    ViewModel() {

    private val _goToWinnerActivity = MutableLiveData<Event<Unit>>()
    val goToWinnerActivity: LiveData<Event<Unit>>
        get() = _goToWinnerActivity

    private val _goToLostActivity = MutableLiveData<Event<Unit>>()
    val goToLostActivity: LiveData<Event<Unit>>
        get() = _goToLostActivity

    private val _currentUserWon = MutableLiveData<Event<Unit>>()
    val currentUserWon: LiveData<Event<Unit>>
        get() = _currentUserWon

    private val _saveImageDrawing = MutableLiveData<Event<Unit>>()
    val saveImageDrawing: LiveData<Event<Unit>>
        get() = _saveImageDrawing

    private val _openAreYouDoneDialog = MutableLiveData<Event<Unit>>()
    val openAreYouDoneDialog: LiveData<Event<Unit>>
        get() = _openAreYouDoneDialog

    private val _closeDrawing = MutableLiveData<Event<Unit>>()
    val closeDrawing: LiveData<Event<Unit>>
        get() = _closeDrawing

    private val _deleteDrawing = MutableLiveData<Event<Unit>>()
    val deleteDrawing: LiveData<Event<Unit>>
        get() = _deleteDrawing

    private val _redoDrawing = MutableLiveData<Event<Unit>>()
    val redoDrawing: LiveData<Event<Unit>>
        get() = _redoDrawing

    private val _undoDrawing = MutableLiveData<Event<Unit>>()
    val undoDrawing: LiveData<Event<Unit>>
        get() = _undoDrawing

    private val _showInterstitial = MutableLiveData<Event<Unit>>()
    val showInterstitial: LiveData<Event<Unit>>
        get() = _showInterstitial

    var mInterstitialAd: InterstitialAd = InterstitialAd(context)

    init {
        mInterstitialAd.adUnitId = context.getString(R.string.ad_interstitial)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    fun loadInterstitialAd() {
        if (mInterstitialAd.isLoaded) {
            mInterstitialAd.show()
        } else if (!Utils.isNetworkAvailable(context)) {
            _closeDrawing.value = Event(Unit)
        }
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                _closeDrawing.value = Event(Unit)
                mInterstitialAd.loadAd(AdRequest.Builder().build())
            }
        }
    }

    fun multiplayerFabButtonPressed() {
        _openAreYouDoneDialog.value = Event(Unit)
        analyticsHelper.logUiEvent("FABDoneDrawingMultiplayer", AnalyticsActions.CLICK)
    }

    fun closeIcon() {
        _showInterstitial.value = Event(Unit)
        analyticsHelper.logUiEvent("CloseDrawingMultiplayer", AnalyticsActions.CLICK)
    }

    fun onDeletePressed() {
        _deleteDrawing.value = Event(Unit)
        analyticsHelper.logUiEvent("DeleteDrawingMultiplayer", AnalyticsActions.CLICK)
    }

    fun onRedoPressed() {
        _redoDrawing.value = Event(Unit)
        analyticsHelper.logUiEvent("RedoDrawingMultiplayer", AnalyticsActions.CLICK)
    }

    fun onUndoPressed() {
        _undoDrawing.value = Event(Unit)
        analyticsHelper.logUiEvent("UndoDrawingMultiplayer", AnalyticsActions.CLICK)
    }

    fun setCurrentUserIDWinner() {
        gameRepository.setCurrentUserIDWinner()
    }

    fun setOpponentIDWinner() {
        gameRepository.setOpponentUserIDWinner()
    }

    fun goToLostActivity() {
        _goToLostActivity.value = Event(Unit)
    }

    fun goToWinnerActivity() {
        _goToWinnerActivity.value = Event(Unit)
    }

    fun setUserIsNotPlaying() {
        userRepository.setUserIsNotPlaying()
    }

    fun setWordFromDatabase(wordTextView: TextView) {
        gameRepository.getWordFromDatabase(wordTextView)
    }

    fun currentUserWon() {
        gameRepository.ifCurrentUserWon(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _currentUserWon.value = Event(Unit)
            }
        })
    }

    fun currentUserLost() {
        gameRepository.ifCurrentUserLost(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _goToLostActivity.value = Event(Unit)
            }

        })
    }

    fun saveImage(image: ByteArray) {
        gameRepository.saveImageDrawingToStorage(image)
    }
}