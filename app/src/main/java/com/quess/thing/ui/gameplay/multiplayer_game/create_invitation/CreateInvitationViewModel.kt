package com.quess.thing.ui.gameplay.multiplayer_game.create_invitation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.util.Event
import com.quess.thing.util.FirebaseSuccessListener
import javax.inject.Inject

class CreateInvitationViewModel @Inject constructor(private val gameRepository: DatabaseGameRepository) :
    ViewModel() {

    var invitationID: String? = null

    private val _navigateToGame = MutableLiveData<Event<Unit>>()
    val navigateToGame: LiveData<Event<Unit>>
        get() = _navigateToGame

    fun createInvitationGame(invitationNumber: String) {
        gameRepository.createGameInvitationRoom(invitationNumber)
    }

    fun ifGameExistsGoToGame() {
        gameRepository.checkIfGameExist(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _navigateToGame.value = Event(Unit)
            }

        })
    }

    fun deleteInvitationsChild() {
        gameRepository.deleteInvitationChild()
    }


}