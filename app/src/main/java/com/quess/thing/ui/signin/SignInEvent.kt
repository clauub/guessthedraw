package com.quess.thing.ui.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.quess.thing.ui.signin.datasource.ObserveUserAuthStateUseCase
import com.quess.thing.ui.signin.datasource.Result
import com.quess.thing.util.Event
import com.quess.thing.util.map
import javax.inject.Inject


enum class SignInEvent {
    RequestSignIn, RequestSignOut
}

/**
 * Interface to implement sign-in functionality in a ViewModel.
 *
 * You can inject a implementation of this via Dagger2, then use the implementation as an interface
 * delegate to add sign in functionality without writing any code
 *
 * Example usage
 *
 * ```
 * class MyViewModel @Inject constructor(
 *     signInViewModelComponent: SignInViewModelDelegate
 * ) : ViewModel(), SignInViewModelDelegate by signInViewModelComponent {
 * ```
 */
interface SignInViewModelDelegate {

    /**
     * Emits Events when a sign-in event should be attempted
     */
    val performSignInEvent: MutableLiveData<Event<SignInEvent>>

    /**
     * Live updated value of the current firebase user
     */
    val currentFirebaseUser: LiveData<Result<AuthenticatedUserInfo>?>


    /**
     * Emit an Event on performSignInEvent to request sign-in
     */
    fun emitSignInRequest()

    /**
     * Emit an Event on performSignInEvent to request sign-out
     */
    fun emitSignOutRequest()

    fun observeSignedInUser(): LiveData<Boolean>

    fun isSignedIn(): Boolean

    fun displayName(): String

    /**
     * Implementation of SignInViewModelDelegate that uses Firebase's auth mechanisms.
     */
    class FirebaseSignInViewModelDelegate @Inject constructor(
        observeUserAuthStateUseCase: ObserveUserAuthStateUseCase,
        private val auth: FirebaseAuth
    ) :
        SignInViewModelDelegate {

        override val performSignInEvent = MutableLiveData<Event<SignInEvent>>()
        override val currentFirebaseUser: LiveData<Result<AuthenticatedUserInfo>?>

        private val _isSignedIn: LiveData<Boolean>
        private val _displayName: LiveData<String>

        init {

            currentFirebaseUser = observeUserAuthStateUseCase.observe()

            _isSignedIn = currentFirebaseUser.map { isSignedIn() }

            _displayName = currentFirebaseUser.map { displayName() }

            observeUserAuthStateUseCase.execute(Any())

        }

        override fun emitSignInRequest() {
            // Refresh the notificationsPrefIsShown because it's used to indicate if the
            // notifications preference dialog should be shown
            performSignInEvent.postValue(Event(SignInEvent.RequestSignIn))
        }

        override fun emitSignOutRequest() {
            performSignInEvent.postValue(Event(SignInEvent.RequestSignOut))
        }

        override fun isSignedIn(): Boolean {
            return auth.currentUser != null
        }

        override fun displayName(): String {
            return auth.currentUser?.displayName!!
        }

        override fun observeSignedInUser(): LiveData<Boolean> {
            return _isSignedIn
        }
    }


}


