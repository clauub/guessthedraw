package com.quess.thing.ui.gameplay.game_dialogs.show_first_letter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.R
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.objects.Constants
import javax.inject.Inject

class ShowFirstLetterViewModel @Inject constructor(private val sharedPref: SharedPref) :
    ViewModel() {

    val coins = sharedPref.getCoins(Constants.COINS_KEY)

    private val decreaseNumberCoins: Int = 5

    var firstLetter: String? = null

    private val _dismissDialogAction = MutableLiveData<Event<Unit>>()
    val dismissDialogAction: LiveData<Event<Unit>>
        get() = _dismissDialogAction

    private val _showFirstLetterIfUserHasCoins = MutableLiveData<Event<Unit>>()
    val showFirstLetterIfUserHasCoins: LiveData<Event<Unit>>
        get() = _showFirstLetterIfUserHasCoins


    private val _toastText = MutableLiveData<Event<Int>>()
    val toastText: LiveData<Event<Int>> = _toastText

    fun onCloseIconClicked() {
        _dismissDialogAction.value = Event(Unit)
    }

    fun onOkButtonPressed() {
        if (coins >= 5) {
            _showFirstLetterIfUserHasCoins.value = Event(Unit)
            updateCoinsToSharedPref()
        } else {
            _toastText.value = Event(R.string.you_dont_have_coins)
        }
        _dismissDialogAction.value = Event(Unit)

    }

    private fun updateCoinsToSharedPref() {
        sharedPref.saveCoins(Constants.COINS_KEY, coins - decreaseNumberCoins)
    }

}