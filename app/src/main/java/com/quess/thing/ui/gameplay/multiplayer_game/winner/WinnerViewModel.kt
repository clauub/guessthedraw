package com.quess.thing.ui.gameplay.multiplayer_game.winner

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.quess.thing.App
import com.quess.thing.R
import com.quess.thing.repository.billing.BillingRepository
import com.quess.thing.repository.billing.localdb.PremiumCar
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.util.objects.Constants.COINS_KEY
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Utils.isNetworkAvailable
import javax.inject.Inject

class WinnerViewModel @Inject constructor(
    private val context: App,
    private val sharedPref: SharedPref,
    private val analyticsHelper: AnalyticsHelper,
    private val gameRepository: DatabaseGameRepository,
    private val userRepository: DatabaseUserRepository
) : ViewModel() {

    private val premiumCarLiveData: LiveData<PremiumCar>
    private val repository: BillingRepository = BillingRepository.getInstance(context)

    val coins = sharedPref.getCoins(COINS_KEY)
    var mInterstitialAd: InterstitialAd = InterstitialAd(context)
    var isAnimation = ObservableBoolean(false)

    init {
        premiumCarLiveData = repository.premiumCarLiveData
        mInterstitialAd.adUnitId = context.getString(R.string.ad_interstitial)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    private val _backHome = MutableLiveData<Event<Unit>>()
    val backHome: LiveData<Event<Unit>>
        get() = _backHome

    private val _showInterstitial = MutableLiveData<Event<Unit>>()
    val showInterstitial: LiveData<Event<Unit>>
        get() = _showInterstitial

    fun saveCoins(coinsNumber: Int) {
        sharedPref.saveCoins(COINS_KEY, coins + coinsNumber)
    }

    fun deleteGameFromFirebase() {
        gameRepository.deleteGameChild()
    }

    fun backHomePressed() {
        _showInterstitial.value = Event(Unit)
        analyticsHelper.logUiEvent("BackHomeFromResult", AnalyticsActions.CLICK)
    }

    fun loadInterstitialAd() {
        if (mInterstitialAd.isLoaded) {
            mInterstitialAd.show()
        } else if (!isNetworkAvailable(context)) {
            _backHome.value = Event(Unit)
        }
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                _backHome.value = Event(Unit)
                mInterstitialAd.loadAd(AdRequest.Builder().build())
            }
        }
    }

    fun setUserIsNotPlaying() {
        userRepository.setUserIsNotPlaying()
    }
}