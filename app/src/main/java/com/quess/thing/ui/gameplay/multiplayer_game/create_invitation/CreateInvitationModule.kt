package com.quess.thing.ui.gameplay.multiplayer_game.create_invitation

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer.MultiplayerFragment
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer.MultiplayerViewModel
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class CreateInvitationModule {

    /**
     * Generates an [AndroidInjector] for the [CreateInvitationFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeCreateInvitationFragment(): CreateInvitationFragment


    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [CreateInvitationViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(CreateInvitationViewModel::class)
    abstract fun bindCreateInvitationViewModel(viewModel: CreateInvitationViewModel): ViewModel


}