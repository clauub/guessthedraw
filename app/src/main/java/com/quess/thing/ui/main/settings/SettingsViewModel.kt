package com.quess.thing.ui.main.settings

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.BuildConfig
import com.quess.thing.ui.signin.SignInViewModelDelegate
import com.quess.thing.util.Event
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import timber.log.Timber
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
    private val context: Context,
    private val analyticsHelper: AnalyticsHelper,
    private val signInViewModelDelegate: SignInViewModelDelegate
) : ViewModel(), SignInViewModelDelegate by signInViewModelDelegate {

    var versionCode = BuildConfig.VERSION_NAME

    private val _backHome = MutableLiveData<Event<Unit>>()
    val backHome: LiveData<Event<Unit>>
        get() = _backHome

    private val _shareApp = MutableLiveData<Event<Unit>>()
    val shareApp: LiveData<Event<Unit>>
        get() = _shareApp

    private val _sendEmail = MutableLiveData<Event<Unit>>()
    val sendEmail: LiveData<Event<Unit>>
        get() = _sendEmail

    private val _rateApp = MutableLiveData<Event<Unit>>()
    val rateApp: LiveData<Event<Unit>>
        get() = _rateApp

    fun onSignOut() {
        Timber.d("Sign out requested")
        emitSignOutRequest()
    }

    fun onIconBackPressed() {
        _backHome.value = Event(Unit)
    }

    fun sendEmail() {
        _sendEmail.value = Event(Unit)
        analyticsHelper.logUiEvent("ContactDeveloper", AnalyticsActions.CLICK)
    }

    fun rateApp() {
        _rateApp.value = Event(Unit)
        analyticsHelper.logUiEvent("RateApp", AnalyticsActions.CLICK)
    }

    fun shareApp() {
        _shareApp.value = Event(Unit)
        analyticsHelper.logUiEvent("ShareApp", AnalyticsActions.CLICK)
    }

}