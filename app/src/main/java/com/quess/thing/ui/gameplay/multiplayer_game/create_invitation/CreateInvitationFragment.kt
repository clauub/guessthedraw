package com.quess.thing.ui.gameplay.multiplayer_game.create_invitation

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R
import com.quess.thing.databinding.FragmentCreateInvitationBinding
import com.quess.thing.databinding.FragmentMultiplayerBinding
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer.MultiplayerViewModel
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game.MultiplayerGameActivity
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game.MultiplayerGameFragment
import com.quess.thing.util.EventObserver
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.replaceFragment
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import kotlin.random.Random

/**
 * A simple [Fragment] subclass.
 */
class CreateInvitationFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var vm: CreateInvitationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)

        val dataBinding =
            FragmentCreateInvitationBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@CreateInvitationFragment
                vm = this@CreateInvitationFragment.vm
            }

        val invitationNumber = generatedNumber()
        vm.createInvitationGame(invitationNumber)

        vm.invitationID = invitationNumber

        vm.navigateToGame.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, MultiplayerGameActivity::class.java))
        })

        vm.ifGameExistsGoToGame()

        return dataBinding.root
    }

    private fun generatedNumber(): String {
        val rnd = Random
        val n = 100000 + rnd.nextInt(900000)
        return n.toString()
    }

    override fun onStop() {
        super.onStop()

        vm.deleteInvitationsChild()
    }

}
