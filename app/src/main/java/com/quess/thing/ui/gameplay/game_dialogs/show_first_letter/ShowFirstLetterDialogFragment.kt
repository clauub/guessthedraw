package com.quess.thing.ui.gameplay.game_dialogs.show_first_letter


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar

import com.quess.thing.R
import com.quess.thing.databinding.FragmentMultiplayerBinding
import com.quess.thing.databinding.FragmentShowFirstLetterDialogBinding
import com.quess.thing.ui.gameplay.game_dialogs.no_coins.NoCoinsFragmentDialog
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer.MultiplayerViewModel
import com.quess.thing.ui.gameplay.offline_gameplay.answer.OneTryDialogFragment
import com.quess.thing.ui.widget.CustomDimDialogFragment
import com.quess.thing.util.EventObserver
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.setupToast
import com.quess.thing.util.toastLong
import com.quess.thing.util.viewModelProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_multiplayer_answer.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ShowFirstLetterDialogFragment : CustomDimDialogFragment(), HasSupportFragmentInjector {

    companion object {
        private const val FIRST_LETTER = "first_letter"
        const val DIALOG_FIRST_LETTER = "dialog_show_first_letter"
        fun newInstance(firstLetter: String): ShowFirstLetterDialogFragment {
            val bundle = Bundle().apply {
                putString(FIRST_LETTER, firstLetter)
            }
            return ShowFirstLetterDialogFragment().apply { arguments = bundle }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var vm: ShowFirstLetterViewModel

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)

        requireNotNull(arguments).run {
            vm.firstLetter = getString(FIRST_LETTER)
        }

        val dataBinding =
            FragmentShowFirstLetterDialogBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@ShowFirstLetterDialogFragment
                vm = this@ShowFirstLetterDialogFragment.vm
            }

        analyticsHelper.sendScreenView("ShowFirstLetterDialogFragment", requireActivity())

        vm.dismissDialogAction.observe(viewLifecycleOwner, EventObserver {
            dismiss()
        })

        vm.showFirstLetterIfUserHasCoins.observe(viewLifecycleOwner, EventObserver {
            toastLong(context!!, "First letter of the word is : ${vm.firstLetter}")
        })

        return dataBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpToast()
    }

    private fun setUpToast() {
        view?.setupToast(
            this,
            vm.toastText,
            Snackbar.LENGTH_SHORT
        )
    }


}
