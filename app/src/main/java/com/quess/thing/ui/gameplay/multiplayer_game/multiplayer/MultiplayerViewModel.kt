package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.ui.signin.SignInViewModelDelegate
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.objects.Constants
import javax.inject.Inject
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.ObservableBoolean
import com.quess.thing.databinding.FragmentMultiplayerBinding
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.util.FirebaseSuccessListener
import com.quess.thing.util.objects.Constants.LOSES
import com.quess.thing.util.objects.Constants.NAME
import com.quess.thing.util.objects.Constants.PHOTO_URL
import com.quess.thing.util.objects.Constants.WINS
import kotlinx.coroutines.*
import timber.log.Timber


class MultiplayerViewModel @Inject constructor(
    private val signInViewModelDelegate: SignInViewModelDelegate,
    private val sharedPref: SharedPref,
    private val userRepository: DatabaseUserRepository,
    private val gameRepository: DatabaseGameRepository
) : ViewModel(), SignInViewModelDelegate by signInViewModelDelegate {

    var isAnimation = ObservableBoolean(false)

    private val _openSignInDialog = MutableLiveData<Event<Unit>>()
    val openSignInDialog: LiveData<Event<Unit>>
        get() = _openSignInDialog

    private val _navigateToCheckInvitationNumber = MutableLiveData<Event<Unit>>()
    val navigateToCheckInvitationNumber: LiveData<Event<Unit>>
        get() = _navigateToCheckInvitationNumber

    private val _navigateToCreateInvitation = MutableLiveData<Event<Unit>>()
    val navigateToCreateInvitation: LiveData<Event<Unit>>
        get() = _navigateToCreateInvitation

    private val _openGameActivity = MutableLiveData<Event<Unit>>()
    val openGameActivity: LiveData<Event<Unit>>
        get() = _openGameActivity

    private val _openRequestGameDialog = MutableLiveData<Event<Unit>>()
    val openRequestGameDialog: LiveData<Event<Unit>>
        get() = _openRequestGameDialog

    private val _onQuickMatchClicked = MutableLiveData<Event<Unit>>()
    val onQuickMatchClicked: LiveData<Event<Unit>>
        get() = _onQuickMatchClicked

    private val _openMultiPlayer = MutableLiveData<Event<Unit>>()
    val openMultiPlayer: LiveData<Event<Unit>>
        get() = _openMultiPlayer

    fun onQuickMatchClicked() {
        _onQuickMatchClicked.value = Event(Unit)
    }

    fun onAcceptInvitationClicked() {
        _navigateToCheckInvitationNumber.value = Event(Unit)
    }

    fun onCreateInvitationClicked() {
        _navigateToCreateInvitation.value = Event(Unit)
    }


    fun onGetGameRequest() {
        if (isSignedIn()) {
            gameRepository.getGameRequest(object : FirebaseSuccessListener {
                override fun onDataFound(isDataFetched: Boolean) {
                    if (isDataFetched) {
                        _openRequestGameDialog.value = Event(Unit)
                    }
                }
            })
        }
    }

    fun checkIfUserFound() {
        if (isSignedIn()) {
            userRepository.checkIfUserFound(object : FirebaseSuccessListener {
                override fun onDataFound(isDataFetched: Boolean) {
                    if (isDataFetched) {
                        Timber.d("Here open game activity")
                        _openGameActivity.value = Event(Unit)
                    }
                }
            })
        }
    }


    fun setUserDetails(binding: FragmentMultiplayerBinding) {
        if (isSignedIn()) {
            gameRepository.getCurrentUserInfo(binding)
        }
    }

    fun onDeclineButtonPressed() {
        gameRepository.deleteGameRequestAndChallenge()
    }

    fun onAcceptButtonPressed() {
        gameRepository.createGame()
        checkIfUserFound()
    }

    fun setUserIsSearching() {
        if (isSignedIn()) {
            userRepository.setUserIsSearching()
        }
    }

    fun setUserIsNotSearching() {
        if (isSignedIn()) {
            userRepository.setUsersIsNotSearching()
        }
    }

    fun checkIfUserSignIn(): Int {
        return if (isSignedIn()) VISIBLE else GONE
    }

    fun showDisplayName(): String? {
        return sharedPref.getStringShared(NAME)
    }

    fun showPhotoUrl(): String? {
        return sharedPref.getStringShared(PHOTO_URL)
    }

    fun showWinsNumber(): String? {
        return sharedPref.getStringShared(WINS)
    }

    fun showLossesNumber(): String? {
        return sharedPref.getStringShared(LOSES)
    }

    fun setUserIsNotPlaying() {
        if (isSignedIn()) {
            userRepository.setUserIsNotPlaying()
        }
    }

    fun deleteGameFromFirebase() {
        if (isSignedIn()) {
            gameRepository.deleteGameChild()
        }
    }

}