package com.quess.thing.ui.gameplay.offline_gameplay.answer


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar

import com.quess.thing.R
import com.quess.thing.databinding.FragmentOneTryDialogBinding
import com.quess.thing.ui.gameplay.multiplayer_game.lost.LostActivity
import com.quess.thing.ui.widget.CustomDimDialogFragment
import com.quess.thing.util.*
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject
import com.quess.thing.util.admob.AdUtil
import com.quess.thing.util.admob.RemoveNativeAdsHelper
import com.quess.thing.util.objects.Utils.isNetworkAvailable

class OneTryDialogFragment : CustomDimDialogFragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    private lateinit var oneTryViewModel: OneMoreTryViewModel

    private val lostString = "lost"

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        oneTryViewModel = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentOneTryDialogBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@OneTryDialogFragment
            vm = oneTryViewModel
        }

        dataBinding.numberCoins.text = getString(R.string.you_have) + " ${oneTryViewModel.coins}"

        oneTryViewModel.dismissDialogAction.observe(this, EventObserver {
            dismiss()
        })

        oneTryViewModel.openLostFragment.observe(this, EventObserver {
            startActivity(LostActivity.starterIntent(requireContext(), lostString))
        })

        if (isNetworkAvailable(context!!)) {
            dataBinding.nativeCardView.visibility = View.VISIBLE
            displayNativeAd(dataBinding.root, context!!)
        }

        removeNativeAd(dataBinding.root)

        oneTryViewModel.loadAd()

        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpToast()
    }

    private fun setUpToast() {
        view?.setupToast(
            this,
            oneTryViewModel.toastText,
            Snackbar.LENGTH_SHORT
        )
    }

    private fun displayNativeAd(root: View, context: Context) {
        AdUtil.loadAdNative(root, context, R.id.my_template)
    }

    private fun removeNativeAd(root: View) {
        RemoveNativeAdsHelper(root, R.id.my_template, this).removeNativeAds(viewLifecycleOwner)
    }

    override fun onResume() {
        oneTryViewModel.onResumeVideoAd()
        super.onResume()
    }

    override fun onPause() {
        oneTryViewModel.onPauseVideoAd()
        super.onPause()
    }

    override fun onDestroy() {
        oneTryViewModel.onDestroyVideoAd()
        super.onDestroy()
    }
}
