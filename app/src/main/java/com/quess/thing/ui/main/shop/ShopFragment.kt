package com.quess.thing.ui.main.shop


import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.quess.thing.databinding.FragmentShopBinding
import com.quess.thing.repository.billing.localdb.AugmentedSkuDetails
import com.quess.thing.util.BillingClickCallback
import com.quess.thing.util.EventObserver
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Navigation
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_shop.view.*
import timber.log.Timber
import javax.inject.Inject

class ShopFragment : DaggerFragment(), BillingClickCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var shopViewModel: ShopViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        shopViewModel = viewModelProvider(viewModelFactory)

        val binding = FragmentShopBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@ShopFragment
            vm = shopViewModel
        }

        analyticsHelper.sendScreenView("Shop", requireActivity())

        shopViewModel.loadAd()

        shopViewModel.backHome.observe(this, EventObserver {
            Navigation.goToMainFragment(view!!)
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val inappAdapter = object : SkuDetailsAdapter(this) {
            override fun onSkuDetailsClicked(item: AugmentedSkuDetails) {
                onPurchase(item)
            }
        }
        attachAdapterToRecyclerView(view.inapp_inventory, inappAdapter)

        shopViewModel.inappSkuDetailsListLiveData.observe(this, Observer { it ->
            it?.let { inappAdapter.setSkuDetailsList(it) }
        })
    }

    private fun onPurchase(skuDetails: AugmentedSkuDetails) {
        shopViewModel.makePurchase(activity as Activity, skuDetails)
        Timber.d("starting purchase flow for SkuDetail:\n $skuDetails")
    }

    private fun attachAdapterToRecyclerView(recyclerView: RecyclerView, skuAdapter: SkuDetailsAdapter) {
        with(recyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = skuAdapter
        }
    }

    override fun onClick(skuDetails: AugmentedSkuDetails) {
        shopViewModel.makePurchase(activity as Activity, skuDetails)
    }

    override fun onResume() {
        shopViewModel.onResumeVideoAd()
        super.onResume()
    }

    override fun onPause() {
        shopViewModel.onPauseVideoAd()
        super.onPause()
    }

    override fun onDestroy() {
        shopViewModel.onDestroyVideoAd()
        super.onDestroy()
    }

}
