package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cn.iwgang.countdownview.CountdownView
import com.androchef.happytimer.countdowntimer.HappyTimer
import com.androchef.happytimer.countdowntimer.NormalCountDownView
import com.divyanshu.draw.widget.DrawView
import com.quess.thing.R
import com.quess.thing.databinding.AreYouDoneBinding
import com.quess.thing.databinding.ExitDialogBinding
import com.quess.thing.databinding.FragmentMultiplayerGameBinding
import com.quess.thing.ui.gameplay.multiplayer_game.lost.LostActivity
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer.MultiplayerFragment
import com.quess.thing.ui.gameplay.multiplayer_game.winner.WinnerActivity
import com.quess.thing.util.*
import com.quess.thing.util.admob.AdUtil
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.admob.RemoveInterstitialAdsHelper
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.drawing.ColorSelector
import com.quess.thing.util.drawing.DrawingTools
import com.quess.thing.util.objects.Utils.isNetworkAvailable
import dagger.android.support.DaggerFragment
import io.opencensus.internal.Utils
import kotlinx.android.synthetic.main.fragment_multiplayer_game_waiting.*
import java.io.ByteArrayOutputStream
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class MultiplayerGameFragment : DaggerFragment(), IOnBackPressed {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var vm: MultiplayerGameViewModel

    private lateinit var currentFragment: MainNavigationFragment

    @SuppressLint("SimpleDateFormat")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)
        val dataBinding = FragmentMultiplayerGameBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@MultiplayerGameFragment
            vm = this@MultiplayerGameFragment.vm
        }

        analyticsHelper.sendScreenView("DrawMultiplayer", requireActivity())

        dataBinding.countDownView.start(61000)
        onTickListenerDone(dataBinding.countDownView, dataBinding.drawView)

        displayAds(dataBinding.root) // display ads

        removeAds(dataBinding.root) // remove ads

        setUpDrawingTools(dataBinding.root)

        setPaintWidth(dataBinding.root)

        colorSelector(dataBinding.root)

        vm.goToWinnerActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, WinnerActivity::class.java))
        })

        vm.goToLostActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, LostActivity::class.java))
        })

        vm.showInterstitial.observe(viewLifecycleOwner, EventObserver {
            removeInterstitialAds(dataBinding.root)
        })

        vm.closeDrawing.observe(viewLifecycleOwner, EventObserver {
            ifCurrentUserLostGame()
        })

        vm.saveImageDrawing.observe(viewLifecycleOwner, EventObserver {
            saveDrawingAsImage(dataBinding.drawView)
        })

        vm.openAreYouDoneDialog.observe(viewLifecycleOwner, EventObserver {
            areYouDoneDialog(dataBinding.drawView)
        })

        vm.deleteDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.clearCanvas()
        })

        vm.redoDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.redo()
        })

        vm.undoDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.undo()
        })

        vm.currentUserWon.observe(viewLifecycleOwner, EventObserver {
            ifCurrentUserWonGame()
        })

        vm.currentUserWon()

        vm.currentUserLost()

        vm.setWordFromDatabase(dataBinding.wordTextView)

        return dataBinding.root
    }

    override fun onBackPressed() {
//        show Dialog if user want to exit
        exitDialog()
    }

    private fun exitDialog() {
        val binding: ExitDialogBinding = DataBindingUtil
            .inflate(LayoutInflater.from(context), R.layout.exit_dialog, null, false)

        val dialog = Dialog(context!!)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)

        binding.declineBtn.setOnClickListener {
            dialog.dismiss()
        }

        binding.acceptBtn.setOnClickListener {
            dialog.dismiss()

            ifCurrentUserLostGame()

        }
        dialog.show()
    }

    private fun areYouDoneDialog(drawView: DrawView) {
        val binding: AreYouDoneBinding = DataBindingUtil
            .inflate(LayoutInflater.from(context), R.layout.are_you_done, null, false)

        val dialog = Dialog(context!!)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)

        binding.declineBtn.setOnClickListener {
            dialog.dismiss()
        }

        binding.acceptBtn.setOnClickListener {
            if (isNetworkAvailable(context!!)) {
                dialog.dismiss()

                saveDrawingAsImage(drawView)
                consume {
                    replaceFragment(MultiplayerGameWaitingFragment())
                }
            } else {
                alertDialogNoInternetConnection(context!!)
            }

        }
        dialog.show()
    }

    override fun onStop() {
        super.onStop()
        vm.setUserIsNotPlaying()
    }

    private fun removeInterstitialAds(root: View) {
        RemoveInterstitialAdsHelper(root, this).removeInterstitialAds(
            viewLifecycleOwner,
            vm.loadInterstitialAd()
        )
    }

    private fun colorSelector(root: View) {
        ColorSelector(
            root,
            R.id.draw_view,
            R.id.image_color_black,
            R.id.image_color_red,
            R.id.image_color_yellow,
            R.id.image_color_green,
            R.id.image_color_blue,
            R.id.image_color_pink,
            R.id.image_color_brown,
            R.id.circle_view_width
        ).colorSelector(context!!)
    }

    private fun setUpDrawingTools(root: View) {
        DrawingTools().setUpDrawingTools(
            root,
            R.id.image_draw_color,
            R.id.image_draw_width,
            R.id.draw_tools,
            R.id.draw_color_palette,
            R.id.circle_view_width,
            R.id.seekBar_width
        )
    }

    private fun setPaintWidth(root: View) {
        DrawingTools().setPaintWidth(
            root,
            R.id.seekBar_width,
            R.id.draw_view,
            R.id.circle_view_width
        )
    }

    private fun displayAds(root: View) {
        AdUtil.loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }

    private fun onTickListenerDone(countDownTime: CountdownView?, drawView: DrawView) {
        if (isNetworkAvailable(context!!)) {
            countDownTime?.setOnCountdownEndListener {
                saveDrawingAsImage(drawView)
                consume {
                    replaceFragment(MultiplayerGameWaitingFragment())
                }
            }
        } else {
            alertDialogNoInternetConnection(context!!)
        }
    }

    private fun saveDrawingAsImage(drawView: DrawView) {
        val bStream = ByteArrayOutputStream()
        val bitmap = drawView.getBitmap()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream)
        val byteArray = bStream.toByteArray()

        vm.saveImage(byteArray)
    }

    private fun ifCurrentUserWonGame() {
        vm.setCurrentUserIDWinner()
        vm.goToWinnerActivity()
    }

    private fun ifCurrentUserLostGame() {
        vm.setOpponentIDWinner()
        vm.goToLostActivity()
    }

    private fun <F> replaceFragment(fragment: F) where F : Fragment, F : MainNavigationFragment {
        fragmentManager!!.inTransaction {
            currentFragment = fragment
            replace(MultiplayerFragment.FRAGMENT_ID, fragment).addToBackStack(null)
        }
    }


}
