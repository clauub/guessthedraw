package com.quess.thing.ui.gameplay.game_dialogs.check_invitation_number

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.util.Event
import com.quess.thing.util.FirebaseSuccessListener
import javax.inject.Inject

class CheckInvitationNumberViewModel @Inject constructor(private val gameRepository: DatabaseGameRepository) :
    ViewModel() {

    private val _dismissDialogAction = MutableLiveData<Event<Unit>>()
    val dismissDialogAction: LiveData<Event<Unit>>
        get() = _dismissDialogAction

    private val _navigateToAnswer = MutableLiveData<Event<Unit>>()
    val navigateToAnswer: LiveData<Event<Unit>>
        get() = _navigateToAnswer

    fun onCloseIconClicked() {
        _dismissDialogAction.value = Event(Unit)
    }

    fun checkInvitationId(invitationNumber: String) {
        gameRepository.checkInvitationNumber(invitationNumber)
    }

    fun ifGameExistsGoToGame() {
        gameRepository.checkIfGameExist(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _navigateToAnswer.value = Event(Unit)
                _dismissDialogAction.value = Event(Unit)
            }

        })
    }

}