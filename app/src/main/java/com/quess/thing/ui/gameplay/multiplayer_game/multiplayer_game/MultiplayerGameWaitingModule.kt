package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class MultiplayerGameWaitingModule {

    /**
     * Generates an [AndroidInjector] for the [MultiplayerGameWaitingFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeMultiplayerGameWaitingFragment(): MultiplayerGameWaitingFragment


    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [MultiplayerGameWaitingViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(MultiplayerGameWaitingViewModel::class)
    abstract fun bindMultiplayerGameWaitingViewModel(viewModel: MultiplayerGameWaitingViewModel): ViewModel

}