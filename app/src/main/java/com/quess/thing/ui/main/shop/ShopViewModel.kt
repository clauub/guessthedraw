package com.quess.thing.ui.main.shop

import android.app.Activity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.quess.thing.App
import com.quess.thing.R
import com.quess.thing.repository.billing.BillingRepository
import com.quess.thing.repository.billing.localdb.AugmentedSkuDetails
import com.quess.thing.repository.billing.localdb.GasTank
import com.quess.thing.repository.billing.localdb.PremiumCar
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Constants.COINS_KEY
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject

class ShopViewModel @Inject constructor(
    private val sharedPref: SharedPref,
    private val context: App,
    private val analyticsHelper: AnalyticsHelper
) : AndroidViewModel(context), RewardedVideoAdListener {

    val coins = sharedPref.getCoins(COINS_KEY)
    private val reward12Coins = 12

    val inappSkuDetailsListLiveData: LiveData<List<AugmentedSkuDetails>>
    private val premiumCarLiveData: LiveData<PremiumCar>

    private val viewModelScope = CoroutineScope(Job() + Dispatchers.Main)
    private val repository: BillingRepository = BillingRepository.getInstance(context)

    private val _backHome = MutableLiveData<Event<Unit>>()
    val backHome: LiveData<Event<Unit>>
        get() = _backHome

    private var rewardedVideoAd: RewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context)

    init {
        repository.startDataSourceConnections()
        premiumCarLiveData = repository.premiumCarLiveData
        inappSkuDetailsListLiveData = repository.inappSkuDetailsListLiveData
        rewardedVideoAd.rewardedVideoAdListener = this
    }

    private fun get5RewardedCoins() {
        sharedPref.saveCoins(COINS_KEY, coins + reward12Coins)
    }

    fun makePurchase(activity: Activity, skuDetails: AugmentedSkuDetails) {
        repository.launchBillingFlow(activity, skuDetails)
    }

    fun onIconBackPressed() {
        _backHome.value = Event(Unit)
    }

    fun onWatchAdClicked() {
        analyticsHelper.logUiEvent("WatchADFromShop", AnalyticsActions.CLICK)
        if (rewardedVideoAd.isLoaded) {
            rewardedVideoAd.show()
        }
    }

    fun loadAd() {
        rewardedVideoAd.loadAd(
            context.getString(R.string.ad_rewarded),
            AdRequest.Builder().build()
        )
    }

    fun onResumeVideoAd() {
        rewardedVideoAd.resume(context)
    }

    fun onPauseVideoAd() {
        rewardedVideoAd.pause(context)
    }

    fun onDestroyVideoAd() {
        rewardedVideoAd.destroy(context)
    }

    override fun onRewardedVideoAdClosed() {
        Timber.d("Video ad closed")
        loadAd()
    }

    override fun onRewardedVideoAdLeftApplication() {
        Timber.d("Video ad left application")
    }

    override fun onRewardedVideoAdLoaded() {
        Timber.d("Video ad loaded")
    }

    override fun onRewardedVideoAdOpened() {
        Timber.d("Video ad opened")
    }

    override fun onRewardedVideoCompleted() {
        Timber.d("Video complete")
    }

    override fun onRewarded(p0: RewardItem?) {
        Timber.d("You got 5 coins reward")
        get5RewardedCoins()
    }

    override fun onRewardedVideoStarted() {
        Timber.d("Video ad started")
    }

    override fun onRewardedVideoAdFailedToLoad(p0: Int) {
        Timber.d("Video ad failed to load")
    }

    override fun onCleared() {
        super.onCleared()
        Timber.d("onCleared")
        repository.endDataSourceConnections()
        viewModelScope.coroutineContext.cancel()
    }

}