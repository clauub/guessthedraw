package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class MultiplayerModule {

    /**
     * Generates an [AndroidInjector] for the [MultiplayerFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeGameFragment(): MultiplayerFragment


    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [MultiplayerViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(MultiplayerViewModel::class)
    abstract fun bindGameViewModel(viewModel: MultiplayerViewModel): ViewModel


}