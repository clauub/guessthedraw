package com.quess.thing.ui.gameplay.multiplayer_game.winner

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class WinnerModule {

    /**
     * Generates an [AndroidInjector] for the [WinnerFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeWinnerFragment(): WinnerFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [WinnerViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(WinnerViewModel::class)
    abstract fun bindWinnerViewModel(viewModel: WinnerViewModel): ViewModel


}