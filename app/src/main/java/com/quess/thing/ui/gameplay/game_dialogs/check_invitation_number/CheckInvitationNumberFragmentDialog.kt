package com.quess.thing.ui.gameplay.game_dialogs.check_invitation_number

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import com.quess.thing.R
import com.quess.thing.databinding.FragmentCheckInvitationNumberDialogBinding
import com.quess.thing.ui.gameplay.game_dialogs.no_coins.NoCoinsDialogViewModel
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.MultiplayerAnswerActivity
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game.MultiplayerGameActivity
import com.quess.thing.ui.widget.CustomDimDialogFragment
import com.quess.thing.util.EventObserver
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.viewModelProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class CheckInvitationNumberFragmentDialog : CustomDimDialogFragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var vm: CheckInvitationNumberViewModel

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)
        val dataBinding =
            FragmentCheckInvitationNumberDialogBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@CheckInvitationNumberFragmentDialog
                vm = this@CheckInvitationNumberFragmentDialog.vm
            }

        analyticsHelper.sendScreenView("CheckInvitationDialogFragment", requireActivity())

        vm.dismissDialogAction.observe(viewLifecycleOwner, EventObserver {
            dismiss()
        })

        vm.navigateToAnswer.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, MultiplayerAnswerActivity::class.java))
        })

        getInvitationID(dataBinding.editTextInvitation, dataBinding)

        vm.ifGameExistsGoToGame()

        return dataBinding.root
    }

    private fun getInvitationID(
        editText: TextInputEditText,
        dataBinding: FragmentCheckInvitationNumberDialogBinding
    ) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                dataBinding.invitationID = p0.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })
    }


}
