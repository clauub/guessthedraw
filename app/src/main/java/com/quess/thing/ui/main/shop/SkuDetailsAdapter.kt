package com.quess.thing.ui.main.shop

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.quess.thing.R
import com.quess.thing.databinding.RemoveAdItemBinding
import com.quess.thing.repository.billing.localdb.AugmentedSkuDetails
import com.quess.thing.util.BillingClickCallback

open class SkuDetailsAdapter(private val billingClickCallback: BillingClickCallback) :
    RecyclerView.Adapter<SkuDetailsAdapter.SkuDetailsViewHolder>() {

    private var skuDetailsList = emptyList<AugmentedSkuDetails>()

    override fun getItemCount() = skuDetailsList.size

    override fun onBindViewHolder(holder: SkuDetailsViewHolder, position: Int) {
        getItem(position)?.let {
            with(holder.binding) {
                skuDetails = it
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SkuDetailsViewHolder {
        return SkuDetailsViewHolder.create(parent, billingClickCallback)
    }

    private fun getItem(position: Int) = if (skuDetailsList.isEmpty()) null else skuDetailsList[position]

    fun setSkuDetailsList(list: List<AugmentedSkuDetails>) {
        if (list != skuDetailsList) {
            skuDetailsList = list
            notifyDataSetChanged()
        }
    }

    open fun onSkuDetailsClicked(item: AugmentedSkuDetails) {
        //clients to implement for callback if needed
    }

    class SkuDetailsViewHolder(internal val binding: RemoveAdItemBinding) : RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun create(parent: ViewGroup, billingClickCallback: BillingClickCallback): SkuDetailsViewHolder {
                val binding: RemoveAdItemBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.remove_ad_item, parent, false
                )
                with(binding) {
                    callback = billingClickCallback
                }
                return SkuDetailsViewHolder(binding)
            }
        }

    }
}
