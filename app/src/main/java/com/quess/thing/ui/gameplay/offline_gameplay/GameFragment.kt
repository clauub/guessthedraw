package com.quess.thing.ui.gameplay.offline_gameplay

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R
import com.quess.thing.databinding.AreYouDoneBinding
import com.quess.thing.databinding.FragmentGameBinding
import com.quess.thing.util.*
import com.quess.thing.util.admob.AdUtil
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.admob.RemoveInterstitialAdsHelper
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.drawing.ColorSelector
import com.quess.thing.util.drawing.DrawingTools
import com.quess.thing.util.objects.Navigation
import com.quess.thing.util.objects.Navigation.goToAnswerFragment
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class GameFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var gameViewModel: GameViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        gameViewModel = viewModelProvider(viewModelFactory)
        val dataBinding = FragmentGameBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@GameFragment
            vm = this@GameFragment.gameViewModel
        }

        analyticsHelper.sendScreenView("Draw", requireActivity())

        displayAds(dataBinding.root) // display ads

        removeAds(dataBinding.root) // remove ads

        setUpDrawingTools(dataBinding.root)

        setPaintWidth(dataBinding.root)

        colorSelector(dataBinding.root)

        gameViewModel.closeDrawing.observe(viewLifecycleOwner, Observer {
            Navigation.goToMainFragment(view!!)
        })

        gameViewModel.deleteDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.clearCanvas()
        })

        gameViewModel.redoDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.redo()
        })

        gameViewModel.undoDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.undo()
        })

        gameViewModel.openAlertDialog.observe(viewLifecycleOwner, EventObserver {
            val bundle = bundleOf("word" to arguments?.getString("word"))

            areYouDoneDialog(view!!, bundle)
        })

        gameViewModel.showInterstitial.observe(viewLifecycleOwner, EventObserver {
            removeInterstitialAds(dataBinding.root)
        })

        return dataBinding.root
    }

    private fun areYouDoneDialog(view: View, bundle: Bundle) {
        val binding: AreYouDoneBinding = DataBindingUtil
            .inflate(LayoutInflater.from(context), R.layout.are_you_done, null, false)

        val dialog = Dialog(context!!)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)

        binding.declineBtn.setOnClickListener {
            dialog.dismiss()
        }

        binding.acceptBtn.setOnClickListener {
            dialog.dismiss()
            goToAnswerFragment(view, bundle)
        }
        dialog.show()
    }

    private fun removeInterstitialAds(root: View) {
        RemoveInterstitialAdsHelper(root, this).removeInterstitialAds(
            viewLifecycleOwner,
            gameViewModel.loadInterstitialAd()
        )
    }

    private fun colorSelector(root: View) {
        ColorSelector(
            root,
            R.id.draw_view,
            R.id.image_color_black,
            R.id.image_color_red,
            R.id.image_color_yellow,
            R.id.image_color_green,
            R.id.image_color_blue,
            R.id.image_color_pink,
            R.id.image_color_brown,
            R.id.circle_view_width
        ).colorSelector(context!!)
    }


    private fun setUpDrawingTools(root: View) {
        DrawingTools().setUpDrawingTools(
            root,
            R.id.image_draw_color,
            R.id.image_draw_width,
            R.id.draw_tools,
            R.id.draw_color_palette,
            R.id.circle_view_width,
            R.id.seekBar_width
        )
    }

    private fun setPaintWidth(root: View) {
        DrawingTools().setPaintWidth(
            root,
            R.id.seekBar_width,
            R.id.draw_view,
            R.id.circle_view_width
        )
    }

    private fun displayAds(root: View) {
        AdUtil.loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }

}

