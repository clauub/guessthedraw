package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Module
internal abstract class MultiplayerGameModule {

    /**
     * Generates an [AndroidInjector] for the [MultiplayerGameFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeMultiplayerGameFragment(): MultiplayerGameFragment


    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [MultiplayerGameViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(MultiplayerGameViewModel::class)
    abstract fun bindMultiplayerGameViewModel(viewModel: MultiplayerGameViewModel): ViewModel

}