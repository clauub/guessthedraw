package com.quess.thing.ui.gameplay.offline_gameplay.category

import android.content.Context
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.R
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.objects.Constants.UNLOCK_ACHIEVEMENT
import javax.inject.Inject

class UnlockCardViewModel @Inject constructor(
    private val sharedPref: SharedPref,
    private val context: Context,
    private val analyticsHelper: AnalyticsHelper
) : ViewModel() {

    private val _dismissDialogAction = MutableLiveData<Event<Unit>>()
    val dismissDialogAction: LiveData<Event<Unit>>
        get() = _dismissDialogAction

    private val _goBackFragment = MutableLiveData<Event<Unit>>()
    val goBackFragment: LiveData<Event<Unit>>
        get() = _goBackFragment

    private val _toastText = MutableLiveData<Event<Int>>()
    val toastText: LiveData<Event<Int>> = _toastText

    val coins = sharedPref.getCoins(Constants.COINS_KEY)
    private val neededCoins = 50

    fun onCancel() {
        _dismissDialogAction.value = Event(Unit)
        analyticsHelper.logUiEvent("NOClickedUnlockCardDialog", AnalyticsActions.CLICK)
    }

    fun onOkPressed(cardInt: Int, view: View) {
        analyticsHelper.logUiEvent("OKClickedUnlockCardDialog", AnalyticsActions.CLICK)
        if (coins >= neededCoins) {
            updateCoins()
            if (cardInt == 1) {
                sharedPref.preferenceBoolean(Constants.UNLOCK_KEY_1, false)
            }
            if (cardInt == 2) {
                sharedPref.preferenceBoolean(Constants.UNLOCK_KEY_2, false)
            }
            if (cardInt == 3) {
                sharedPref.preferenceBoolean(Constants.UNLOCK_KEY_3, false)
            }
            _dismissDialogAction.value = Event(Unit)
            _goBackFragment.value = Event(Unit)
            analyticsHelper.logUiEvent(UNLOCK_ACHIEVEMENT, AnalyticsActions.CLICK)
        } else {
            _toastText.value = Event(R.string.you_dont_have_coins)
        }
    }

    private fun updateCoins() {
        sharedPref.saveCoins(Constants.COINS_KEY, coins - neededCoins)
    }
}