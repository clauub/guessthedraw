package com.quess.thing.ui.gameplay.offline_gameplay.answer

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class AnswerModule {

    /**
     * Generates an [AndroidInjector] for the [AnswerFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeAnswerFragment(): AnswerFragment

    @Binds
    @IntoMap
    @ViewModelKey(AnswerViewModel::class)
    abstract fun bindAnswerViewModel(viewModel: AnswerViewModel): ViewModel
}