package com.quess.thing.ui.main.settings


import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R
import com.quess.thing.databinding.FragmentSettingsBinding
import com.quess.thing.ui.signin.SignInEvent
import com.quess.thing.util.EventObserver
import com.quess.thing.util.objects.Navigation.goToMainFragment
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.signin.SignInHandler
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class SettingsFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    @Inject
    lateinit var signInHandler: SignInHandler

    private lateinit var settingsViewModel: SettingsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        settingsViewModel = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentSettingsBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@SettingsFragment
            vm = this@SettingsFragment.settingsViewModel
        }

        analyticsHelper.sendScreenView("Settings", requireActivity())

        settingsViewModel.backHome.observe(this, EventObserver {
            goToMainFragment(view!!)
        })

        settingsViewModel.shareApp.observe(this, EventObserver {
            shareApp()
        })

        settingsViewModel.sendEmail.observe(this, EventObserver {
            addToCalendar()
        })

        settingsViewModel.rateApp.observe(this, EventObserver {
            rateApp()
        })
        settingsViewModel.performSignInEvent.observe(this, EventObserver { signInRequest ->
            if (signInRequest == SignInEvent.RequestSignOut) {
                signInHandler.signOut(requireContext())
            }
        })

        return dataBinding.root
    }

    private fun shareApp() {
        ShareCompat.IntentBuilder.from(context!! as Activity)
            .setType("text/plain")
            .setChooserTitle(R.string.share)
            .startChooser()
    }

    private fun addToCalendar() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(context!!.getString(R.string.email)))
        intent.putExtra(Intent.EXTRA_SUBJECT, arrayOf(context!!.getString(R.string.app_name)))
        intent.putExtra(Intent.EXTRA_TEXT, "")
        if (intent.resolveActivity(requireContext().packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun rateApp() {
        val uri = Uri.parse("market://details?id=" + context?.packageName)
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(
            Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        )
        try {
            context?.startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            context?.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context?.packageName)
                )
            )
        }
    }
}
