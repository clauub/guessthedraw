package com.quess.thing.ui.gameplay.offline_gameplay

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.quess.thing.R
import com.quess.thing.util.Event
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Utils
import javax.inject.Inject

class GameViewModel @Inject constructor(
    private val context: Context,
    private val analyticsHelper: AnalyticsHelper
) :
    ViewModel() {

    private val _openAlertDialog = MutableLiveData<Event<Unit>>()
    val openAlertDialog: LiveData<Event<Unit>>
        get() = _openAlertDialog

    private val _closeDrawing = MutableLiveData<Event<Unit>>()
    val closeDrawing: LiveData<Event<Unit>>
        get() = _closeDrawing

    private val _deleteDrawing = MutableLiveData<Event<Unit>>()
    val deleteDrawing: LiveData<Event<Unit>>
        get() = _deleteDrawing

    private val _redoDrawing = MutableLiveData<Event<Unit>>()
    val redoDrawing: LiveData<Event<Unit>>
        get() = _redoDrawing

    private val _undoDrawing = MutableLiveData<Event<Unit>>()
    val undoDrawing: LiveData<Event<Unit>>
        get() = _undoDrawing

    private val _showInterstitial = MutableLiveData<Event<Unit>>()
    val showInterstitial: LiveData<Event<Unit>>
        get() = _showInterstitial

    var mInterstitialAd: InterstitialAd = InterstitialAd(context)

    init {
        mInterstitialAd.adUnitId = context.getString(R.string.ad_interstitial)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    fun loadInterstitialAd() {
        if (mInterstitialAd.isLoaded) {
            mInterstitialAd.show()
        } else if (!Utils.isNetworkAvailable(context)) {
            _closeDrawing.value = Event(Unit)
        }
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                _closeDrawing.value = Event(Unit)
                mInterstitialAd.loadAd(AdRequest.Builder().build())
            }
        }
    }

    fun gameFabPressed() {
        _openAlertDialog.value = Event(Unit)
        analyticsHelper.logUiEvent("FABDoneDrawing", AnalyticsActions.CLICK)
    }

    fun closeIcon() {
        _showInterstitial.value = Event(Unit)
        analyticsHelper.logUiEvent("CloseDrawing", AnalyticsActions.CLICK)
    }

    fun onDeletePressed() {
        _deleteDrawing.value = Event(Unit)
        analyticsHelper.logUiEvent("DeleteDrawing", AnalyticsActions.CLICK)
    }

    fun onRedoPressed() {
        _redoDrawing.value = Event(Unit)
        analyticsHelper.logUiEvent("RedoDrawing", AnalyticsActions.CLICK)
    }

    fun onUndoPressed() {
        _undoDrawing.value = Event(Unit)
        analyticsHelper.logUiEvent("UndoDrawing", AnalyticsActions.CLICK)
    }

}

