package com.quess.thing.ui.gameplay.offline_gameplay.category

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.card.MaterialCardView
import com.quess.thing.R
import com.quess.thing.util.*
import com.quess.thing.util.objects.Navigation.goToMainActivity
import com.quess.thing.util.objects.Navigation.goToWordFragment
import com.quess.thing.util.objects.Utils.randomWordFromList
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.objects.Constants.UNLOCK_KEY_1
import com.quess.thing.util.objects.Constants.UNLOCK_KEY_2
import com.quess.thing.util.objects.Constants.UNLOCK_KEY_3
import java.util.*
import javax.inject.Inject

class CategoryViewModel @Inject constructor(
    private val context: Context,
    private val sharedPref: SharedPref,
    private val analyticsHelper: AnalyticsHelper
) : ViewModel() {

    private val _toastText = MutableLiveData<Event<Int>>()
    val toastText: LiveData<Event<Int>> = _toastText

    val coins = sharedPref.getCoins(Constants.COINS_KEY)

    private val animalsList = ListString(context).ANIMALS_LIST
    private val foodsList = ListString(context).FOODS_LIST
    private val dessertsList = ListString(context).DESSERTS_LIST
    private val produceList = ListString(context).PRODUCE_LIST
    private val sportsList = ListString(context).SPORTS_LIST
    private val clothesList = ListString(context).CLOTHES_LIST
    private val playersList = ListString(context).PLAYER_LIST
    private val homeList = ListString(context).HOME_LIST
    private val techList = ListString(context).TECH_LIST
    val statusLocked1 = sharedPref.getLocked1(UNLOCK_KEY_1)
    val statusLocked2 = sharedPref.getLocked1(UNLOCK_KEY_2)
    val statusLocked3 = sharedPref.getLocked1(UNLOCK_KEY_3)
    private val random = Random()
    var isAnimation = ObservableBoolean(false)


    fun goToWordAnimals(view: View) {
        goToWordFragment(view, getBundleList(animalsList))
        analyticsHelper.logUiEvent("Animals", AnalyticsActions.CLICK)
    }

    fun goToWordFoods(view: View) {
        goToWordFragment(view, getBundleList(foodsList))
        analyticsHelper.logUiEvent("Foods", AnalyticsActions.CLICK)
    }

    fun goToWordDesserts(view: View) {
        goToWordFragment(view, getBundleList(dessertsList))
        analyticsHelper.logUiEvent("Desserts", AnalyticsActions.CLICK)
    }

    fun goToWordProduce(view: View) {
        goToWordFragment(view, getBundleList(produceList))
        analyticsHelper.logUiEvent("Produce", AnalyticsActions.CLICK)
    }

    fun goToWordSports(view: View) {
        goToWordFragment(view, getBundleList(sportsList))
        analyticsHelper.logUiEvent("Sports", AnalyticsActions.CLICK)
    }

    fun goToWordClothes(view: View) {
        goToWordFragment(view, getBundleList(clothesList))
        analyticsHelper.logUiEvent("Clothes", AnalyticsActions.CLICK)
    }

    fun goToWordHome(view: View) {
        goToWordFragment(view, getBundleList(homeList))
        analyticsHelper.logUiEvent("Home", AnalyticsActions.CLICK)
    }

    fun goToWordTech(view: View) {
        goToWordFragment(view, getBundleList(techList))
        analyticsHelper.logUiEvent("Tech", AnalyticsActions.CLICK)
    }

    fun goBackToHome(view: View) {
        goToMainActivity(view)
        analyticsHelper.logUiEvent("BackHomeFromCategory", AnalyticsActions.CLICK)
    }

    private fun getBundleList(word: List<String>): Bundle {
        return bundleOf(
            "word" to randomWordFromList(word, random),
            "player" to randomWordFromList(playersList, random)
        )
    }

    fun cardIsDisabled(
        cardItem: MaterialCardView,
        iconImage: ImageView,
        drawable: Drawable
    ) {
        cardItem.setCardBackgroundColor(context.getColor(R.color.light_textColor))
        iconImage.setImageDrawable(drawable)
    }

}