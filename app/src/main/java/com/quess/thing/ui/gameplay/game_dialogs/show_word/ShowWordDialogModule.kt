package com.quess.thing.ui.gameplay.game_dialogs.show_word

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.ChildFragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import com.quess.thing.ui.gameplay.game_dialogs.show_first_letter.ShowFirstLetterDialogFragment
import com.quess.thing.ui.gameplay.game_dialogs.show_first_letter.ShowFirstLetterViewModel
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class ShowWordDialogModule {

    /**
     * Generates an [AndroidInjector] for the [ShowWordDialogFragment].
     */
    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeShowWordDialogModule(): ShowWordDialogFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [ShowWordDialogViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(ShowWordDialogViewModel::class)
    abstract fun bindShowShowWordDialogViewModel(viewModel: ShowWordDialogViewModel): ViewModel
}