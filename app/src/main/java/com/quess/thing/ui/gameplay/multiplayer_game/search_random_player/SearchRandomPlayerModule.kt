package com.quess.thing.ui.gameplay.multiplayer_game.search_random_player

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class SearchRandomPlayerModule {

    /**
     * Generates an [AndroidInjector] for the [SearchRandomPlayerFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeSearchRandomPlayerFragment(): SearchRandomPlayerFragment


    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [SearchRandomPlayerViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(SearchRandomPlayerViewModel::class)
    abstract fun bindSearchRandomPlayerViewModel(viewModel: SearchRandomPlayerViewModel): ViewModel


}