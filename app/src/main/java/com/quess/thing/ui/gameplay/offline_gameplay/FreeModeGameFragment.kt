package com.quess.thing.ui.gameplay.offline_gameplay


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider

import com.quess.thing.R
import com.quess.thing.databinding.FragmentFreeModeGameBinding
import com.quess.thing.ui.main.MainActivity
import com.quess.thing.util.*
import com.quess.thing.util.admob.AdUtil
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.admob.RemoveInterstitialAdsHelper
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.drawing.ColorSelector
import com.quess.thing.util.drawing.DrawingTools
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class FreeModeGameFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var gameViewModel: GameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        gameViewModel = viewModelProvider(viewModelFactory)
        val dataBinding = FragmentFreeModeGameBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@FreeModeGameFragment
            vm = this@FreeModeGameFragment.gameViewModel
        }

        analyticsHelper.sendScreenView("FreeModeDraw", requireActivity())

        displayAds(dataBinding.root) // display ads

        removeAds(dataBinding.root) // remove ads

        setUpDrawingTools(dataBinding.root)

        setPaintWidth(dataBinding.root)

        colorSelector(dataBinding.root)

        gameViewModel.closeDrawing.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, MainActivity::class.java))
        })

        gameViewModel.deleteDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.clearCanvas()
        })

        gameViewModel.redoDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.redo()
        })

        gameViewModel.undoDrawing.observe(viewLifecycleOwner, EventObserver {
            dataBinding.drawView.undo()
        })

        gameViewModel.showInterstitial.observe(viewLifecycleOwner, EventObserver {
            removeInterstitialAds(dataBinding.root)
        })

        return dataBinding.root
    }

    private fun removeInterstitialAds(root: View) {
        RemoveInterstitialAdsHelper(root, this).removeInterstitialAds(
            viewLifecycleOwner,
            gameViewModel.loadInterstitialAd()
        )
    }

    private fun colorSelector(root: View) {
        ColorSelector(
            root,
            R.id.draw_view,
            R.id.image_color_black,
            R.id.image_color_red,
            R.id.image_color_yellow,
            R.id.image_color_green,
            R.id.image_color_blue,
            R.id.image_color_pink,
            R.id.image_color_brown,
            R.id.circle_view_width
        ).colorSelector(context!!)
    }

    private fun setUpDrawingTools(root: View) {
        DrawingTools().setUpDrawingTools(
            root,
            R.id.image_draw_color,
            R.id.image_draw_width,
            R.id.draw_tools,
            R.id.draw_color_palette,
            R.id.circle_view_width,
            R.id.seekBar_width
        )
    }

    private fun setPaintWidth(root: View) {
        DrawingTools().setPaintWidth(
            root,
            R.id.seekBar_width,
            R.id.draw_view,
            R.id.circle_view_width
        )
    }

    private fun displayAds(root: View) {
        AdUtil.loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }
}
