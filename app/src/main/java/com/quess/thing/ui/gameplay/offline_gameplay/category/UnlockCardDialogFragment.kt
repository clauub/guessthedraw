package com.quess.thing.ui.gameplay.offline_gameplay.category


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar

import com.quess.thing.R
import com.quess.thing.databinding.FragmentUnlockCardDialogBinding
import com.quess.thing.ui.widget.CustomDimDialogFragment
import com.quess.thing.util.EventObserver
import com.quess.thing.util.objects.Constants.CARD_INT_KEY
import com.quess.thing.util.setupToast
import com.quess.thing.util.viewModelProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import timber.log.Timber
import javax.inject.Inject


class UnlockCardDialogFragment : CustomDimDialogFragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    private lateinit var unlockCardViewModel: UnlockCardViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    @SuppressLint("SetTextI18n", "BinaryOperationInTimber")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        unlockCardViewModel = viewModelProvider(viewModelFactory)

        val dataBinding =
            FragmentUnlockCardDialogBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@UnlockCardDialogFragment
                vm = unlockCardViewModel
            }

        val cardInt = arguments!!.getInt(CARD_INT_KEY)

        dataBinding.numberCoins.text =
            getString(R.string.you_have) + " ${unlockCardViewModel.coins}"

        unlockCardViewModel.dismissDialogAction.observe(this, EventObserver {
            dismiss()
        })

        unlockCardViewModel.goBackFragment.observe(this, EventObserver {
            fragmentManager?.popBackStack()
        })

        dataBinding.okBtn.setOnClickListener {
            unlockCardViewModel.onOkPressed(cardInt, view!!)
        }

        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpToast()
    }

    private fun setUpToast() {
        view?.setupToast(
            this,
            unlockCardViewModel.toastText,
            Snackbar.LENGTH_SHORT
        )
    }

}
