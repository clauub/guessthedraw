package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_game

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.util.Event
import com.quess.thing.util.FirebaseSuccessListener
import com.quess.thing.util.ListString
import com.quess.thing.util.alertDialogNoInternetConnection
import com.quess.thing.util.objects.Utils
import com.quess.thing.util.objects.Utils.isNetworkAvailable
import com.quess.thing.util.objects.Utils.randomWordFromList
import kotlinx.coroutines.*
import java.util.*
import javax.inject.Inject

class SelectWordToDrawViewModel @Inject constructor(
    private val context: Context,
    private val gameRepository: DatabaseGameRepository
) : ViewModel() {

    private val easyList = ListString(context).EASY_WORD_LIST
    private val mediumList = ListString(context).MEDIUM_WORD_LIST
    private val hardList = ListString(context).HARD_WORD_LIST
    private val random = Random()

    private val _openMultiplayerGameFragment = MutableLiveData<Event<Unit>>()
    val openMultiplayerGameFragment: LiveData<Event<Unit>>
        get() = _openMultiplayerGameFragment

    private val _goToLostActivity = MutableLiveData<Event<Unit>>()
    val goToLostActivity: LiveData<Event<Unit>>
        get() = _goToLostActivity

    private val _goToWinnerActivity = MutableLiveData<Event<Unit>>()
    val goToWinnerActivity: LiveData<Event<Unit>>
        get() = _goToWinnerActivity

    private val _currentUserWon = MutableLiveData<Event<Unit>>()
    val currentUserWon: LiveData<Event<Unit>>
        get() = _currentUserWon

    private val _easyWord = MutableLiveData<Event<Unit>>()
    val easyWord: LiveData<Event<Unit>>
        get() = _easyWord

    private val _mediumWord = MutableLiveData<Event<Unit>>()
    val mediumWord: LiveData<Event<Unit>>
        get() = _mediumWord

    private val _hardWord = MutableLiveData<Event<Unit>>()
    val hardWord: LiveData<Event<Unit>>
        get() = _hardWord


    private val _openNoConnectionDialog = MutableLiveData<Event<Unit>>()
    val openNoConnectionDialog: LiveData<Event<Unit>>
        get() = _openNoConnectionDialog

    fun saveWordToDatabase(word: String) {
        gameRepository.saveWordToDrawGame(word)
    }

    fun onEasyWordClicked() {
        if (isNetworkAvailable(context)) {
            _openMultiplayerGameFragment.value = Event(Unit)
            _easyWord.value = Event(Unit)
        } else {
            _openNoConnectionDialog.value = Event(Unit)
        }
    }

    fun onMediumWordClicked() {
        if (isNetworkAvailable(context)) {
            _openMultiplayerGameFragment.value = Event(Unit)
            _mediumWord.value = Event(Unit)
        } else {
            _openNoConnectionDialog.value = Event(Unit)
        }
    }

    fun onHardWordClicked() {
        if (isNetworkAvailable(context)) {
            _openMultiplayerGameFragment.value = Event(Unit)
            _hardWord.value = Event(Unit)
        } else {
            _openNoConnectionDialog.value = Event(Unit)
        }
    }


    fun randomEasyWordList(): String {
        return randomWordFromList(easyList, random)
    }

    fun randomMediumWordList(): String {
        return randomWordFromList(mediumList, random)
    }

    fun randomHardWordList(): String {
        return randomWordFromList(hardList, random)
    }

    fun setCurrentUserIDWinner() {
        gameRepository.setCurrentUserIDWinner()
    }

    fun goToWinnerActivity() {
        _goToWinnerActivity.value = Event(Unit)
    }

    private fun setOpponentIDWinner() {
        gameRepository.setOpponentUserIDWinner()
    }

    fun currentUserWon() {
        gameRepository.ifCurrentUserWon(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _currentUserWon.value = Event(Unit)
            }
        })
    }

    fun currentUserLost() {
        gameRepository.ifCurrentUserLost(object : FirebaseSuccessListener {
            override fun onDataFound(isDataFetched: Boolean) {
                _goToLostActivity.value = Event(Unit)
            }

        })
    }

    fun setWinnerAfter10SecondsInactivity() {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                delay(10000) // 10 seconds

                setOpponentIDWinner()

            }
        }
    }

}