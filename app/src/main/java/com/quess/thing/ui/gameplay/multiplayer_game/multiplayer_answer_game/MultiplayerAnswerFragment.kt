package com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import cn.iwgang.countdownview.CountdownView
import com.google.android.material.snackbar.Snackbar
import com.quess.thing.R
import com.quess.thing.databinding.ExitDialogBinding
import com.quess.thing.databinding.FragmentMultiplayerAnswerBinding.inflate
import com.quess.thing.ui.gameplay.game_dialogs.no_coins.NoCoinsFragmentDialog
import com.quess.thing.ui.gameplay.game_dialogs.show_first_letter.ShowFirstLetterDialogFragment
import com.quess.thing.ui.gameplay.game_dialogs.show_word.ShowWordDialogFragment
import com.quess.thing.ui.gameplay.multiplayer_game.lost.LostActivity
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter.GridViewLetterAdapterMultiplayer
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter.GridViewLetterClickListener
import com.quess.thing.ui.gameplay.multiplayer_game.winner.WinnerActivity
import com.quess.thing.ui.gameplay.offline_gameplay.answer.OneTryDialogFragment
import com.quess.thing.util.*
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Utils.isNetworkAvailable
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_multiplayer_answer.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MultiplayerAnswerFragment : DaggerFragment(), IOnBackPressed, GridViewLetterClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var vm: MultiplayerAnswerViewModel

    private var attempts: Int = 0

    @SuppressLint("DefaultLocale")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)
        val dataBinding =
            inflate(inflater, container, false).apply {
                lifecycleOwner = this@MultiplayerAnswerFragment
                vm = this@MultiplayerAnswerFragment.vm
            }

        analyticsHelper.sendScreenView("MultiplayerAnswerFragment", requireActivity())
        dataBinding.countDownView?.start(59000)

        onTickListenerDone(dataBinding.countDownView)

        vm.openFirstLetterDialog.observe(this, EventObserver {
            val firstLetter = dataBinding.wordTextView.text.substring(0, 1)
            openFirstLetterDialog(requireActivity(), firstLetter)
        })

        vm.goToWinnerActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(activity, WinnerActivity::class.java))
        })

        vm.goToLostActivity.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(activity, LostActivity::class.java))
        })

        vm.deleteEditTextWord.observe(viewLifecycleOwner, EventObserver {
            deleteLastLetter(dataBinding.editText)
        })

        vm.openOneMoreTryDialog.observe(viewLifecycleOwner, EventObserver {
            openMoreTryDialog()
        })

        vm.openShowWordDialog.observe(viewLifecycleOwner, EventObserver {
            val word = dataBinding.wordTextView.text.toString()
            openShowWordDialog(requireActivity(), word)
        })

        vm.openNoCoinsDialog.observe(viewLifecycleOwner, EventObserver {
            openNoCoinsDialog()
        })

        vm.checkWord.observe(viewLifecycleOwner, EventObserver {
            attempts++
            if (isNetworkAvailable(context!!)) {
                if (dataBinding.editText.text.toString().toUpperCase() == dataBinding.wordTextView.text.toString().toUpperCase()) {
                    ifCurrentUserWonGame()
                } else {
                    when {
                        attempts < 2 -> {
                            snackBarCustomColor(
                                view!!,
                                R.string.wrong_answer_one_more_try,
                                context!!.getColor(R.color.colorAccent)
                            )
                            editText.startAnimation(
                                AnimationUtils.loadAnimation(
                                    context,
                                    R.anim.shake
                                )
                            )
                        }
                        attempts == 2 -> {
                            vm.openOneMoreTryDialog()
                        }
                        attempts == 3 -> {
                            ifCurrentUserLostGame()
                        }
                    }
                }
            } else {
                alertDialogNoInternetConnection(context!!)
            }
        })

        vm.currentUserWon()

        vm.currentUserLost()

        vm.setWordFromDatabase(
            dataBinding.gridView,
            context!!,
            dataBinding,
            this
        )
        return dataBinding.root
    }

    private fun openFirstLetterDialog(
        activity: FragmentActivity,
        firstLetter: String
    ) {
        val dialog = ShowFirstLetterDialogFragment.newInstance(firstLetter)
        dialog.show(
            activity.supportFragmentManager,
            ShowFirstLetterDialogFragment.DIALOG_FIRST_LETTER
        )
    }

    private fun openNoCoinsDialog() {
        val dialog = NoCoinsFragmentDialog()
        dialog.show(requireActivity().supportFragmentManager, "NO_COINS_DIALOG")
    }

    private fun openShowWordDialog(
        activity: FragmentActivity,
        word: String
    ) {
        val dialog = ShowWordDialogFragment.newInstance(word)
        dialog.show(
            activity.supportFragmentManager,
            ShowWordDialogFragment.DIALOG_SHOW_WORD
        )
    }

    private fun onTickListenerDone(countDownTime: CountdownView?) {
        countDownTime?.setOnCountdownEndListener {
            ifCurrentUserLostGame()
        }
    }

    private fun openMoreTryDialog() {
        val dialog =
            OneTryDialogFragment()
        dialog.show(requireActivity().supportFragmentManager, "DIALOG")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpSnackbar()
    }

    override fun onBackPressed() {
        exitDialog()
    }

    private fun setUpSnackbar() {
        view?.setupSnackbarWithCustomColor(
            this,
            vm.snackbarText,
            Snackbar.LENGTH_SHORT,
            context!!.getColor(R.color.colorAccent)
        )
    }

    private fun exitDialog() {
        val binding: ExitDialogBinding = DataBindingUtil
            .inflate(LayoutInflater.from(context), R.layout.exit_dialog, null, false)

        val dialog = Dialog(context!!)
        dialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)

        binding.declineBtn.setOnClickListener {
            dialog.dismiss()
        }

        binding.acceptBtn.setOnClickListener {
            dialog.dismiss()

            ifCurrentUserLostGame()
        }

        dialog.show()
    }


    @ExperimentalStdlibApi
    @SuppressLint("SetTextI18n")
    override fun onLetterItemClick(
        view: View,
        letter: String,
        lettersList: ArrayList<String>,
        position: Int
    ) {

        editText.setText(editText.text.toString() + letter)

        val adapter = GridViewLetterAdapterMultiplayer(context!!, lettersList, this, vm)
        gridView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun deleteLastLetter(editText: EditText) {
        var str: String = editText.text.toString()
        if (editText.text.trim().isNotEmpty()) {
            str = str.substring(0, str.length - 1)
            editText.setText(str)
        }
    }

    private fun ifCurrentUserWonGame() {
        vm.setCurrentUserIDWinner()
        vm.goToWinnerActivity()
    }

    private fun ifCurrentUserLostGame() {
        vm.setOpponentIDWinner()
        vm.goToLostActivity()
    }

    override fun onPause() {
        super.onPause()
        countDownView.stop()
    }

    override fun onStart() {
        super.onStart()
        vm.isAnimation.set(true)
    }
}
