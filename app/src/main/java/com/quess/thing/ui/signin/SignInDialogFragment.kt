package com.quess.thing.ui.signin


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.quess.thing.databinding.FragmentSignInDialogBinding
import com.quess.thing.ui.widget.CustomDimDialogFragment
import com.quess.thing.util.EventObserver
import com.quess.thing.util.signin.SignInHandler
import com.quess.thing.util.viewModelProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class SignInDialogFragment : CustomDimDialogFragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var signInHandler: SignInHandler

    @Inject
    lateinit var firebaseDatabase: FirebaseDatabase

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    private lateinit var signInViewModel: SignInViewModel


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        signInViewModel = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentSignInDialogBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@SignInDialogFragment
            viewModel = signInViewModel
        }

        signInViewModel.performSignInEvent.observe(
            viewLifecycleOwner,
            EventObserver { signInRequest ->
                if (signInRequest == SignInEvent.RequestSignIn) {
                    signInHandler.makeSignInIntent()?.let {
                        startActivityForResult(it, SIGN_IN_ACTIVITY_REQUEST_CODE)
                    }
                }
            })

        signInViewModel.dismissDialogAction.observe(viewLifecycleOwner, EventObserver {
            dismiss()
        })

        return dataBinding.root
    }

    companion object {
        const val DIALOG_NEED_TO_SIGN_IN = "dialog_need_to_sign_in"
        const val SIGN_IN_ACTIVITY_REQUEST_CODE = 42
    }
}
