package com.quess.thing.ui.gameplay.game_dialogs.no_coins


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R
import com.quess.thing.databinding.FragmentNoCoinsFragmentDialogBinding
import com.quess.thing.ui.main.shop.ShopFragment
import com.quess.thing.ui.widget.CustomDimDialogFragment
import com.quess.thing.util.EventObserver
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.replaceFragment
import com.quess.thing.util.viewModelProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class NoCoinsFragmentDialog : CustomDimDialogFragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var vm: NoCoinsDialogViewModel

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)
        val dataBinding =
            FragmentNoCoinsFragmentDialogBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@NoCoinsFragmentDialog
                vm = this@NoCoinsFragmentDialog.vm
            }

        analyticsHelper.sendScreenView("NoCoinsDialogFragment", requireActivity())

        vm.loadAd()

        dataBinding.textViewCoins.text = vm.coins.toString()
        vm.dismissDialogAction.observe(viewLifecycleOwner, EventObserver {
            dismiss()
        })

        vm.openShop.observe(viewLifecycleOwner, EventObserver {
            replaceFragment(context!!, R.id.container, ShopFragment())
        })


        return dataBinding.root
    }

    override fun onResume() {
        vm.onResumeVideoAd()
        super.onResume()
    }

    override fun onPause() {
        vm.onPauseVideoAd()
        super.onPause()
    }

    override fun onDestroy() {
        vm.onDestroyVideoAd()
        super.onDestroy()
    }

}
