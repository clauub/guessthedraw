package com.quess.thing.ui.main

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R
import com.quess.thing.databinding.FragmentMainBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import com.quess.thing.util.EventObserver
import com.quess.thing.util.objects.Navigation.goToCategoryFragment
import com.quess.thing.util.objects.Navigation.goToSettingsFragment
import com.quess.thing.util.objects.Navigation.goToShopFragment
import com.quess.thing.util.viewModelProvider
import com.quess.thing.util.admob.AdUtil.loadAd
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.objects.Navigation.goToFreeModeFragment
import com.quess.thing.util.signin.SignInHandler
import android.content.Intent
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import com.quess.thing.databinding.NoInternetConnectionDialogBinding
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer.MultiplayerActivity
import com.quess.thing.ui.signin.SignInDialogFragment
import com.quess.thing.util.alertDialogNoInternetConnection
import www.sanju.motiontoast.MotionToast


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MainFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var signInHandler: SignInHandler

    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mainViewModel = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentMainBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@MainFragment
            vm = this@MainFragment.mainViewModel
        }

        displayAds(dataBinding.root) // display ads

        removeAds(dataBinding.root) //

        mainViewModel.goToShop.observe(viewLifecycleOwner, EventObserver {
            goToShopFragment(view!!)
        })

        mainViewModel.goToCategory.observe(viewLifecycleOwner, EventObserver {
            goToCategoryFragment(view!!)
        })

        mainViewModel.goToSettings.observe(viewLifecycleOwner, EventObserver {
            goToSettingsFragment(view!!)
        })

        mainViewModel.goToFreeModeGame.observe(viewLifecycleOwner, EventObserver {
            goToFreeModeFragment(view!!)
        })

        mainViewModel.openSignInDialog.observe(viewLifecycleOwner, EventObserver {
            openSignInDialog()
        })

        mainViewModel.navigateToMultiplayer.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(context, MultiplayerActivity::class.java))
        })

        mainViewModel.openNoConnectionDialog.observe(viewLifecycleOwner, EventObserver {
            alertDialogNoInternetConnection(context!!)
        })

//        Update coins that were bought
        mainViewModel.buy20Coins()
        mainViewModel.buy35Coins()
        mainViewModel.buy50Coins()
        mainViewModel.buy100Coins()

        return dataBinding.root
    }

    private fun openSignInDialog() {
        val dialog = SignInDialogFragment()
        dialog.show(requireActivity().supportFragmentManager, DIALOG_NEED_TO_SIGN_IN)
    }

    private fun displayAds(root: View) {
        loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SIGN_IN_ACTIVITY_REQUEST_CODE) {
            val i = Intent(context, MultiplayerActivity::class.java)
            startActivity(i)
        }
    }

    companion object {
        const val DIALOG_NEED_TO_SIGN_IN = "dialog_need_to_sign_in"
        const val SIGN_IN_ACTIVITY_REQUEST_CODE = 42
    }

    override fun onStart() {
        super.onStart()
        mainViewModel.isAnimation.set(true)
    }
}
