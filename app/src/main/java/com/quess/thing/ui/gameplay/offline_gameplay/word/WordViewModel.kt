package com.quess.thing.ui.gameplay.offline_gameplay.word

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.util.Event
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import javax.inject.Inject

class WordViewModel @Inject constructor(private val analyticsHelper: AnalyticsHelper) :
    ViewModel() {

    var isAnimation = ObservableBoolean(false)

    private val _backHome = MutableLiveData<Event<Unit>>()
    val backHome: LiveData<Event<Unit>>
        get() = _backHome

    private val _startGame = MutableLiveData<Event<Unit>>()
    val startGame: LiveData<Event<Unit>>
        get() = _startGame

    private val _backToCategory = MutableLiveData<Event<Unit>>()
    val backToCategory: LiveData<Event<Unit>>
        get() = _backToCategory


    fun backHomePressed() {
        _backHome.value = Event(Unit)
        analyticsHelper.logUiEvent("BackHomeFromWord", AnalyticsActions.CLICK)
    }

    fun goBackToCategory() {
        _backToCategory.value = Event(Unit)
        analyticsHelper.logUiEvent("BackCategoryFromWord", AnalyticsActions.CLICK)
    }

    fun startGamePressed() {
        _startGame.value = Event(Unit)
        analyticsHelper.logUiEvent("StartDrawingWord", AnalyticsActions.CLICK)
    }
}