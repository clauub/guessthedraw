package com.quess.thing.ui.main

import android.os.Bundle
import androidx.navigation.findNavController
import com.quess.thing.R
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    // back button support
    override fun onSupportNavigateUp() =
        this.findNavController(R.id.nav_host_main_fragment).popBackStack()

    //    Disable BackButton
    override fun onBackPressed() {
    }
}

