package com.quess.thing.ui.main

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quess.thing.App
import com.quess.thing.repository.billing.BillingRepository
import com.quess.thing.repository.billing.localdb.AugmentedSkuDetails
import com.quess.thing.repository.billing.localdb.GasTank
import com.quess.thing.repository.billing.multiplayer.DatabaseUserRepository
import com.quess.thing.ui.signin.SignInViewModelDelegate
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.objects.Utils
import com.quess.thing.util.objects.Utils.isNetworkAvailable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val context: App,
    private val sharedPref: SharedPref,
    private val signInViewModelDelegate: SignInViewModelDelegate,
    private val userRepository: DatabaseUserRepository
) : ViewModel(), SignInViewModelDelegate by signInViewModelDelegate {

    private val inappSkuDetailsListLiveData: LiveData<List<AugmentedSkuDetails>>
    private val buy20Coins: LiveData<GasTank>
    private val buy35Coins: LiveData<GasTank>
    private val buy50Coins: LiveData<GasTank>
    private val buy100Coins: LiveData<GasTank>
    private val viewModelScope = CoroutineScope(Job() + Dispatchers.Main)
    private val repository: BillingRepository = BillingRepository.getInstance(context)
    var isAnimation = ObservableBoolean(false)

    val coins = sharedPref.getCoins(Constants.COINS_KEY)

    init {
        repository.startDataSourceConnections()
        buy20Coins = repository.buy20CoinsLiveData
        buy35Coins = repository.buy35CoinsLiveData
        buy50Coins = repository.buy50CoinsLiveData
        buy100Coins = repository.buy100CoinsLiveData
        inappSkuDetailsListLiveData = repository.inappSkuDetailsListLiveData
    }

    private val _goToCategory = MutableLiveData<Event<Unit>>()
    val goToCategory: LiveData<Event<Unit>>
        get() = _goToCategory

    private val _goToSettings = MutableLiveData<Event<Unit>>()
    val goToSettings: LiveData<Event<Unit>>
        get() = _goToSettings

    private val _goToShop = MutableLiveData<Event<Unit>>()
    val goToShop: LiveData<Event<Unit>>
        get() = _goToShop

    private val _goToFreeModeGame = MutableLiveData<Event<Unit>>()
    val goToFreeModeGame: LiveData<Event<Unit>>
        get() = _goToFreeModeGame

    private val _navigateToMultiplayer = MutableLiveData<Event<Unit>>()
    val navigateToMultiplayer: LiveData<Event<Unit>>
        get() = _navigateToMultiplayer

    private val _openNoConnectionDialog = MutableLiveData<Event<Unit>>()
    val openNoConnectionDialog: LiveData<Event<Unit>>
        get() = _openNoConnectionDialog

    private val _openSignInDialog = MutableLiveData<Event<Unit>>()
    val openSignInDialog: LiveData<Event<Unit>>
        get() = _openSignInDialog


    fun onMultiPlayerPressed() {
        if (!isSignedIn() && isNetworkAvailable(context)) {
            _openSignInDialog.value = Event(Unit)
        } else if (isSignedIn() && isNetworkAvailable(context)) {
            _navigateToMultiplayer.value = Event(Unit)
        } else if (!isNetworkAvailable(context)) {
            _openNoConnectionDialog.value = Event(Unit)
        }
    }

    fun buy20Coins() {
        val gas = buy20Coins.value
        gas?.apply {
            viewModelScope.launch {
                repository.updateBuy20Coins()
            }
        }
    }

    fun buy35Coins() {
        val gas = buy35Coins.value
        gas?.apply {
            viewModelScope.launch {
                repository.updateBuy35Coins()
            }
        }
    }

    fun buy50Coins() {
        val gas = buy50Coins.value
        gas?.apply {
            viewModelScope.launch {
                repository.updateBuy50Coins()
            }
        }
    }

    fun buy100Coins() {
        val gas = buy100Coins.value
        gas?.apply {
            viewModelScope.launch {
                repository.updateBuy100Coins()
            }
        }
    }

    fun onCategoryPressed() {
        _goToCategory.value = Event(Unit)
    }

    fun onShopPressed() {
        _goToShop.value = Event(Unit)
    }

    fun onSettingsPressed() {
        _goToSettings.value = Event(Unit)
    }

    fun onFreeModePressed() {
        _goToFreeModeGame.value = Event(Unit)
    }
}

