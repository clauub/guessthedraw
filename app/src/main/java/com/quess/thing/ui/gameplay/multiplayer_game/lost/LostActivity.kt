package com.quess.thing.ui.gameplay.multiplayer_game.lost

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.quess.thing.R
import dagger.android.support.DaggerAppCompatActivity

class LostActivity : DaggerAppCompatActivity() {

    companion object {

        const val EXTRA_LOST_ID = "extra.LOST_ID"

        fun starterIntent(context: Context, lostString: String): Intent {
            return Intent(context, LostActivity::class.java).apply {
                putExtra(EXTRA_LOST_ID, lostString)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lost)

    }

    //    Disable BackButton
    override fun onBackPressed() {

    }
}
