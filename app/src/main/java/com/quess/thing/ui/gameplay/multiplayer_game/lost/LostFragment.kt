package com.quess.thing.ui.gameplay.multiplayer_game.lost


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.quess.thing.R

import com.quess.thing.databinding.FragmentLostBinding
import com.quess.thing.util.EventObserver
import com.quess.thing.util.admob.AdUtil.loadAd
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.admob.RemoveInterstitialAdsHelper
import com.quess.thing.util.objects.Navigation.goToMainActivity
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class LostFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    private lateinit var vm: LostViewModel

    private val coinsNumberLost: Int = 2

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentLostBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@LostFragment
            vm = this@LostFragment.vm
        }

        displayAds(dataBinding.root)
        removeAds(dataBinding.root)

        analyticsHelper.sendScreenView("Lost", requireActivity())

        vm.backHome.observe(viewLifecycleOwner, EventObserver {
            goToMainActivity(view!!)
        })

        vm.saveCoins(coinsNumberLost)

        vm.showInterstitial.observe(viewLifecycleOwner, EventObserver {
            removeInterstitialAds(dataBinding.root)
        })

        return dataBinding.root
    }

    private fun displayAds(root: View) {
        loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }

    private fun removeInterstitialAds(root: View) {
        RemoveInterstitialAdsHelper(root, this).removeInterstitialAds(
            viewLifecycleOwner,
            vm.loadInterstitialAd()
        )
    }

    override fun onStop() {
        super.onStop()
        vm.deleteGameFromFirebase()
        vm.setUserIsNotPlaying()
    }

    override fun onStart() {
        super.onStart()
        vm.isAnimation.set(true)
    }
}

