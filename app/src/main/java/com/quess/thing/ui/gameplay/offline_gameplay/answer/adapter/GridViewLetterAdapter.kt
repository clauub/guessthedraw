package com.quess.thing.ui.gameplay.offline_gameplay.answer.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.quess.thing.R
import com.quess.thing.databinding.LetterTextViewItemBinding
import com.quess.thing.ui.gameplay.multiplayer_game.multiplayer_answer_game.adapter.GridViewLetterClickListener
import com.quess.thing.ui.gameplay.offline_gameplay.answer.AnswerViewModel


class GridViewLetterAdapter constructor(
    private val mContext: Context,
    private val lettersList: ArrayList<String>,
    private val listener: GridViewLetterClickListener,
    private val vm: AnswerViewModel
) : BaseAdapter() {


    @SuppressLint("ViewHolder", "InflateParams", "DefaultLocale")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View? {
        val letter = this.lettersList[p0]
        val holder: ViewHolder

        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (p1 == null) {
            val itemBinding = DataBindingUtil.inflate<LetterTextViewItemBinding>(
                inflater,
                R.layout.letter_text_view_item,
                p2,
                false
            )

            holder =
                ViewHolder(
                    itemBinding
                )
            holder.view = itemBinding.root
            holder.view.tag = holder
            itemBinding.executePendingBindings()

        } else {
            holder = p1.tag as ViewHolder
        }


        holder.binding.letterItem.text = letter.capitalize()
        holder.binding.vm = vm

        holder.binding.letterItem.setOnClickListener {
            listener.onLetterItemClick(holder.binding.letterItem, lettersList[p0], lettersList, p0)
        }
        return holder.binding.root
    }

    override fun getItem(p0: Int): Any {
        return lettersList[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return lettersList.size
    }


    private class ViewHolder internal constructor(val binding: LetterTextViewItemBinding) {
        var view: View = binding.root


    }


}
