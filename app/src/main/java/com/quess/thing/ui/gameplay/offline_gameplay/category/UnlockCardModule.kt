package com.quess.thing.ui.gameplay.offline_gameplay.category

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.ChildFragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class UnlockCardModule {

    /**
     * Generates an [AndroidInjector] for the [UnlockCardDialogFragment].
     */
    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeUnlockCardFragment(): UnlockCardDialogFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [UnlockCardViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(UnlockCardViewModel::class)
    abstract fun bindUnlockCardViewModel(viewModel: UnlockCardViewModel): ViewModel
}