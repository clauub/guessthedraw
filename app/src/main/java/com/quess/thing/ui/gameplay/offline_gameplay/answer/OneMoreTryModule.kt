package com.quess.thing.ui.gameplay.offline_gameplay.answer

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.ChildFragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class OneMoreTryModule {

    /**
     * Generates an [AndroidInjector] for the [OneTryDialogFragment].
     */
    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeOneTryFragment(): OneTryDialogFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [OneMoreTryViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(OneMoreTryViewModel::class)
    abstract fun bindOneMoreTryViewModel(viewModel: OneMoreTryViewModel): ViewModel
}