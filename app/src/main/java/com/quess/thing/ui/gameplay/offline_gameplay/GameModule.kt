package com.quess.thing.ui.gameplay.offline_gameplay

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class GameModule {

    /**
     * Generates an [AndroidInjector] for the [GameFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeGameFragment(): GameFragment

    /**
     * Generates an [AndroidInjector] for the [FreeModeGameFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeFreeModeGameFragment(): FreeModeGameFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [GameViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(GameViewModel::class)
    abstract fun bindGameViewModel(viewModel: GameViewModel): ViewModel


}