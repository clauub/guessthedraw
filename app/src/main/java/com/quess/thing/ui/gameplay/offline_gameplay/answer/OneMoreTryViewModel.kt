package com.quess.thing.ui.gameplay.offline_gameplay.answer

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.quess.thing.R
import com.quess.thing.repository.billing.multiplayer.DatabaseGameRepository
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.analytics.AnalyticsActions
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.objects.Constants.COINS_KEY
import timber.log.Timber
import javax.inject.Inject

class OneMoreTryViewModel @Inject constructor(
    private val sharedPref: SharedPref,
    private val context: Context,
    private val analyticsHelper: AnalyticsHelper,
    private val gameRepository: DatabaseGameRepository
) : ViewModel(), RewardedVideoAdListener {

    val coins = sharedPref.getCoins(COINS_KEY)
    private val decreaseCoins: Int = 20

    private val _dismissDialogAction = MutableLiveData<Event<Unit>>()
    val dismissDialogAction: LiveData<Event<Unit>>
        get() = _dismissDialogAction

    private val _openLostFragment = MutableLiveData<Event<Unit>>()
    val openLostFragment: LiveData<Event<Unit>>
        get() = _openLostFragment

    private val _toastText = MutableLiveData<Event<Int>>()
    val toastText: LiveData<Event<Int>> = _toastText

    private var rewardedVideoAd: RewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context)

    init {
        rewardedVideoAd.rewardedVideoAdListener = this
    }

    fun onCancel() {
        _dismissDialogAction.value = Event(Unit)
        _openLostFragment.value = Event(Unit)
        gameRepository.setOpponentUserIDWinner()
        analyticsHelper.logUiEvent("NOClickedOneTryDialog", AnalyticsActions.CLICK)
    }

    fun onOkPressed() {
        analyticsHelper.logUiEvent("OKClickedOneTryDialog", AnalyticsActions.CLICK)
        if (coins >= 20) {
            _dismissDialogAction.value = Event(Unit)
            updateCoinsToSharedPref()
        } else {
            _dismissDialogAction.value = Event(Unit)
            _toastText.value = Event(R.string.you_dont_have_coins)
            _openLostFragment.value = Event(Unit)
        }
    }

    private fun updateCoinsToSharedPref() {
        sharedPref.saveCoins(Constants.COINS_KEY, coins - decreaseCoins)
    }

    fun onWatchAdClicked() {
        analyticsHelper.logUiEvent("WatchADFromOneTry", AnalyticsActions.CLICK)
        if (rewardedVideoAd.isLoaded) {
            rewardedVideoAd.show()
        }
    }

    fun loadAd() {
        rewardedVideoAd.loadAd(
            context.getString(R.string.ad_rewarded),
            AdRequest.Builder().addTestDevice("135F1B9563701B763CCE2B7708DD997C").build()
        )
    }

    fun onResumeVideoAd() {
        rewardedVideoAd.resume(context)
    }

    fun onPauseVideoAd() {
        rewardedVideoAd.pause(context)
    }

    fun onDestroyVideoAd() {
        rewardedVideoAd.destroy(context)
    }

    override fun onRewardedVideoAdClosed() {
        Timber.d("Video ad closed")
        loadAd()
    }

    override fun onRewardedVideoAdLeftApplication() {
        Timber.d("Video ad left application")
    }

    override fun onRewardedVideoAdLoaded() {
        Timber.d("Video ad loaded")
    }

    override fun onRewardedVideoAdOpened() {
        Timber.d("Video ad opened")
    }

    override fun onRewardedVideoCompleted() {
        Timber.d("Video complete")
        _dismissDialogAction.value = Event(Unit)
    }

    override fun onRewarded(p0: RewardItem?) {
        Timber.d("You got 5 coins reward")
        _dismissDialogAction.value = Event(Unit)
    }

    override fun onRewardedVideoStarted() {
        Timber.d("Video ad started")
    }

    override fun onRewardedVideoAdFailedToLoad(p0: Int) {
        Timber.d("Video ad failed to load")
    }
}
