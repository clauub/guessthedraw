package com.quess.thing.ui.gameplay.game_dialogs.no_coins

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.ChildFragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class NoCoinsModule {

    /**
     * Generates an [AndroidInjector] for the [NoCoinsFragmentDialog].
     */
    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeNoCoinsModule(): NoCoinsFragmentDialog

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [NoCoinsDialogViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(NoCoinsDialogViewModel::class)
    abstract fun bindShowNoCoinsDialogViewModel(viewModel: NoCoinsDialogViewModel): ViewModel
}