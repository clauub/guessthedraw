package com.quess.thing.ui.gameplay.game_dialogs.show_word

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.quess.thing.R
import com.quess.thing.databinding.FragmentShowFirstLetterDialogBinding
import com.quess.thing.databinding.FragmentShowWordDialogBinding
import com.quess.thing.ui.gameplay.game_dialogs.no_coins.NoCoinsFragmentDialog
import com.quess.thing.ui.widget.CustomDimDialogFragment
import com.quess.thing.util.*
import com.quess.thing.util.analytics.AnalyticsHelper
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class ShowWordDialogFragment : CustomDimDialogFragment(), HasSupportFragmentInjector {

    companion object {
        private const val WORD = "show_word"
        const val DIALOG_SHOW_WORD = "dialog_show_word"
        fun newInstance(word: String): ShowWordDialogFragment {
            val bundle = Bundle().apply {
                putString(WORD, word)
            }
            return ShowWordDialogFragment().apply { arguments = bundle }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var vm: ShowWordDialogViewModel

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = viewModelProvider(viewModelFactory)

        requireNotNull(arguments).run {
            vm.word = getString(WORD)
        }

        val dataBinding =
            FragmentShowWordDialogBinding.inflate(inflater, container, false).apply {
                lifecycleOwner = this@ShowWordDialogFragment
                vm = this@ShowWordDialogFragment.vm
            }

        analyticsHelper.sendScreenView("ShowWordDialogFragment", requireActivity())

        vm.dismissDialogAction.observe(this, EventObserver {
            dismiss()
        })

        vm.showWordIfUserHasCoins.observe(viewLifecycleOwner, EventObserver {
            toastLong(context!!, "Your word is : ${vm.word}")
        })

        return dataBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpToast()
    }


    private fun setUpToast() {
        view?.setupToast(
            this,
            vm.toastText,
            Snackbar.LENGTH_SHORT
        )
    }

}