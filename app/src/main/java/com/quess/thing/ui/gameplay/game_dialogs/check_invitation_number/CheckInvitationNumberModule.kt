package com.quess.thing.ui.gameplay.game_dialogs.check_invitation_number

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.ChildFragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import com.quess.thing.ui.gameplay.game_dialogs.no_coins.NoCoinsDialogViewModel
import com.quess.thing.ui.gameplay.game_dialogs.no_coins.NoCoinsFragmentDialog
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class CheckInvitationNumberModule {

    /**
     * Generates an [AndroidInjector] for the [CheckInvitationNumberFragmentDialog].
     */
    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeCheckInvitationNumberModule(): CheckInvitationNumberFragmentDialog

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [CheckInvitationNumberViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(CheckInvitationNumberViewModel::class)
    abstract fun bindCheckInvitationNumberViewModel(viewModel: CheckInvitationNumberViewModel): ViewModel
}