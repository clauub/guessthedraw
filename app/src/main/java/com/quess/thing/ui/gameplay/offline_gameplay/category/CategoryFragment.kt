package com.quess.thing.ui.gameplay.offline_gameplay.category


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.quess.thing.R
import com.quess.thing.databinding.FragmentCategoryBinding
import com.quess.thing.util.admob.AdUtil.loadAd
import com.quess.thing.util.admob.RemoveAdsHelper
import com.quess.thing.util.analytics.AnalyticsHelper
import com.quess.thing.util.setupToast
import com.quess.thing.util.viewModelProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import com.google.android.material.card.MaterialCardView
import com.quess.thing.util.objects.Constants.CARD_INT_KEY

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CategoryFragment : DaggerFragment() {

    @Inject
    lateinit var analyticsHelper: AnalyticsHelper
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CategoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = viewModelProvider(viewModelFactory)

        val dataBinding = FragmentCategoryBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@CategoryFragment
            vm = viewModel
        }

        displayAds(dataBinding.root)

        removeAds(dataBinding.root)

        analyticsHelper.sendScreenView("Category", requireActivity())

//       Locked cards
        checkIfProduceCardIsLocked(dataBinding)

        checkIfSportsCardIsLocked(dataBinding)

        checkIfTechCardIsLocked(dataBinding)

        return dataBinding.root
    }

    private fun displayAds(root: View) {
        loadAd(root, R.id.adView)
    }

    private fun removeAds(root: View) {
        RemoveAdsHelper(root, R.id.adView, this).removeAds(viewLifecycleOwner)
    }

    private fun checkIfProduceCardIsLocked(databinding: FragmentCategoryBinding) {
        if (viewModel.statusLocked1) {
            viewModel.cardIsDisabled(
                databinding.cardVegetables,
                databinding.iconVegetables,
                context!!.getDrawable(R.drawable.ic_diet_disabled)
            )
            onClickSendCardInfo(databinding.cardVegetables, 1)
        } else {
            databinding.cardVegetables.setOnClickListener {
                viewModel.goToWordProduce(view!!)
            }
        }
    }

    private fun checkIfSportsCardIsLocked(databinding: FragmentCategoryBinding) {
        if (viewModel.statusLocked2) {
            viewModel.cardIsDisabled(
                databinding.cardSports,
                databinding.iconSports,
                context!!.getDrawable(R.drawable.ic_sport_disabled)
            )
            onClickSendCardInfo(databinding.cardSports, 2)
        } else {
            databinding.cardSports.setOnClickListener {
                viewModel.goToWordSports(view!!)
            }
        }
    }

    private fun checkIfTechCardIsLocked(databinding: FragmentCategoryBinding) {
        if (viewModel.statusLocked3) {
            viewModel.cardIsDisabled(
                databinding.cardTech,
                databinding.iconTech,
                context!!.getDrawable(R.drawable.ic_tech_disabled)
            )
            onClickSendCardInfo(databinding.cardTech, 3)
        } else {
            databinding.cardTech.setOnClickListener {
                viewModel.goToWordTech(view!!)
            }
        }
    }

    private fun openUnlockCardDialog(cardInt: Int) {
        val args = Bundle()
        args.putInt(CARD_INT_KEY, cardInt)
        val dialog =
            UnlockCardDialogFragment()
        dialog.arguments = args
        dialog.show(requireActivity().supportFragmentManager, "DIALOG")
    }

    private fun onClickSendCardInfo(cardItem: MaterialCardView, cardInt: Int) {
        cardItem.setOnClickListener {
            openUnlockCardDialog(cardInt)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpToast()
    }

    private fun setUpToast() {
        view?.setupToast(
            this,
            viewModel.toastText,
            Snackbar.LENGTH_SHORT
        )
    }

    override fun onStart() {
        super.onStart()
        viewModel.isAnimation.set(true)
    }
}
