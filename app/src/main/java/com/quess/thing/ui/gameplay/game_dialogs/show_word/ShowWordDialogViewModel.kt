package com.quess.thing.ui.gameplay.game_dialogs.show_word

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.snackbar.Snackbar
import com.quess.thing.R
import com.quess.thing.util.Event
import com.quess.thing.util.SharedPref
import com.quess.thing.util.objects.Constants
import com.quess.thing.util.setupToast
import javax.inject.Inject

class ShowWordDialogViewModel @Inject constructor(private val sharedPref: SharedPref) :
    ViewModel() {

    val coins = sharedPref.getCoins(Constants.COINS_KEY)

    private val decreaseNumberCoins: Int = 15

    var word: String? = null

    private val _dismissDialogAction = MutableLiveData<Event<Unit>>()
    val dismissDialogAction: LiveData<Event<Unit>>
        get() = _dismissDialogAction

    private val _showWordIfUserHasCoins = MutableLiveData<Event<Unit>>()
    val showWordIfUserHasCoins: LiveData<Event<Unit>>
        get() = _showWordIfUserHasCoins

    private val _toastText = MutableLiveData<Event<Int>>()
    val toastText: LiveData<Event<Int>> = _toastText

    private val _snackbarText = MutableLiveData<Event<Int>>()
    val snackbarText: LiveData<Event<Int>> = _snackbarText


    fun onCloseIconClicked() {
        _dismissDialogAction.value = Event(Unit)
    }

    fun onOkButtonPressed() {
        if (coins >= 15) {
            _showWordIfUserHasCoins.value = Event(Unit)
            updateCoinsToSharedPref()
        } else {
            _toastText.value = Event(R.string.you_dont_have_coins)
        }
        _dismissDialogAction.value = Event(Unit)
    }

    private fun updateCoinsToSharedPref() {
        sharedPref.saveCoins(Constants.COINS_KEY, coins - decreaseNumberCoins)
    }
}

