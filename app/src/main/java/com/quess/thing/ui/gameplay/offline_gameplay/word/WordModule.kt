package com.quess.thing.ui.gameplay.offline_gameplay.word

import androidx.lifecycle.ViewModel
import com.quess.thing.di.scopes.FragmentScoped
import com.quess.thing.di.scopes.ViewModelKey
import com.quess.thing.ui.main.MainFragment
import com.quess.thing.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class WordModule {

    /**
     * Generates an [AndroidInjector] for the [WordFragment].
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeWordFragment(): WordFragment

    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [WordViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(WordViewModel::class)
    abstract fun bindWordViewModel(viewModel: WordViewModel): ViewModel


}